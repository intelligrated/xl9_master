// filename: SPICommunication.c
//

#include "utils.h"
#include "SPICommunication.h"
#include "XLConstants.h"
#include "TrakConstants.h"
#include "portBits.h"
#include "iosgb.h"


unsigned char SPIbuffer[MAX_XL_BUTTONLIGHTS_PER_PORT * MAX_XL_READ_BYTES_PER_STICK];


void
SPI_Init( void )    // initialize the SPI port
{

    // init SPI port
  SPIC1 = SPICFG1;      

  SPIC2 = SPICFG2;            // see XLconstants.h                              
  SPIBAUD = SPIDIV;         // SPI port baud rate

}

void
pollSPI_Read (unsigned char SpiLastTxmt)
{
  // reading the SPI port works better with polling than with interrupts
  // set SpiLastTxmt to the # of bytes to read from SPI port
  // this version optimized for minimum time between SPI port write/reads (7 uS)

  unsigned char dummy;
  unsigned char MoreDataToSend = TRUE;
  unsigned char SpiNextTxmt;
  unsigned char SpiNextRcv;
  int counter;

		
  SpiNextRcv = 0;
  SpiNextTxmt = 0;
  
  while (SPIRXFULL)  // make sure no data in the receive buffer
  {
	dummy = SPID;
  }
 

  while (MoreDataToSend) // if we have something to transmit
  {
	// assume the Transmit Buffer is empty
	SPIDATA = 0xff; // load dummy byte to SPI data buffer
    SpiNextTxmt++; // (you have to transmit a byte to receive a byte)

	// while waiting for shifting to complete, do some overhead chores
	if (SpiNextRcv >= MAX_XL_BUTTONLIGHTS_PER_PORT * MAX_XL_READ_BYTES_PER_STICK) // don't write past the end of the buffer
	  SpiNextRcv = (MAX_XL_BUTTONLIGHTS_PER_PORT * MAX_XL_READ_BYTES_PER_STICK) - 1;
	if (SpiNextTxmt >= SpiLastTxmt)
	  MoreDataToSend = FALSE;
    
	while(!SPIRXFULL)
	  ;
    
	SPIbuffer[SpiNextRcv++] = SPIDATA;   // this also clears SPIRXFULL
    
  } // while
}     // end of pollSPI_ReadOnly()


void
pollSPI_Write(unsigned char outputByte)
{
  while (!SPITXEMPTY)     // spin till SPI tx data bfr empty
    ;
  
  SPIDATA = outputByte; 

}

void
pollSPI_TransmitComplete(void)
{
    unsigned int failsafe;
    unsigned char dummy;

	while (!SPITXEMPTY);  // wait for TX buffer to empty
						  // then we must wait for the transmit shift register to empty
	while (SPIRXFULL)     // clear the SPI receive buffer full flag
	{                     // (SPI transmit and receive functions happen simultaneously)
	  dummy = SPIDATA;
	}

	failsafe = 500;
	while (failsafe)
	{
	  failsafe--;
	  if (SPIRXFULL)    // RX buffer full means transmit function is complete
		break;
	}
}

void
SPI_EnableTransmitInterrupt (void)
{

  SPIC1 |= SPTIE;   // enable transmit buffer empty interrupt
}

void
SPI_EnableReceiveInterrupt (void)
{

  SPIC1 |= SPIE;   // enable receive buffer full interrupt
}


void
SPI_DisableTransmitInterrupt (void)
{

  SPIC1 &= (unsigned char)(~SPTIE);   // disable transmit buffer empty interrupt
}

void
SPI_DisableReceiveInterrupt (void)
{

  SPIC1 &= (unsigned char)(~SPIE);   // disable receive buffer full interrupt
}

void
SendCommand2Slave(unsigned char command, unsigned char portIdx, unsigned char led1,
                  unsigned char color, unsigned char status, unsigned char led2)
{
  unsigned char *ptr;

  ptr = SPIbuffer;
  *ptr++ = PKT_START_BYTE;
  *ptr++ = command;

  switch (command)
  {
    case CMD_DIAGNOSTIC_STATE:
    case CMD_RUN_AUTOCONFIG:
    case CMD_DISPLAY_CLEAR:
    case CMD_INDICATE_DOWNLOAD:
    case CMD_UPDATE_ALL_PORTS:
      *ptr = PKT_STOP_BYTE;
      break;

    case CMD_PORT_CLEAR:
    case CMD_PORT_UPDATE:
      *ptr++ = portIdx;
      *ptr = PKT_STOP_BYTE;
      break;

    case CMD_LED_RAM_UPDATE1:
    case CMD_COLOR_FILL_PORT:
      *ptr++ = portIdx;
      *ptr++ = led1;
      *ptr++ = color;
      *ptr++ = status;
      *ptr = PKT_STOP_BYTE;
      break;

    case CMD_LED_RAM_UPDATEx:
      *ptr++ = portIdx;
      *ptr++ = led1;
      *ptr++ = color;
      *ptr++ = status;
      *ptr++ = led2;
      *ptr = PKT_STOP_BYTE;
      break;

    default:
      return;
  }
  ptr = SPIbuffer;    // reset pointer
                      // using polled transmit because message is short
  while (1)           // baud rate is fast
  {
    while(!SPITXEMPTY);   // wait for TX buffer empty
    SPIDATA = *ptr;
    if (*ptr == PKT_STOP_BYTE)
      break;
    else
      ptr++;
  }
}

void
SendPortDevices2Slave(unsigned char command, unsigned char portIdx, unsigned char nbrOfDevices, unsigned char deviceType)
{
  unsigned char *ptr;

  ptr = SPIbuffer;
  *ptr++ = PKT_START_BYTE;
  *ptr++ = command;
  *ptr++ = portIdx;
  *ptr++ = nbrOfDevices;
  *ptr++ = deviceType;
  *ptr   == PKT_STOP_BYTE;

  ptr = SPIbuffer;    // reset pointer
                      // using polled transmit because message is short
  while (1)           // baud rate is fast
  {
    while(!SPITXEMPTY);   // wait for TX buffer empty
    SPIDATA = *ptr;
    if (*ptr == PKT_STOP_BYTE)
      break;
    else
      ptr++;
  }
}

