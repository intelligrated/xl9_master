/******************************
*SPICommunications.h          *
*Helper functions to send     *
*data out the SPI port        *
*******************************/

#ifndef SPICOMMUNICATION_HEADER
#define SPICOMMUNICATION_HEADER

void SPI_Init( void );
void ReadyPortForSPIWrite (unsigned char port);
void ReadyPortForSPIRead(unsigned char port);
void DisableSPI_ReadWrite(void);
void GenReadStrobe( void );
void pollSPI_Read (unsigned char SpiLastTxmt);
void pollSPI_Write(unsigned char outputByte);
void pollSPI_TransmitComplete(void);
void SPI_EnableTransmitInterrupt(void);
void SPI_EnableReceiveInterrupt(void);
void SPI_DisableTransmitInterrupt(void);
void SPI_DisableReceiveInterrupt(void);
void SendCommand2Slave(unsigned char command, unsigned char portIdx, unsigned char led1,
                  unsigned char color, unsigned char status, unsigned char led2);
void SendPortDevices2Slave(unsigned char command, unsigned char portIdx, unsigned char nbrOfDevices, unsigned char deviceType);
#endif

