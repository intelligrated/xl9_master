//
// Author: tpr
//

#ifndef XLCONSTANTS_H
#define XLCONSTANTS_H

#define MINI_PKT_MAX_LENGTH 8
#define MB_EMPTY 0
#define MB_FULL  1
#define MAX_SPI_PACKETS 10
#define SPI_RX_BFR_SIZE 20
#define BAY_DISP_DEV_ACTIVITY_TC 120  // a bay disp device must respond every (120) secs or it may be replaced
#define PWR_ON_RESET    0x80          // POR status bit in SRS register
#define ILLEGAL_OPCODE_RESET 0x10     // ILOP bit in SRS register
// commands to slave processor
#define CMD_DISPLAY_CLEAR     0xc0
#define CMD_DIAGNOSTIC_STATE  0xc1
#define CMD_LED_RAM_UPDATE1   0xc2
#define CMD_LED_RAM_UPDATEx   0xc3
#define CMD_PORT_UPDATE       0xc4
#define CMD_PORT_CLEAR        0xc5
#define CMD_RUN_AUTOCONFIG    0xc6
#define CMD_INDICATE_DOWNLOAD 0xc7
#define CMD_UPDATE_ALL_PORTS  0xc8
#define CMD_SET_PORT_DEVICES  0xc9
#define CMD_COLOR_FILL_PORT   0xca
// commands initiated by slave
#define CMD_XLBUTTON          0xcc
#define CMD_PORT_DEVICES      0xcd

// handshake lines
#define SlaveDataReadyN PC7
#define SendSlaveDataN  PC6
#define ResetSlaveN     PG4
// SPI
#define SPICNTL1        SPIC1
#define SPICNTL2        SPIC2
#define SPIBAUD         SPIBR
#define SPISTATUS       SPIS
#define SPIDATA         SPID
                                              
#define SPITXEMPTY      (SPISTATUS & 0X20)
#define SPIRXFULL       (SPISTATUS & 0X80)

#define SPICLKPRESCALE  4     // 4: divide by 5   
#define SPICLKDIV       3     // 5: div by 64, 4: div by 32, 3 => 16, 2 => 8  (5 * 64 = 320 [18.8MHz/320 => 58.75 Kbaud]
#define SPIDIV          ((SPICLKPRESCALE<<4)|SPICLKDIV)  // clk rate division (5 * 32 = 160 -> 117 Kbaud)
                                                         // clk rate division (5 * 16 = 80 -> 235 Kbaud)
                                                         // clk rate division (5 * 8 = 40 -> 470 Kbaud)
                              // ssn = gpio, lsbit 1st out
#define SPTIE			0x20  // Transmit Interrupt Enable bit
#define SPIE			0x80  // Receive Interrupt Enable bit
#define SPE             0x40  // SPI port enable

#define SPICFG2         0x10  // unused7, unused6, unused5, master ss pin = slave select 
                              // mosi enabled, unused2, clk active in wait mode, 2 pin data xfer

#define SPICFG1         0x52  // no ints, spi enable, master, active hi clk,edge in middle

#define TURNOFFSPTIE   	0xDF  // Transmit Interrupt Enable bit

#define TXEN1           PG6   // RS-485 transmit enable, serial port 1
#define TXEN2           PC4   // RS-485 transmit enable, serial port 2, 
#endif 
