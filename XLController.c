//
// filename: XLController.c
// Author: tom rebel
// 10/7/2014
//


////////////

// Headers
////////////
#include "globals.h"
#include "XLController.h"
#include "portBits.h"
#include "string.h"
#include "packet.h"
#include "utils.h"
#include "iosgb.h"              
#include "XLConstants.h"
#include "packet.h"                       
#include "processMsgs.h"
#include "SPICommunication.h"






unsigned char SPIrxBuffer[SPI_RX_BFR_SIZE];
unsigned char SPIinPtr, DFSHstate;

void
HandleS2(void)  // handle S2 (send ID switch) on the XL bay controller
{

  switch (S2.Status)
  {
  case BUTTON_CLEAR:
	if (PG7 == 0)   // if switch pushed 
	{
	  if (S2.Value == 0)  // need 2 consecutive samples equal to pushed
		S2.Status = BUTTON_PUSHED;
	  else
		S2.Value = 0;   // set last sample to "pushed" 
	}
	break;

  case BUTTON_PUSHED:
	if (PG7 != 0)   // if switch released
	{
	  S2.Status = BUTTON_CLEAR;
	  S2.Value = 1;   // set last sample to "not pushed"

          txPacket.data[PKT_SEQUENCE_INDEX] = sendSeq++;
          txPacket.data[PKT_DATA_INDEX] = INSTR_BUTTON;
          txPacket.data[MSG_BUTTON_VALUE] = 1;
          msgDataLength = 1;
          BuildPacket();
          SendPacket(); // this is sent for configuration
	}
	break;
  }
}


void
Initialize_XL_BayController (void)
{
  unsigned long address;
  unsigned char i;

  /* turn on all input pull ups to conserve current */
  PTAPE = PTBPE = PTCPE = PTDPE = PTEPE = PTFPE = PTGPE = 0xff;

  /* config pio's */
  PTAD = 0xff;                    // set PORT A outputs hi
  PTADD = 0x00;                   // config PORT A all inputs

  PTBD = 0xff;                    // set PORT B outputs hi 
  PTBDD = 0x00;                   // config PORT B all inputs

  PTCD = 0x4f;                    // set PORT C outputs c4, c5 low, all else hi
  PTCDD = 0x70;                   // config PORT C c7, c0-c3 inputs, c4-c6 outputs (disables transmitters 2, enables receiver 2, SendSlaveData = FALSE)

  PTDD = 0xff;                    // set PORT D outputs hi
  PTDDD = 0x00;                   // config PORT D bits all inputs

  PTED = 0xff;                    // set PORT E outputs hi
  PTEDD = 0x04;                   // config PORT E all inputs cept PE2 = output

  PTFD = 0xff;                    // set PORT F outputs hi
  PTFDD = 0x00;                   // config PORT F bits all inputs

  PTGD = 0xbf;                    // set PORT G outputs hi, cept c6 = 0 (TXEN1)
  PTGDD = 0x58;                   // config PORT G g3,g4,g6 outputs, rest inputs
								  //    PWR/STAT LED which should be ON (logic 1 -> LED ON)
  SPI_Init();             // init SPI port
  PE2 = 0;                // slave select always low (not controlled by SPI module)

  S2.Status = BUTTON_CLEAR;   // init S2 processing variables
  S2.Value = 1;
  DFSHstate = 0;

  // bay display port initialization
  
  ClearExternalAddresses();       // clear all stored bay device addresses

  MilliSecondDelayWait(75); // need some time before serial communications work

  BroadcastReset2BayDisp();       // reset all devices on bay display port

}// End Initialize_XL_BayController

// DataFromSlaveHandler

void 
DataFromSlaveHandler(void)
{
  unsigned char spiDat, dummy, i, devCnt, porti;

  switch (DFSHstate)
  {
    case 0:
      SendSlaveDataN = 1;   // initial value
      if (SlaveDataReadyN == 0)
      {
        DFSHstate = 1;
        SendSlaveDataN = 0;
        SPIinPtr = 0;
      }
      break;

    case 1:
      dummy = SPIDATA;    // read data bfr to clear SPIRXFULL
      SPIDATA = PKT_START_BYTE; // send a byte to start commnications
      DFSHstate = 2;
      break;

    case 2:
      if (SPITXEMPTY)
      {
        SPIDATA = PKT_START_BYTE;
      }
      if (SPIRXFULL)
      {
        if (SPIDATA == PKT_START_BYTE)
        {
          SPIinPtr = 0;
          DFSHstate = 3;
        }
      }
      if (SlaveDataReadyN == 1)
        DFSHstate = 0;      // abort if ready line goes false
        
      break;

  case 3:
      if (SPITXEMPTY)
      {
        SPIDATA = PKT_START_BYTE;
      }
      if (SPIRXFULL)
      {
        spiDat = SPIDATA;
        if (spiDat == PKT_STOP_BYTE)
        { 
          DFSHstate = 4;
        }
        else
        {
          SPIrxBuffer[SPIinPtr] = spiDat;  // save received data
          if (++SPIinPtr >= SPI_RX_BFR_SIZE)
          {
            SPIinPtr--;
          }
        }
      }// end of if (SPIRXFULL
      if (SlaveDataReadyN == 1)
        DFSHstate = 0;    // abort if ready line goes false
        
      break;

    case 4:
      SendSlaveDataN = 1;
      DFSHstate = 5;
      break;

    case 5:
      if (SlaveDataReadyN == 1)
        DFSHstate = 6;
      break;

    case 6:
      if (SPIrxBuffer[0] == CMD_XLBUTTON)
      {
        ButtonMailBox.portIdx = SPIrxBuffer[1];
        ButtonMailBox.deviceIdx = SPIrxBuffer[2];
        ButtonMailBox.value = SPIrxBuffer[3];
        ButtonMailBox.status = MB_FULL;
      }
      else if (SPIrxBuffer[0] == CMD_PORT_DEVICES)
      {
        porti = SPIrxBuffer[1];
        devCnt = SPIrxBuffer[2];
        XLConfigPort[porti].deviceCount = devCnt;
        for (i = 0; i < devCnt; i++)
        {
          XLConfigPort[porti].deviceType[i] = SPIrxBuffer[i+3];
        }
      }
      DFSHstate = 0;    // ready for next command
      break;

    default:
      DFSHstate = 0;
      SendSlaveDataN = 1;
  } // end of switch

} // DataFromSlaveHandler


                            // SCI1 recv interrupt checks received packets for bay_display_addresses.
void                        // If a match, packets are put in the rx packet buffer
Run_XL_BayController (void) // GetPacket() intercepts these packets and copies them to txPacket2,  
{                           // and sets txPacket.status to PKT_TX_READY_TO_SEND  
  unsigned long *addressPtr;
  unsigned char i;
  static unsigned last_t1_cnt = 0;
  //static unsigned char heart_beat_flag = 0;
  

  if (txPacket2.status == PKT_TX_EMPTY) {
    struct Packet *txBuffer2 = GetQueuedTxBuffer2();
    if (txBuffer2 != NULL) {
      txBuffer2->status = PKT_TX_READY_TO_SEND;  // packet is ready to send via the bay display port
      memcpy(&txPacket2, txBuffer2, sizeof(txPacket2));
      txBuffer2->status = PKT_TX_EMPTY; // so txBuffer2 memory can be reused
    }
  }

  if (txPacket2.status == PKT_TX_READY_TO_SEND) {
    addressPtr = (unsigned long *) &txPacket2.data[PKT_ADDRESS_INDEX];
    SendOnce2();

    if ((txPacket2.data[INSTR_INDEX] == INSTR_DOWNLOAD)  ||   // if that packet was a DownloadX instruction
        (txPacket2.data[INSTR_INDEX] == INSTR_DOWNLOAD2) ||
        (txPacket2.data[INSTR_INDEX] == INSTR_DOWNLOAD3)) { // XD uses DOWNLOAD3, CL uses DOWNLOAD2
      while (!(SCI2S1 & 0x40))    // wait for last char to be sent
        ;

      _asm ("sei\n"); // DISABLE_INTERRUPTS();
      PassThru2BayDisplayPort (); // enter pass-thru mode - send all received chars from SCI1 to SCI2
                                  // exit pass-thru mode when 4 consecutive S9 records are detected
      _asm ("cli\n");  // enable interrupts 
      SetState(TRAK_UNCONFIGURED_STATE);
    }
  }

#ifndef DEMO      // in DEMO mode, DemoXLState() handles msgs from bay display port
                  // in normal mode,
                  // packets received from the bay display port are sent to the local controller
				  // if the packet address is present in the bay disp device array.
				  
  if (GetPacket2() != NULL) {  // if not NULL, rxPacket2 filled w/ msg from external device on bay display port
    addressPtr = (unsigned long *) &rxPacket2.data[PKT_ADDRESS_INDEX]; // get address of the external device
    if (!IsABayDispAddress(*addressPtr)) {
      AddBayDispAddress(*addressPtr);
      rxPacket2.status = PKT_RX_EMPTY;
      SendAck2(*addressPtr);
      SendAck2(*addressPtr);
    }
    else { // pass to controller
      struct Packet *txBuffer = GetTxBuffer();
      if (txBuffer != NULL) {
        memcpy(txBuffer, &rxPacket2, rxPacket2.data[PKT_LENGTH_INDEX] + 2);
        rxPacket2.status = PKT_RX_EMPTY;
        QueueTxPacket(txBuffer);
      }
    }
  }// if GetPacket2()
#endif
  
  DataFromSlaveHandler();     // check for data from slave processor, handle if necessary
   
  //---------------- 11/4/16 changes ----------------------
  if (DFSHstate != 0)
  {
    slaveCommTimer = 10;    // 10 ms timeout  
  
    while (DFSHstate != 0)      // if data upload in process, don't do anything else
    {                           // until finished (SPI port busy)
      DataFromSlaveHandler();
      if (slaveCommTimer == 0)  // if timeout, reset slave procesor
      {
        ResetSlaveN = 0;   // activate reset line to slave cpu
        MilliSecondDelayWait(50); // 50 mS reset pulse
        ResetSlaveN = 1;
        DFSHstate = 0;
      }
    }
  }
  //------------------- end of 11/4/16 changes//
/*  while (DFSHstate != 0)      // if data upload in process, don't do anything else
  {                           // until finished (SPI port busy)
    DataFromSlaveHandler();
  }
*/
  if (last_t1_cnt == timer1_ctr) { // timer 1 interrupts every 50 mS
    return;
  }
  last_t1_cnt = timer1_ctr;
  
  HandleS2();     // handle the S2 (send ID) switch
 
  if ((timer1_ctr % 20) == 0) { // 20 * 50mS = 1 second cycle
    for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++) {
      if (XLBayDispDevice[i].timer == 0) {
        XLBayDispDevice[i].address = 0; // remove device
      } else {
        XLBayDispDevice[i].timer--;     // dec Bay Disp Device activity timer
      }
    }
/*    
    if (heart_beat)
    {
      heart_beat_flag = FALSE;
      PG3 = 0;
    }
    else
    {
      heart_beat_flag = TRUE;
      PG3 = 1;
    }
*/
  } // end of every 1 sec chores
  

  if ((timer1_ctr % (20*3)) == 0) { // 3 seconds
    if (rxPacket2.status == PKT_RX_EMPTY && txPacket2.status == PKT_TX_EMPTY) {
      for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++) {
        if (XLBayDispDevice[i].address != 0) {
          break;
        }
      }
      if (i == MAX_XL_BAY_DISP_DEVICES) { // we have not found any devices
        BroadcastNotConfigured2BayDisp();// broadcast NOT CONFIGURED via Bay Display port
      }
    }
  } // end of every 3 secd chores

}// Run_XL_BayController


void
SelfTest_XL_BayController(void)   // reception of INSTR_SELF_TEST puts the bay controller
{                                 // in the Diagnostic state. See diagState.c for details

} 
