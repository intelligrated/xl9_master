//
// Author: tpr
//                                   


#ifndef XL_CONTROLLER_H
#define XL_CONTROLLER_H


// Function Prototypes
void Run_XL_BayController(void);
void SelfTest_XL_BayController(void);
void Initialize_XL_BayController(void);


#endif // End of ifndef XL_CONTROLLER_H

