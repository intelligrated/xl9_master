//
// Track module bootstrap code
//
// Author: Lamar
//

////////////
// Headers
////////////
#include "flash.h"
#include "iosgb.h"


////////////
// Globals 
////////////


//////////////
// Functions 
//////////////

void ConfigureSCI (void);
void InitClk (void);
void LoadDownloader (void);



void
main(void)
{
  int i;
  const unsigned char *address = (unsigned char*) 0x1840;

  // SOPT is a write once register.
  // Enable COP Watchdog 0x80
  // Enable COP long timer 2E18 cycles of BUSCLK
  // Background debug enabled 0x02
  //SOPT = 0xD1;
  SOPT = 0x13;
  //SOPT = 0x11;

  InitClk ();
  ConfigureSCI ();

  for (i = 0; i < 8; i++)
  { // Check the first 8 bytes of FLASH.  If a non-0xFF byte is found within 
    // these bytes, then an executable is assumed to be present.
    if (*(address+i) != 0xFF) {
      _asm ("jmp 0x1840\n");
    }
  }

  LoadDownloader ();
}


void
LoadDownloader (void)   // make sure this is located @ 0xf067
{
  // If we make it to here, there is no executable code at MAIN_CODE.  Execute
  // the downloader code.
  if (_fctcpy ('r')) {  // 'r' is 1st letter of segment name "ram_exec"
                        // copy flash routines to RAM
    _asm ("sei\n"); // DISABLE_INTERRUPTS();
    _flashDownloader ();
  }
}


static void
uDelay (unsigned char u)
{                               // NOTE: lower numbers are less accurate
while(u--);                     // 2:2us,  100:100us,  (these require int argument:: 1000: 1.2ms,  10000: 10ms)
} // end uDelay()


unsigned char
IsItXd24 (void)
{
  unsigned char tmp;

  // code      @ 0x1840
  // bootstrap @ 0xF000
  //unsigned long unitSerialNumber @0xFFAC;
  //unsigned char preAssignedUnitType @0x1200;

  PTAPE = PTBPE = PTCPE = PTDPE = PTEPE = PTFPE = PTGPE = 0xff;

  // auto detect the unit type
  PTADD = 0xff;                   // config PORT D bit 0,2,3,4,5,6,7 to outputs
  PTAD = 0xff;                    // set PORT D outputs hi
  PTADD = 0x00;                   // config PORT D bit 7..0 to inputs

  uDelay (1);

  tmp = PTAD;
  tmp &= 0x0f;    // keep only the 4 lsb's

  // all done, config the port for outputs
  PTAD = 0xff;                    // set PORT D outputs hi
  PTADD = 0xff;                   // config PORT D bit 7..0 to outputs

  switch (tmp)
  {
    case 0x0D: return 1;
  }

  return 0;

}// IsItXd24


void
ConfigureSCI (void)
{
  // Set the SCI baud rate
  // baud rate = BUSCLK/(16*BR)
  // BUSCLK = 18.874368MHz
  // BR = 10
  // baud rate = 18.874368MHz / (16*10) = 117964.8 == 115200 bits per second
  SCI1BD = 10;

  // Configure data
  // 9-bit SCI characters, M=1 -> 16   // this is the parity bit
  // Idle char bit count starts after start bit ILT=1 -> 4
  // Parity function enabled, PE=1 -> 2
  // Even parity, PTY=0 -> 0
  // Note: 1 start bit, 8 data bits, even parity, 1 stop bit,char length=11 bits
  SCI1C1 = 0x16;

  // Configure interrupts
  // Enable the SCI receiver and receive interrupts.
  // Receiver Interrupt Enable RIE=1 -> 0x20
  // Transmit Enable TE=1 -> 0x08
  // Receiver Enable RE=1 -> 0x04
  SCI1C2 = 0x2C;

  /*
  if (IsItXd24 ()) {
    PTCDD = 0x71;
    PTCD &= (unsigned char) ~0x20; // XD24 // Enable 485 receive
    PTCD &= (unsigned char) ~0x10; // XD24 // Disable 485 transmit
  } else {
    PTCDD = 0x0f;
    PTCD &= (unsigned char) ~0x08; // Enable 485 receive
    PTCD &= (unsigned char) ~0x04; // Disable 485 transmit
  }
  */
  // XL bay controller - SCI1 receiver always enabled, SCI2 not used for downloader
  PTCDD = 0x1d; // c0, c2 - c5 outputs c1 input
  PTCD = 0x10;  // SCI1 disable 485 xmit, SCI2 disable 485 xmit & recv
  
  PTGD = 0xbf;       // set PORT G outputs hi, except G6 (TXEN) fixed 10/18/2017 tpr
  PTGDD = 0x7f;      // config PORT G G7 input, G6 - G0 to outputs
                     // G3 drives PWR/STAT LED

  // Turn off overrun, noise, framing, and parity error interrupts
  SCI1C3 = 0;

}// ConfigureSCI


void
InitClk (void)
{
  // FLL in FEE mode
  ICGC2 = 0x70;

  // internal clk =32.768kHz * 64 * N / R=32.768kHz * 64 * 18 / 1=37.748736MHz
  ICGC1 = 0x38;
                                  // busclk = 18.874368MHz
  while(1) {
    if(ICGS1 & 0x08)          // wait for FLL lock
      break;
  }

  if(ICGS1 & 0x01)                  // if ICG intr flag set, clear it
    ICGS1 |= 1;

}// InitClk
