@echo off
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no bootstrap.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no pickState.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no configuredState.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no demoXLState.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no diagState.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no driver.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no flash.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no globals.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no ignoreState.c 
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no interruptSCI2Tx.c  
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no interruptSCI1Rx.c interruptSCI1Tx.c interruptSCI2Rx.c interrupt.c interruptRTI.c interruptTimer1.c interruptTimer2.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no jump_table.s
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no multidrop.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no packet.c  
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no processMsgs.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no unconfiguredState.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no utils.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no vectors.c
if errorlevel 1 goto bad
cx6808 %1 -dXL -d DEMO -v -l -dxdebug +debug -pp -pck -e -no XLController.c
if errorlevel 1 goto bad

:clink
echo.
echo Linking ...
clnk -sa -p -odriver.s08 -mdriver.map driver.lkf
if errorlevel 1 goto bad
clnk -sa -p -oboot.s08 -mboot.map boot.lkf
if errorlevel 1 goto bad


:chexa
echo.
echo Converting ...
chex +hT3C -fm -odriver.s19 driver.s08
if errorlevel 1 goto bad
chex +hT3C -fm -oboot.s19 boot.s08
if errorlevel 1 goto bad
:cllabs
echo.
echo Generating absolute listing ...
clabs driver.s08
if errorlevel 1 goto bad
clabs boot.s08
if errorlevel 1 goto bad
echo.
echo.
echo        The Compilation is successfull.
echo.
rem pause
goto sortie
:bad
echo.
echo.
echo        THE COMPILATION FAILED.
echo.
pause
:sortie


del *.o

echo on

