//
// Author: Tom
//


#include "configuredState.h"
#include "globals.h"
#include "packet.h"

// button values
//   1 -> S1 button closest to upstream end of stick
//   2 -> S2 button next to S1
//   4 -> S3 button next to S2
//   8 -> S4 button closest to downstream (controller side) of stick
 
void ConfiguredState ()   
{
  
  if (txPacket.status == PKT_TX_EMPTY)
  {
    if (ButtonMailBox.status == MB_FULL)
    {
      txPacket.data [PKT_SEQUENCE_INDEX] = sendSeq++;
      txPacket.data [PKT_DATA_INDEX] = INSTR_XLBUTTON;
      txPacket.data [MSG_XLBUTTON_PORT] = (unsigned char) (ButtonMailBox.portIdx+1);    // send port # instead of index
      txPacket.data [MSG_XLBUTTON_DEVICE_NUMBER] = (unsigned char) (ButtonMailBox.deviceIdx+1);  // send device # instead of index
      txPacket.data [MSG_XLBUTTON_VALUE] = ButtonMailBox.value;
      msgDataLength = 3;
      BuildPacket ();
      SendPacket ();
      ButtonMailBox.status = MB_EMPTY;
    }
  }  // end of if*/
}

