#ifdef DEMO
//
// Author: Lamar
// Tue Apr 22 14:53:48 PDT 2014
//

#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "demoXLState.h"
#include "XLController.h"
#include "globals.h"
#include "iosgb.h"
#include "packet.h"
#include "TrakConstants.h"
#include "utils.h"

#define DEMO_MAX_PICKS  5   // 5 picks per order
#define DEMO_MAX_UNITS  1

struct DemoPick {
  unsigned char qty;
  unsigned char portIdx;
  unsigned char devIdx;
  unsigned char value;
  unsigned char begLtIdx;
  unsigned char color;
} picks[DEMO_MAX_PICKS];


enum DemoStates {
  DEMO_DONE_STATE,
  DEMO_HI_STATE,
  DEMO_MENU_STATE,
  DEMO_PICK_STATE,
  DEMO_WELCOME_STATE
};


enum Menus {
  menuAdjust=0,
//  menuFull,
//  menuLast,
  menuLocation,
  menuLogoff,
  menuStore,
  menuSku,
  menuMax
};
static char *menus [menuMax];

/*
struct DemoUnit {
  unsigned long address;
  struct DemoPick picks[DEMO_MAX_PICKS];
};
*/

#define Lt1Color (unsigned char)LT_LIME
#define Lt2Color (unsigned char)LT_LIME
#define Lt3Color (unsigned char)LT_BLUE
#define Lt4Color (unsigned char)LT_BLUE


static unsigned char light;
unsigned char color;
static enum DemoStates demoState;
static enum Menus currentMenu;
static struct DemoUnit units[DEMO_MAX_UNITS];
unsigned char BtnReleasedValue;
unsigned char BtnReleasedDevice;
unsigned char BtnReleasedPort;
unsigned char XDButtonReceived;
unsigned char pickIdx;

void DemoDoneState(void);
void DemoHiState(void);
void DemoMenuState(void);
void DemoPickState(void);
void DemoWelcomeState(void);

void ClearMenus(void);
void debugLine(const int);
unsigned char DisplayPick(unsigned char);
void DisplayStr(const char *str);
//void Display2Pick(unsigned char idx,unsigned char pickIdx);
//void Display4Pick(unsigned char idx,unsigned char pickIdx);
unsigned char GetPickIdxForButton(unsigned long,unsigned char);
unsigned char GetMenuPickIdx(unsigned long);
struct DemoUnit *GetUnit(unsigned long address,unsigned char *idx);
void HandleAdjustPick(unsigned long address);
void IncrementLight(unsigned char *light);
unsigned char IsCL4(unsigned long address);
void MenuChoice(unsigned long address,unsigned char menuIdx);
unsigned char NbrOfUnits(void);
//unsigned char PicksLeft(void);
void SendAbort(unsigned long address);
void SendAdjust(unsigned long address);
unsigned char SendMenu(unsigned long address);
unsigned char SendMenuAdj(unsigned long address);
unsigned char SendMenuLoc(unsigned long address);
unsigned char SendMenuOrder(unsigned long address);
unsigned char SendMenuSku(unsigned long address);
unsigned char Send1stMenu(unsigned long address, unsigned char value,
                          unsigned char seq);
void GeneratePicks(void);

void
ActivateDemoXLState() {  // 

    menus [menuAdjust] = "ADJUST";
//  menus [menuFull] = "FULL";
//  menus [menuLast] = "LAST";
  menus [menuLocation] = "LOC";
  menus [menuLogoff] = "LOGOFF";
  menus [menuStore] = "STORE";
  menus [menuSku] = "SKU";
  //ClearAllPorts();    // all lights off - done on reset
                                                                                                                                                                                                                                                                                        
  //light = LT1;
  //color = LT_BLUE;
  //dnLts[light].status = LT_SOLID | LT_NEEDS_ACTIVATION;
  //dnLts[light].color = color;
  
  demoState = DEMO_WELCOME_STATE;
  MilliSecondDelayNoWait3(1500);

  //SetAddressToSend(MASK_BROADCAST);
  //BroadcastNotConfigured2BayDisp(); // will blank display
 
  // to randomize the picks
  TPM2CNTH;
  srand(TPM2CNTL);

  XDButtonReceived = FALSE;

}// ActivateDemoXLState

void
ActivateMenu (unsigned char lUnitIdx)
{
  //ClearUnit ();

  //strcpy(display.str1, MenuStr ());
  //StringCopy (display.str1, MenuStr ());
  currentMenu = menuAdjust;
  //strcat (display.str1, menus [currentMenu]);

  //display.strToDisplay = 1;
  //SET_DISPLAY_STATUS (DISPLAY_SOLID);

  //TurnOnPickLightsForMenus (lUnitIdx);

  demoState = DEMO_MENU_STATE;

}// ActivateMenu


/*
void
ClearMenus() {
  unsigned char idx;

  for (idx = 0; idx < NbrOfUnits(); ++idx) {
    units[idx].picks[0].menu = FALSE;
    units[idx].picks[1].menu = FALSE;
    units[idx].picks[2].menu = FALSE;
    units[idx].picks[3].menu = FALSE;
  }
}
*/

/*
unsigned char
CompleteMenuPick(unsigned long address) {
  unsigned char idx;
  struct DemoUnit *u = GetUnit(address, &idx);

  if (u == NULL) {
    return FALSE;
  }

  for (idx = 0; idx < DEMO_MAX_PICKS; ++idx) {
    if (u->picks[idx].menu) {
      u->picks[idx].displayed = FALSE;
      u->picks[idx].qty = 0;
      break;
    }
  }

  return TRUE;

}// CompleteMenuPick
*/

/*
unsigned char
CompletePick(unsigned long address, unsigned char light) {
  unsigned char idx;
  unsigned char pickIdx = UCHAR_MAX;
  struct DemoUnit *u = GetUnit(address, &idx);

  if (u == NULL) {
    return FALSE;
  }

  pickIdx = (unsigned char) (light - 1);
  if (pickIdx > 3) {
    return FALSE;
  }

  if (u->picks[pickIdx].displayed) {
    u->picks[pickIdx].displayed = FALSE;
    u->picks[pickIdx].qty = 0;
  }

  UpdateBayDisplay();

  if (IsCL4(u->address)) {
    if (pickIdx == 0) {
      if (u->picks[2].displayed) {
        Display4Pick(idx, 2);
        return TRUE;
      } else if (u->picks[3].qty != 0) {
        Display4Pick(idx, 3);
        return TRUE;
      }
    } else if (pickIdx == 1) {
      if (u->picks[3].displayed) {
        Display4Pick(idx, 3);
        return TRUE;
      } else if (u->picks[2].qty != 0) {
        Display4Pick(idx, 2);
        return TRUE;
      }
    } else if (pickIdx == 2) {
      if (u->picks[0].displayed) {
        Display4Pick(idx, 0);
        return TRUE;
      } else if (u->picks[1].qty != 0) {
        Display4Pick(idx, 1);
        return TRUE;
      }
    } else if (pickIdx == 3) {
      if (u->picks[1].displayed) {
        Display4Pick(idx, 1);
        return TRUE;
      } else if (u->picks[0].qty != 0) {
        Display4Pick(idx, 0);
        return TRUE;
      }
    }
  }

  return TRUE;

}// CompletePick
*/

/*
void
debugLine(const int line) {
  char str[24 + 1];

  StringCopy(str, "Line ");

  NbrToStr(line, 10, 4);
  strcat(str, nbrToStr);

  str[24] = 0;
  DisplayStr(str);
}

*/
// activates lights associated with data generated with "GeneratePicks()"
// and displays quantity on the XD connected to the bay display port
unsigned char
DisplayPick(unsigned char pickIdx) {
 
  unsigned char i, idx;
  char msgbfr[25];

  if ((XLConfigPort[picks[pickIdx].portIdx].deviceType[0] == XL_BL_1_BUTTON) ||
      (XLConfigPort[picks[pickIdx].portIdx].deviceType[0] == XL_BL_2_BUTTON))
  {
    UpdateBLmemory(picks[pickIdx].portIdx, picks[pickIdx].begLtIdx, picks[pickIdx].color, LT_SOLID);
    SendButtonLightData(picks[pickIdx].portIdx, XLConfigPort[picks[pickIdx].portIdx].deviceCount, FALSE);  // update physical LEDs
  }
  else    // else it's a stick light with buttons
  {
    for (i = 0; i < 3; i++)
      UpdateXL12LEDMemory(picks[pickIdx].portIdx, picks[pickIdx].begLtIdx+i, picks[pickIdx].color, LT_SOLID); // 3 lights around switch
    SendShelfStickData(picks[pickIdx].portIdx, XLConfigPort[picks[pickIdx].portIdx].deviceCount, FALSE);  // update physical LEDs
  }
  
  NbrToStr(picks[pickIdx].qty, 10);
  strcpy(msgbfr, "PICK ");
  strcat(msgbfr, nbrToStr);
  SendMsgToBayDisplay(msgbfr); //display quantity to pick 
  return(1);
}// DisplayPick

// turn off lights associated with data generated with "GeneratePick()"
void
ClearPick(unsigned char pickIdx) {
  unsigned char idx, i;

  
  if ((XLConfigPort[picks[pickIdx].portIdx].deviceType[0] == XL_BL_1_BUTTON) ||
      (XLConfigPort[picks[pickIdx].portIdx].deviceType[0] == XL_BL_2_BUTTON))
  {
    UpdateBLmemory(picks[pickIdx].portIdx, picks[pickIdx].begLtIdx, color, LT_OFF);
    SendButtonLightData(picks[pickIdx].portIdx, XLConfigPort[picks[pickIdx].portIdx].deviceCount, FALSE);  // update physical LEDs
  }
  else    // else it's a stick light with buttons
  {
    for (i = 0; i < 3; i++)
      UpdateXL12LEDMemory(picks[pickIdx].portIdx, picks[pickIdx].begLtIdx+i, color, LT_OFF); // 3 lights around switch
    SendShelfStickData(picks[pickIdx].portIdx, XLConfigPort[picks[pickIdx].portIdx].deviceCount, FALSE);  // update physical LEDs
  }
  
  // display will be cleared by subsequent message 
  return;

/*
 SendAbort();
  for (idx = 0; idx < NbrOfUnits(); ++idx) {
    units[idx].picks[0].displayed = FALSE;
    units[idx].picks[1].displayed = FALSE;
    units[idx].picks[2].displayed = FALSE;
    units[idx].picks[3].displayed = FALSE;
  }
*/
}

void
IncrementColor( void )
{
  color++;
  if (color == 3)    
    color = 4;
  else if (color > 5)
    color = 1;
}

void
GeneratePicks(void) {     // generate all the picks required for an order & throw out duplicates   
  unsigned char i, j, porti;
  unsigned char pic = 0;

  do 
  {
    porti = (unsigned char)(rand() % MAX_XL_PORTS); // port index range 0 - 9
    if (XLConfigPort[porti].deviceCount == 0)
      continue;   // start over if no devices on random port
    picks[pic].portIdx = porti;     // save valid port index
    IncrementColor();   // cycle thru 4 colors
    picks[pic].color = color;
    picks[pic].qty = (unsigned char)(1 + rand() % 4);   // qty between 1 and 4
    picks[pic].devIdx = (unsigned char) (rand() % XLConfigPort[picks[pic].portIdx].deviceCount);
    if (XLConfigPort[picks[pic].portIdx].deviceType[picks[pic].devIdx] == XL_STICK_W_BUTTONS)
    {
      i = (unsigned char)(rand() % 4);    // random # from 0 to 3
      picks[pic].value = (unsigned char)(1 << i);  // convert to random 1, 2, 4 or 8
      picks[pic].begLtIdx = (unsigned char)((picks[pic].devIdx * 12) + (i * 3));
    }
    else if (XLConfigPort[picks[pic].portIdx].deviceType[picks[pic].devIdx] == XL_BL_2_BUTTON)
    {
      picks[pic].value = (unsigned char)(1 + (rand() % 2));    // randomly select 1 or 2
      picks[pic].begLtIdx = (unsigned char)((picks[pic].devIdx * 2) + picks[pic].value - 1);
    }
    else if (XLConfigPort[picks[pic].portIdx].deviceType[picks[pic].devIdx] == XL_BL_1_BUTTON)
    {
      picks[pic].value = 1; // only 1 button on this device
      picks[pic].begLtIdx = (unsigned char)(picks[pic].devIdx * 2);
    }
    else      // XL_STICK_WO_BUTTONS
      continue;     // start over - no buttons on this device
    
    if (pic > 1)
    {
      for (j = 0; j < pic; j++)
      {
        if ((picks[pic].portIdx == picks[j].portIdx) &&   // if latest pick is the same as one of the previous
            (picks[pic].devIdx == picks[j].devIdx) &&
            (picks[pic].value == picks[j].value))
        {
          break;                                         // try it again
        }
      }
      if (j < pic) // if break was executed
        continue;  // re-pic
    }
    pic++;
  } while (pic < DEMO_MAX_PICKS);
}

void
DemoXLState() {  // CPU time directed here from main() when XL in TRAK_DEMO_STATE
  unsigned long address;
  unsigned long *addressPtr;
  //unsigned char tmp1UChar;
  //unsigned char tmp2UChar;
  unsigned char iport, jstick, stick_count;

  // nothing connected to SC1 port when in demo mode
  // process msgs from XDs connected to SC2 (bay display port)

  if (GetPacket2 () != NULL)    // if not NULL, rxPacket2 filled w/ msg from bay display device                                   
  {
    addressPtr = (unsigned long *) &rxPacket2.data[PKT_ADDRESS_INDEX]; // get address of the bay disp device
    if (!AddBayDispAddress (*addressPtr))     // check if it is in the array of bay disp devices
    {                                         // if not and can't be added, it is (probably) an unconfigured msg
  	  rxPacket2.status = PKT_RX_EMPTY;        // send an ACK to the bay disp device 
	  SendAck2(*addressPtr);                  // it's addr will be added to the array when another devices timer = 0
	}
	else  // device address is in the array, handle the packet here
	{
      address = *((unsigned long*)(&rxPacket2.data[PKT_ADDRESS_INDEX]));
      //SetAddressToSend(address);

      switch (rxPacket2.data[PKT_DATA_INDEX]) {
        case INSTR_ACK:
          rxPacket2.status = PKT_RX_EMPTY;
          txPacket2.status = PKT_TX_EMPTY;
        break;
/*
        case INSTR_ADJUST_PICK:
          rxPacket2.status = PKT_RX_EMPTY;
          SendAck2(*addressPtr);
          HandleAdjustPick(address);
        break;
*/
        case INSTR_XDBUTTON:
          XDButtonReceived = TRUE;
          //tmp2UChar = txPacket.data[PKT_SEQ];
          rxPacket2.status = PKT_RX_EMPTY;
          SendAck2(*addressPtr);
/*          
          if (demoState == DEMO_PICK_STATE) {
            ClearPick(address);
            Send1stMenu(address, tmp1UChar, tmp2UChar);
            demoState = DEMO_MENU_STATE;
          }
*/
        break;

        case INSTR_UNCONFIGURED:
          rxPacket2.status = PKT_RX_EMPTY;
          //AddUnit(address);
          //SendConfigure();
        break;

        default:
          rxPacket2.status = PKT_RX_EMPTY;
          SendAck2(*addressPtr); // just ACK whatever came in
        break;
      }   // end of switch
      return;
    }     // end of else
  }   // end of if Getpacket2(  )

  // scan XL/SL buttons

  for (iport = 0; iport < MAX_XL_PORTS; iport++)
  {
    stick_count = XLConfigPort[iport].deviceCount;

    for (jstick = 0; jstick < stick_count; jstick++)
    {
      if (ButtonPort[iport].ButtonStick[jstick].Status == BUTTON_RELEASED)
      {
        BtnReleasedPort = (unsigned char) (iport);    // record port index
        BtnReleasedDevice = (unsigned char) (jstick);  //
        BtnReleasedValue = ButtonPort[iport].ButtonStick[jstick].Value;
         
        ButtonPort[iport].ButtonStick[jstick].Value = 0;
        ButtonPort[iport].ButtonStick[jstick].Status = BUTTON_CLEAR;
      }
    }
  }

  switch (demoState) {
    case DEMO_DONE_STATE:    DemoDoneState(); break;
    case DEMO_HI_STATE:      DemoHiState(); break;      // follows welcome state
    case DEMO_MENU_STATE:    DemoMenuState(); break;    // not used
    case DEMO_PICK_STATE:    DemoPickState(); break;    // follows hi state
    case DEMO_WELCOME_STATE: DemoWelcomeState(); break; // initial state
  }
}   // DemoXLState


void
DemoWelcomeState() {  // initial state of demo

  if ((deviceStatus & DEVICE_STATUS_DELAYING3) == 0) {
    //dnLts[light].status = LT_OFF;
    //IncrementLight(&light);
    //IncrementColor(&color);
    //dnLts[light].status = LT_SOLID | LT_NEEDS_ACTIVATION;
    //dnLts[light].color = color;
    SendMsgToBayDisplay("XL/SL DEMO PRESS BUTTON");
    MilliSecondDelayNoWait3(1000);
    
  }

  //if (dnButtonsReleased & BUTTON1) {
  if (XDButtonReceived)
  {
    XDButtonReceived = FALSE;
    //ClearAllPorts();
    SendMsgToBayDisplay("        HI JOHN         ");
    MilliSecondDelayNoWait3(1000);
    pickIdx = 0;
    demoState = DEMO_HI_STATE;
  }

}// DemoWelcomeState


void
DemoHiState() {
  if ((deviceStatus & DEVICE_STATUS_DELAYING3) == 0) {// after timeout
    ClearAllPorts();
    GeneratePicks();
    DisplayPick(pickIdx);
    demoState = DEMO_PICK_STATE;
  }
}

void
DemoPickState() {

  if (BtnReleasedValue)
  {
    if ((BtnReleasedValue == picks[pickIdx].value) &&
        (BtnReleasedDevice == picks[pickIdx].devIdx) &&
        (BtnReleasedPort == picks[pickIdx].portIdx))
    {
      ClearPick(pickIdx);
      BtnReleasedValue = 0;
      BtnReleasedDevice = 0;
      BtnReleasedPort = 0;
      if (++pickIdx < DEMO_MAX_PICKS)
        demoState = DEMO_HI_STATE;
      else
        demoState = DEMO_DONE_STATE;
    }
  }
} // DemoPickState

void
DemoDoneState() {
    SendMsgToBayDisplay("ORDER COMPLETE         ");
    XDButtonReceived = FALSE;
    MilliSecondDelayNoWait3(4000);  // disp ORDER COMPLETE for 4 secs
    demoState = DEMO_WELCOME_STATE;
}

void
DemoMenuState() {
}// DemoMenuState


/*
struct DemoUnit *
GetUnit(unsigned long address, unsigned char *idx) {
  struct DemoUnit *u = NULL;

  for (*idx = 0; *idx < NbrOfUnits(); ++*idx) {
    if (address == units[*idx].address) {
      u = &units[*idx];
      break;
    }
  }
  return u;
}
*/
/*
void
IncrementLight(unsigned char *light) {
  *light = (unsigned char) (*light + 1);
  if (*light > LT12) {
    *light = LT1;
  }
}
*/
/*
void
MenuChoice(unsigned long address, unsigned char menuIdx) {
  memset(&txPacket.data, 0, sizeof(txPacket.data));

  txPacket.data[MSG_MENU_INDEX] = menuIdx;

  if (menuIdx == 0) { // Ord
    strcpy(&txPacket.data[MSG_MENU_STR], "0155");
    SendMenu(address);
    DisplayStr("ORDER 0155XB");
    return;
  }

  if (menuIdx == 1) { // Sku
    strcpy(&txPacket.data[MSG_MENU_STR], "34TA");
    SendMenu(address);
    DisplayStr("SKU 34TA17");
    return;
  }

  if (menuIdx == 2) { // Loc
    strcpy(&txPacket.data[MSG_MENU_STR], "BYZ9");
    SendMenu(address);
    DisplayStr("LOC BYZ9");
    return;
  }

  SendAdjust(address);
  DisplayStr("USER BUSY");

}// MenuChoice
*/
/* 
unsigned char
SendMenuLoc(unsigned long address) {
  memset(&txPacket.data, 0, sizeof(txPacket.data));

  txPacket.data[MSG_MENU_INDEX] = 2;
  strcpy(&txPacket.data[MSG_MENU_STR], "LOC");
  return SendMenu(address);
}
*/
/*
unsigned char
SendMenuOrder(unsigned long address) {
  memset(&txPacket.data, 0, sizeof(txPacket.data));

  txPacket.data[MSG_MENU_INDEX] = 0;
  strcpy(&txPacket.data[MSG_MENU_STR], "ORD");
  return SendMenu(address);
}
*/
/*

unsigned char
SendMenuSku(unsigned long address) {
  memset(&txPacket.data, 0, sizeof(txPacket.data));

  txPacket.data[MSG_MENU_INDEX] = 1;
  strcpy(&txPacket.data[MSG_MENU_STR], "SKU");
  return SendMenu(address);
}
*/
/*
unsigned char
Send1stMenu(unsigned long address, unsigned char value, unsigned char seq) {
  unsigned char pickIdx;
  struct DemoUnit *u = GetUnit(address, &pickIdx); // pickIdx not used

  memset(&txPacket.data, 0, sizeof(txPacket.data));

  txPacket.data[MSG_MENU_INDEX] = 0;
  strcpy(&txPacket.data[MSG_MENU_STR], "ORD");

  pickIdx = GetPickIdxForButton(address, value);
  if (pickIdx == UCHAR_MAX) {
    return FALSE;
  }

  if (u == NULL) {
    return FALSE;
  }
  u->picks[pickIdx].menu = TRUE;

  txPacket.data[PKT_INSTR] = INSTR_MENU;

  switch (pickIdx + 1) {
    case 1:
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT1_STATUS])) = LT_BLINKING;
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT1_COLOR])) = Lt1Color;
    break;

    case 2:
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT2_STATUS])) = LT_BLINKING;
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT2_COLOR])) = Lt2Color;
    break;

    case 3:
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT3_STATUS])) = LT_BLINKING;
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT3_COLOR])) = Lt3Color;
    break;

    case 4:
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT4_STATUS])) = LT_BLINKING;
    *((unsigned char *)(&txPacket.data[MSG_MENU_LT4_COLOR])) = Lt4Color;
    break;
  }

  msgDataLength = (unsigned char)(16+strlen(&txPacket.data[MSG_MENU_STR] + 1));
  SetAddressToSend(address);
  txPacket.data[PKT_SEQ] = seq;
  SendOnce();

  DisplayStr("USER BUSY");

  return TRUE;

}// Send1stMenu 
*/
/*
void
UpdateBayDisplay() {
  unsigned char idx;
  unsigned char idx2;
  unsigned int totalQty = 0;

  for (idx = 0; idx < NbrOfUnits(); ++idx) {
    if (units[idx].address == 0) {
      continue;
    }
    for (idx2 = 0; idx2 < DEMO_MAX_PICKS; ++idx2) {
      totalQty += units[idx].picks[idx2].qty;
    }
  }

  display.strToDisplay = 1;
  //NbrToStr(totalQty, 10, 3);
  StringCopy(display.str1, nbrToStr);
  SET_DISPLAY_STATUS(DISPLAY_SOLID);

}// UpdateBayDisplay
*/
#endif // #ifdef DEMO
