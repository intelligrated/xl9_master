//
// Author: Lamar
// Thu Aug  5 12:25:15 PDT 2010
//

#include <string.h>

#include "diagState.h"

#include "TrakConstants.h"
#include "SPICommunication.h"

unsigned char diagTimer;  // keeps lights from being "all on" forever

 
void
ActivateDiagState (void) {
  
  SendCommand2Slave(CMD_DIAGNOSTIC_STATE,0,0,0,0,0);  // tell slave to enter diagnostic state
 
  
}// ActivateDiagState


void
DiagState (void)    // not much for the master to do
{



}// DiagState
