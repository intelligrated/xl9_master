// 
// Author: Lamar, Joe, Tom
//

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "demoXLState.h"
#include "flash.h"
#include "globals.h"
#include "interruptSCI2Rx.h"
#include "iosgb.h"
#include "packet.h"
#include "portbits.h"
#include "processMsgs.h"
#include "utils.h"

#include "configuredState.h"
#include "diagState.h"
#include "ignoreState.h"
#include "unconfiguredState.h"
#include "pickState.h"
#include "XLController.h"
#include "SPICommunication.h"


//#define _DEBUGGER

#ifdef _DEBUGGER
//------------------------------------------------------------------------------
// included code from bootstrap.c for debugging driver using the Zap Debugger.
//------------------------------------------------------------------------------
// TO DEBUG USING ZAP:
// 1) uncomment out the define _DEBUGGER above to enable debugging
// 2) uncomment out the vector.o lines in driver.lkf
// 3) modify the address = 0x29000000; (with the correct device address) under SetAddress in main().
// 4) modify build.bat to add  -dxdebug +debug
//------------------------------------------------------------------------------

void
InitClk (void)
{
  // FLL in FEE mode
  ICGC2 = 0x70;

  // internal clk =32.768kHz * 64 * N / R=32.768kHz * 64 * 18 / 1=37.748736MHz
  ICGC1 = 0x38;
                                  // busclk = 18.874368MHz
  while(1) {
    if(ICGS1 & 0x08)          // wait for FLL lock
      break;
  }

  if(ICGS1 & 0x01)                  // if ICG intr flag set, clear it
    ICGS1 |= 1;

}// InitClk

void
ConfigureSCI (void)
{
  // Set the SCI baud rate
  // baud rate = BUSCLK/(16*BR)
  // BUSCLK = 18.874368MHz
  // BR = 10
  // baud rate = 18.874368MHz / (16*10) = 117964.8 == 115200 bits per second
  SCI1BD = 10;

  // Configure data
  // 9-bit SCI characters, M=1 -> 16   // this is the parity bit
  // Idle char bit count starts after start bit ILT=1 -> 4
  // Parity function enabled, PE=1 -> 2
  // Even parity, PTY=0 -> 0
  // Note: 1 start bit, 8 data bits, even parity, 1 stop bit,char length=11 bits
  SCI1C1 = 0x16;

  // Configure interrupts
  // Enable the SCI receiver and receive interrupts.
  // Receiver Interrupt Enable RIE=1 -> 0x20
  // Transmit Enable TE=1 -> 0x08
  // Receiver Enable RE=1 -> 0x04
  SCI1C2 = 0x2C;

  PTCDD = 0x1f;
  //PTCD &= (unsigned char) ~0x08; // Enable 485 receive
  // receiver always enabled
  PTCD &= (unsigned char) ~0x04; // Disable 485 transmit


  // Turn off overrun, noise, framing, and parity error interrupts
  SCI1C3 = 0;

}// ConfigureSCI

#endif  // --------------------------------------------------- ifdef _DEBUGGER

void
ConfigureSCI2_Multidrop (void)
{
  // Set the SCI baud rate
  // baud rate = BUSCLK/(16*BR)
  // BUSCLK = 18.874368MHz
  // BR = 61
  // baud rate = 18.874368MHz / (16*61) = 19338.49 ==  19200 bits per second
  SCI2BD = 61;

  // Configure data
  // 8-bit SCI characters, M=0 BIT4 -> 0
  // Idle char bit count starts after start bit ILT=1 BIT2 -> 4
  // Parity function disabled, PE=1 BIT1 -> 0
  // Note: 1 start bit, 8 data bits, no parity, 1 stop bit,char length=10 bits
  //SCI2C1 = 0x16;
  SCI2C1 = 0x04; // no parity

  // Configure interrupts
  // Enable the SCI receiver and receive interrupts.
  // Receiver Interrupt Enable RIE=1 BIT5 -> 0x20
  // Transmit Enable TE=1 BIT3 -> 0x08
  // Receiver Enable RE=1 BIT2 -> 0x04
  SCI2C2 = 0x24;

  // Turn off overrun, noise, framing, and parity error interrupts
  SCI2C3 = 0;

}// ConfigureSCI2_Multidrop

void
ConfigureSCI2_BayDisplay (void)
{
  // Set the SCI baud rate
  // baud rate = BUSCLK/(16*BR)
  // BUSCLK = 18.874368MHz
  // BR = 10
  // baud rate = 18.874368MHz / (16*10) = 117964.8 == 115200 bits per second
  SCI2BD = 10;

  // Configure data
  // 9-bit SCI characters, M=1 -> 16   // this is the parity bit
  // Idle char bit count starts after start bit ILT=1 -> 4
  // Parity function enabled, PE=1 -> 2
  // Even parity, PTY=0 -> 0
  // Note: 1 start bit, 8 data bits, even parity, 1 stop bit,char length=11 bits
  SCI2C1 = 0x16;

  // Configure interrupts
  // Enable the SCI receiver and receive interrupts.
  // Receiver Interrupt Enable RIE=1 -> 0x20
  // Transmit Enable TE=1 -> 0x08
  // Receiver Enable RE=1 -> 0x04
  SCI2C2 = 0x2C;

  PTCDD = 0x1f;         // 5 lsbits are outputs
  PTCD &= (unsigned char) ~0x10; // Enable 485 receive SCI2
  PTCD &= (unsigned char) ~0x08; // Disable 485 transmit SCI2


  // Turn off overrun, noise, framing, and parity error interrupts
  SCI2C3 = 0;

}// ConfigureSCI2_BayDisplay


void
main ()
{

//------------------------------------------------------------------------------
#ifdef _DEBUGGER

  // the following is copied from bootstrap.c for use when debugging

  // SOPT is a write once register.
  // Enable COP Watchdog 0x80
  // Enable COP long timer 2E18 cycles of BUSCLK
  // Background debug enabled 0x02
  //SOPT = 0xD1;
  SOPT = 0x13;    // Watchdog disabled, background debug enabled, stop mode disabled
  //SOPT = 0x11;  // (if stop mode disabled, the "stop" instr causes a reset (illegal opcode))

  //initclk();
  InitClk ();
  ConfigureSCI ();         // SCI1

#endif
//------------------------------------------------------------------------------


  // Read system reset status register to send to controller
  bootStatus = SRS;

  SetAddress ();    // determine address for this hardware config 
 

//------------------------------------------------------------------------------
// modify address of device you want to debug
//------------------------------------------------------------------------------
#ifdef _DEBUGGER
                            // override address set in SetAdress()
  address = 0x40000044;   // bay controller 
  
#endif
//------------------------------------------------------------------------------

  InitPacketVars();
  InitTimers ();
  ConfigureSCI2_BayDisplay();


  // Configure rti interrupt 125 cycles/second
  //SET_BIT (SRTISC, BIT5); // set external clock
  //SET_BIT (SRTISC, BIT0); // select divider 001 -> 8ms interrupts
  //SET_BIT (SRTISC, BIT4); // enable rti interrupt
  

  InitializeGlobals ();

  _asm("cli");      // enable interrupts

  myClass = CLASS_BAY_CONTROLLER >> 16;
  
  Initialize_XL_BayController ();
 
  srand (TPM1CNTH);
  srand (TPM1CNTL);
  
  ResetSlaveN = 0;   // activate reset line to slave cpu
  MilliSecondDelayWait(50);
  ResetSlaveN = 1;

  // send 1st message to controller
  MilliSecondDelayNoWait3 (60000); // unconfigured message to be sent in 60 secs
  
  SetState (TRAK_UNCONFIGURED_STATE);

#ifdef DEMO
  SetState (TRAK_DEMO_STATE);
  ActivateDemoXLState();
#endif

  while (1) {
    //SRS = 1; // reset COP timer

    if (GetPacket1 () != NULL) {
      switch (rxPacket.data [PKT_DATA_INDEX]) {
        case INSTR_GET_STATUS:
          GetStatusMsg ();
        break;

        case INSTR_PING:
          PingMsg ();
        break;

        case INSTR_RESET:
          _asm("stop");      // reboot this processor
        break;

        case INSTR_SELF_TEST:
          SelfTestMsg ();
        break;

        case INSTR_SET_STATE:
          SetStateMsg ();
        break;

        default:
          deviceStatus |= DEVICE_STATUS_HAS_MESSAGE;
      }
    } // End of if (GetPacket () != NULL)

    switch (currentState) {
      case TRAK_CONFIGURED_STATE: ConfiguredState (); break;
      case TRAK_IGNORE_STATE: IgnoreState (); break;
      case TRAK_UNCONFIGURED_STATE: UnconfiguredState (); break;
      case TRAK_DIAG_STATE: DiagState (); break;
      case TRAK_PICK_STATE: PickState(); break;
#ifdef DEMO
      case TRAK_DEMO_STATE: DemoXLState(); break;
#endif
    }

    // in case state did not handle msg
    if (deviceStatus & DEVICE_STATUS_HAS_MESSAGE)
    {
      deviceStatus &= (unsigned int) ~DEVICE_STATUS_HAS_MESSAGE;
      switch (rxPacket.data [PKT_DATA_INDEX]) {
        case INSTR_ABORT:
          AbortMsg();
        break;

        case INSTR_ACK:
          if (rxPacket.data [PKT_SEQUENCE_INDEX] == lastSendSeq) {
            txPacket.status = PKT_TX_EMPTY;
          }
          rxPacket.status = PKT_RX_EMPTY;
        break;

        case INSTR_CONFIGURE:
          ConfigureMsg();
        break;

        case INSTR_XLABORT_PORT: {
          unsigned char portIdx;
          portIdx = (unsigned char) (rxPacket.data[MSG_XLABORT_PORT_PORT] - 1);
          rxPacket.status = PKT_RX_EMPTY;
          if (portIdx < MAX_XL_PORTS) {
            SendAck();
            SendCommand2Slave(CMD_PORT_CLEAR, portIdx, 0, 0, 0, 0); // off LEDs
          } else {
            SendError("bad xl port=", rxPacket.data[MSG_XLABORT_PORT_PORT]);
          }
        }
        break;

        case INSTR_XLCONFIG: XLConfigMsg (); break;
        case INSTR_NOT_CONFIGURED:
          rxPacket.status = PKT_RX_EMPTY;
          if (mySection == 0)
            SendUnconfigured ();
          else
            SendAck ();
        break;

      case INSTR_DEBUG:
        //ClearPortLEDs(1);
//          SendAck ();
        break;

        case INSTR_SET_ZONE:
          myZone = *((unsigned int *) &rxPacket.data [MSG_SET_ZONE_ZONE]);
          rxPacket.status = PKT_RX_EMPTY;
          SendAck ();
        break;
      
         // XL 
        case INSTR_XLSINGLE_LIGHT: XLSingleLightMsg(); break;
        case INSTR_XLRANGE_COLOR: XLRangeColorMsg(); break;
        case INSTR_XLRANGE_LIGHT: XLRangeLightMsg(); break;
        case INSTR_XLGET_PORT_DEVICES: XLGetPortDevicesMsg(); break;
        case INSTR_XLSET_PORT_DEVICES: XLSetPortDevicesMsg(); break;
        case INSTR_XLSET_EXTERNAL_ADDR: XLSetExternalAddrMsg(); break;
        case INSTR_XLGET_EXTERNAL_ADDR: XLGetExternalAddrMsg(); break;
        case INSTR_XLRUN_AUTOCONFIG: XLRunAutoConfigMsg(); break;
        case INSTR_GENERIC: GenericMsg (); break;
        case INSTR_XLPICK: XLPickMsg (); break;

        case INSTR_SYNC_COUNTERS:
          rxPacket.status = PKT_RX_EMPTY;
          SendAck ();
        break;
        
        default:
          rxPacket.status = PKT_RX_EMPTY;
          SendError ("INSTR=", rxPacket.data [INSTR_INDEX]);
          txPacket.status = PKT_TX_EMPTY;
        break;
      }
      rxPacket.status = PKT_RX_EMPTY;
    } // End of if (deviceStatus & DEVICE_STATUS_HAS_MESSAGE)

    if (rxPacket.status & PKT_RX_READ) {
      continue;
    }

    Run_XL_BayController(); 
    
    if ((txPacket.status == PKT_TX_SENT) ||
        (txPacket.status == PKT_TX_COLLIDED) ||
        (txPacket.status == PKT_TX_READY_TO_SEND)) {
      if (!(deviceStatus & DEVICE_STATUS_DELAYING))  // if not delaying
        SendPacket ();
    } else if (txPacket.status == PKT_TX_EMPTY) {
      struct Packet *packet = GetQueuedTxBuffer();
      if (packet != NULL) {
        packet->status = PKT_TX_EMPTY;
        memcpy(&txPacket, packet, sizeof(txPacket));
        if (txPacket.data[INSTR_INDEX] == INSTR_ACK) {
          SendOnce();
        } else {
          SendPacket();
        }
      }
    }

  }// while (1)
}// main
