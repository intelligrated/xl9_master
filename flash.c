//
// Flash memory rewriting functionality
//
//

#include <stdlib.h>
#include <string.h>

#include "flash.h"
#include "iosgb.h"
#include "portbits.h"


// Constants
#define FLASH_ROW_SIZE 64
#define FLASH_BLOCK_SIZE 512

#define S0_STATE         1
#define S1_STATE         2
#define S5_STATE         3
#define S9_STATE         4


// Globals
struct FlashVars1 { // use no more than 55 bytes 09/27/09
  unsigned char activeRow;
  unsigned int  inputAddress;
  unsigned int  address1;
  unsigned int  address2;
  unsigned int  address3;
  unsigned int  address4;
  unsigned int  address5;
  unsigned int  address6;
  unsigned int  address7;
  unsigned int  address8;
  unsigned char idx;
  unsigned char firstPass;
  unsigned char nbrOfInputChars;
  unsigned char light; // debug
  unsigned long lightCycles; // debug
  unsigned char readChar;
  unsigned char readSCI1S1;
  unsigned char row1Bytes;
  unsigned char row2Bytes;
  unsigned char row3Bytes;
  unsigned char row4Bytes;
  unsigned char row5Bytes;
  unsigned char row6Bytes;
  unsigned char row7Bytes;
  unsigned char row8Bytes;
  unsigned char state;
  // total bytes = 34
//ljw delete
  unsigned long cycle1;
  unsigned long cycle2;
  

//} f1 @0x85;
} f1 @0x100;

struct FlashVars2 { // use no more than 1024 bytes 09/27/09
  unsigned char input [(FLASH_ROW_SIZE * 2)+2+2+4+2+1]; // S?+NN+AAAA+KK+ 0term
  unsigned char row1 [FLASH_ROW_SIZE];
  unsigned char row2 [FLASH_ROW_SIZE];
  unsigned char row3 [FLASH_ROW_SIZE];
  unsigned char row4 [FLASH_ROW_SIZE];
  unsigned char row5 [FLASH_ROW_SIZE];
  unsigned char row6 [FLASH_ROW_SIZE];
  unsigned char row7 [FLASH_ROW_SIZE];
  unsigned char row8 [FLASH_ROW_SIZE];
  // total bytes = 651
//} f2 @0x100;
} f2 @0x140;


// Prototypes
unsigned char ClearAnyErrors (void);
void Convert (const unsigned char input [], unsigned char row [],
              unsigned char *bytes);
unsigned int HexToDecimal (const char *data, const unsigned char length);
void Erase (void);
void FlashLight (unsigned char reset); // debug
void NoProtect (void);
void Protect (void);
void HandleS0 (void);
void HandleS1 (void);
void HandleS5 (void);
void InitializeRow2 (void);
void InitializeRow3 (void);
void InitializeRow4 (void);
void InitializeRow5 (void);
void InitializeRow6 (void);
void InitializeRow7 (void);
void InitializeRow8 (void);
void InitializeRow9 (void);
void InitializeS0 (void);
void InitializeS1 (void);
void InitializeS5 (void);
void InitIo (void); // debug
void ReadChar (void);
void UDelay (unsigned char u);
void WriteBytes (const unsigned int address, const unsigned char row [],
                 const unsigned char nbrOfBytes);
void WriteRow (const unsigned int address, const unsigned char row []);


// Functions
void Blue (void)
{
//  PTBD &= (unsigned char) ~0x01;
//  PTBD &= (unsigned char) ~0x02;
//  PTBD |= (unsigned char)  0x04;
}
void Green (void)
{
//  PTBD &= (unsigned char) ~0x01;
//  PTBD |= (unsigned char)  0x02;
//  PTBD &= (unsigned char) ~0x04;
}
void Cyan (void)
{
//  PTBD &= (unsigned char) ~0x01;
//  PTBD |= (unsigned char)  0x02;
//  PTBD |= (unsigned char)  0x04;
}
void Red (void)
{
//  PTBD |= (unsigned char)  0x01;
//  PTBD &= (unsigned char) ~0x02;
//  PTBD &= (unsigned char) ~0x04;
}
void Magenta (void)
{
//  PTBD |= (unsigned char)  0x01;
//  PTBD &= (unsigned char) ~0x02;
//  PTBD |= (unsigned char)  0x04;
}
void Yellow (void)
{
//  PTBD |= (unsigned char)  0x01;
//  PTBD |= (unsigned char)  0x02;
//  PTBD &= (unsigned char) ~0x04;
}
void White (void)
{
//  PTBD |= (unsigned char)  0x01;
//  PTBD |= (unsigned char)  0x02;
//  PTBD |= (unsigned char)  0x04;
}


unsigned char
Checksum (const char *input, const unsigned char nbrOfChars)
{
  unsigned char checkSum;
  unsigned char idx;


  checkSum = 0xFF;
  for (idx = 0; idx < nbrOfChars; idx += 2) {
    checkSum -= (unsigned char) HexToDecimal (&input [idx], 2);
  }
  return checkSum;
}


unsigned char
ClearAnyErrors (void)
{
  unsigned char rc = 0;

  // 0x10 Access Error Flag. if set write 0x80 to clear
  if (FSTAT & 0x10) {
    FSTAT |= 0x10;
    rc = 1;
  }
  if (FSTAT & 0x20) {
    FSTAT |= 0x20;
    rc = 1;
  }
  return rc;

} // ClearAnyErrors


void
Convert (const unsigned char input [], unsigned char row [],
         unsigned char *bytes)
{
  unsigned char idx;

  *bytes = (unsigned char) (f1.nbrOfInputChars - 6);
  *bytes = (unsigned char) (*bytes / 2);
  for (idx = 0; idx < *bytes; idx++) {
    row [idx] = (unsigned char) HexToDecimal (&input [idx * 2], 2);
  }
}


void
Erase (void)
{
  unsigned char *dst;
  unsigned char nbrOfBlocks;

  Blue ();

  nbrOfBlocks = (unsigned char) HexToDecimal (&f2.input [10], 2);
  
  ClearAnyErrors ();

  while ((FSTAT & 0x80) == 0) // wait until a cmd may be written
    ;

  dst = (unsigned char *) f1.inputAddress;
  while (nbrOfBlocks > 0) {
    nbrOfBlocks--;

    FlashLight (1);

    // write a data value to an address in the flash array, any value
    *dst = 1; // any value
    dst += FLASH_BLOCK_SIZE; // move to next block

    // 0x40 Page erase 512 bytes per page
    FCMD = 0x40;      // write cmd to FCMD

    // 0x80 FLASH Command Buffer Empty Flag. is cleared by writing a 1 to it
    FSTAT |= 0x80; // activate cmd, clear cmd buffer empty

    UDelay (16);

    // check for flash protection
    // check for flash access error
    if (ClearAnyErrors ()) {
      InitializeS0 ();
      return;
    }

    while ((FSTAT & 0x40) == 0) // wait for command to complete
      ;
  }// while (nbrOfBlocks > 0)

  InitializeS1 ();

}// Erase


void
FlashLight (unsigned char reset) // debug
{

  f1.light = 0;

  f1.light++;
  if (f1.light > 4)
    f1.light = 1;

/*
  PTFD |= 0x10; // lt1
  PTFD |= 0x20; // lt2
  PTFD |= 0x40; // lt3
  PTFD |= 0x80; // lt4
*/
  if (reset) {
    f1.lightCycles = 0;
  }

  f1.lightCycles++;
  if (f1.lightCycles > f1.cycle1) {
    if (f1.lightCycles > f1.cycle2) {
      f1.lightCycles = 0;
      return;
    }
    return;
  }
/* ljw
  f1.lightCycles++;
  if (f1.lightCycles > 25000) {
    if (f1.lightCycles > 50000) {
      f1.lightCycles = 0;
      return;
    }
    return;
  }
*/

/*
  if (f1.state == 1) {
    PTFD &= (unsigned char) ~0x10;
  }
  if (f1.state == 2) {
    PTFD &= (unsigned char) ~0x20;
  }
  if (f1.state == 3) {
    PTFD &= (unsigned char) ~0x40;
  }
  if (f1.state == 4) {
    PTFD &= (unsigned char) ~0x80;
  }
*/
}// FlashLight


unsigned int
HexToDecimal (const unsigned char *data, const unsigned char length)
{
  unsigned char idx;
  unsigned int value;

  value = 0;
  for (idx = 0; idx < length; idx++) {
    if (data [idx] <= '9')
      value += (data [idx] - '0') * (1 << (4 * (length - 1 - idx)));
    else
      value += (data [idx] - '7') * (1 << (4 * (length - 1 - idx)));
  }
  return value;
}


void
HandleS0 (void)
{
  unsigned char command;

  command = (unsigned char) HexToDecimal (&f2.input [8], 2);
  switch (command) {
    case 1:
      NoProtect ();
      Erase ();
    break;

    case 2:
      Protect ();
      Erase ();
    break;

    default:
      InitializeS0 ();
  }
}// HandleS0


void
HandleS1 (void)
{
  switch (f1.activeRow) {
    case 1:
      f1.address1 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row1 [0], &f1.row1Bytes);
      InitializeRow2 ();
    break;
    case 2:
      f1.address2 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row2 [0], &f1.row2Bytes);
      InitializeRow3 ();
    break;
    case 3:
      f1.address3 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row3 [0], &f1.row3Bytes);
      InitializeRow4 ();
    break;
    case 4:
      f1.address4 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row4 [0], &f1.row4Bytes);
      InitializeRow5 ();
    break;
    case 5:
      f1.address5 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row5 [0], &f1.row5Bytes);
      InitializeRow6 ();
    break;
    case 6:
      f1.address6 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row6 [0], &f1.row6Bytes);
      InitializeRow7 ();
    break;
    case 7:
      f1.address7 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row7 [0], &f1.row7Bytes);
      InitializeRow8 ();
    break;
    case 8:
      f1.address8 = f1.inputAddress;
      Convert (&f2.input [8], &f2.row8 [0], &f1.row8Bytes);
      InitializeRow9 ();
      InitializeS5 ();
    break;

    default:
      InitializeS0 ();
      return;
    break;
  }
}// HandleS1


void
HandleS5 (void)
{
  unsigned int nbrOfS1Recs;

  nbrOfS1Recs = HexToDecimal (&f2.input [4], 4);
  if (nbrOfS1Recs != f1.activeRow - 1) {
    return;
  }

  if (f1.row1Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address1, f2.row1);
  } else {
    WriteBytes (f1.address1, f2.row1, f1.row1Bytes);
  }
  if (nbrOfS1Recs == 1) {
    return;
  }

  if (f1.row2Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address2, f2.row2);
  } else {
    WriteBytes (f1.address2, f2.row2, f1.row2Bytes);
  }
  if (nbrOfS1Recs == 2) {
    return;
  }

  if (f1.row3Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address3, f2.row3);
  } else {
    WriteBytes (f1.address3, f2.row3, f1.row3Bytes);
  }
  if (nbrOfS1Recs == 3) {
    return;
  }

  if (f1.row4Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address4, f2.row4);
  } else {
    WriteBytes (f1.address4, f2.row4, f1.row4Bytes);
  }
  if (nbrOfS1Recs == 4) {
    return;
  }

  if (f1.row5Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address5, f2.row5);
  } else {
    WriteBytes (f1.address5, f2.row5, f1.row5Bytes);
  }
  if (nbrOfS1Recs == 5) {
    return;
  }

  if (f1.row6Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address6, f2.row6);
  } else {
    WriteBytes (f1.address6, f2.row6, f1.row6Bytes);
  }
  if (nbrOfS1Recs == 6) {
    return;
  }

  if (f1.row7Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address7, f2.row7);
  } else {
    WriteBytes (f1.address7, f2.row7, f1.row7Bytes);
  }
  if (nbrOfS1Recs == 7) {
    return;
  }

  if (f1.row8Bytes == FLASH_ROW_SIZE) {
    WriteRow (f1.address8, f2.row8);
  } else {
    WriteBytes (f1.address8, f2.row8, f1.row8Bytes);
  }
  return;


/*
  if (nbrOfS1Recs == 1) {
    if (f1.activeRow == 2) {
      if (f1.row1Bytes == FLASH_ROW_SIZE) {
        WriteRow (f1.address1, f2.row1);
      } else {
        WriteBytes (f1.address1, f2.row1, f1.row1Bytes);
      }
    }
    InitializeS0 ();
    return;
  }
*/
}// HandleS5


void
InitializeFlashClock (void)
{
  // initialize clock for programming and erasing flash

  // flash clock 18.874368MHz - FCDIV is a 1 time write
  // 0x40 div by 8 prescaler for flash clock
  if(!(FCDIV & 0x80)) // if FCDIV has been written since reboot, can't write it
    FCDIV = 0x40|12;//18.874368MHz/8/12=196608kHz(150kHz .. 200kHz allowed)
}


void
InitializeRow2 (void)
{
  Magenta ();

  f1.activeRow = 2;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.row2Bytes = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row2, 0, FLASH_ROW_SIZE);
}


void
InitializeRow3 (void)
{
  Magenta ();

  f1.activeRow = 3;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.row3Bytes = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row3, 0, FLASH_ROW_SIZE);
}


void
InitializeRow4 (void)
{
  Magenta ();

  f1.activeRow = 4;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.row4Bytes = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row4, 0, FLASH_ROW_SIZE);
}


void
InitializeRow5 (void)
{
  Magenta ();

  f1.activeRow = 5;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.row5Bytes = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row5, 0, FLASH_ROW_SIZE);
}


void
InitializeRow6 (void)
{
  Magenta ();

  f1.activeRow = 6;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.row6Bytes = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row6, 0, FLASH_ROW_SIZE);
}


void
InitializeRow7 (void)
{
  Magenta ();

  f1.activeRow = 7;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.row7Bytes = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row7, 0, FLASH_ROW_SIZE);
}


void
InitializeRow8 (void)
{
  Magenta ();

  f1.activeRow = 8;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.row8Bytes = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row8, 0, FLASH_ROW_SIZE);
}


void
InitializeRow9 (void)
{
  Magenta ();

  f1.activeRow = 9;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.state = S5_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
}


void
InitializeS0 (void)
{
  Yellow ();

  f1.activeRow = 1;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.light = 1;
  f1.lightCycles = 1;
  f1.row1Bytes = 0;
  f1.state = S0_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
}


void
InitializeS1 (void)
{
  White ();

  f1.activeRow = 1;
  f1.idx = 0;
  f1.firstPass = 1;
  f1.nbrOfInputChars = 0;
  f1.state = S1_STATE; 
  //memset (f2.input, 0, (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2 + 1);
  //memset (f2.row1, 0, FLASH_ROW_SIZE);
}


void
InitializeS5 (void)
{
  Cyan ();

  f1.state = S5_STATE; 
}


void
InitIo (void) // debug
{
//  display.size = 8;

// turn on all input pull ups to conserve current
PTAPE = PTBPE = PTCPE = PTDPE = PTEPE = PTFPE = PTGPE = 0xff;

// config pio's
PTAD = 0xff;                    // set PORT A outputs hi
PTADD = 0xc0;                   // config PORT A bits 5..0 to inputs, 7..6 outputs

PTBD = 0xff;                    // set PORT B outputs hi
PTBDD = 0xff;                   // config PORT B bits 7..0 to outputs

//PTCD = 0xf3;                    // set PORT C outputs hi
//PTCDD = 0x0f;                   // config PORT C bits 3..0 to outputs, 7..4 inputs

PTDD = 0xff;                    // set PORT D outputs hi
PTDDD = 0xff;                   // config PORT D bit 0,2,3,4,5,6,7 to outputs

//PTED = 0xff;                    // set PORT E outputs hi
//PTEDD = 0xff;                   // config PORT E bits 7..0 to outputs


PTFD = 0xff;                    // set PORT F outputs hi
PTFDD = 0xff;                   // config PORT F bits 7..0 to outputs

PTGD = 0xff;                    // set PORT G outputs hi
PTGDD = 0xff;                   // config PORT G bit 7..0 to outputs
                                // G3 drives PWR/STAT LED
}// InitIo


void
NoProtect (void)
{
  // protection reg
  // 0x80 any location, not protected or secured, maybe erased or programmed
  // 0x40 no flash block is procted, else FPS2:FPS0 is block protected
  // bits 0x20,0x10,0x08 are flash protect size selects
  FPROT = 0x80 | 0x40;

  UDelay (16);
}


void
Protect (void)
{
  // protection reg
  // 0x80 any location, not protected or secured, maybe erased or programmed
  // 0x40 no flash block is procted, else FPS2:FPS0 is block protected
  // bits 0x20,0x10,0x08 are flash protect size selects
  // 
  unsigned char protectBits;

  protectBits = (unsigned char) HexToDecimal (&f2.input [12], 2);
  protectBits = (unsigned char) (protectBits << 3);
  FPROT = (unsigned char) (0x80 | protectBits);

  UDelay (16);

}// Proctect


void
ReadChar (void)
{
  // returns the character read
  // returns 0 no data
  // returns -1 data error

  f1.readSCI1S1 = SCI1S1; // read status register
  f1.readChar = SCI1D;    // read data
  SCI1C3; // 9 bit mode

  if ((f1.readSCI1S1 & 0x20) == 0) { // no data
    f1.readChar = 0;
  }
//  if ((f1.readSCI1S1 & 0x0F) != 0) { // data error
//    f1.readChar = -1;
//  }
  if ((f1.readSCI1S1 & 0x10) != 0) { // idle event
    f1.readChar = 0;
  }
}// ReadChar


void
UDelay(unsigned char u)
{           // NOTE: lower numbers are less accurate
while(u--); // 2:2us,  100:100us,  (these require int argument:: 1000: 1.2ms,  10000: 10ms)
}


void
WriteBytes (const unsigned int address, const unsigned char row [],
            const unsigned char nbrOfBytes)
{
  unsigned char *dst;
  unsigned char idx;

  Red ();
  FlashLight (1);

  ClearAnyErrors ();

  while ((FSTAT & 0x80) == 0) // wait until a cmd may be written
    ;

  for (idx = 0; idx < nbrOfBytes; idx++) {

    dst = (unsigned char *) (address + idx);
    *dst = row [idx];

    // 0x20 byte program
    FCMD = 0x20;      // write cmd to FCMD

    // 0x80 FLASH Command Buffer Empty Flag. is cleared by writing a 1 to it
    FSTAT |= 0x80; // activate cmd, clear cmd buffer empty

    UDelay (16);

    // check for flash protection
    // check for flash access error
    if (ClearAnyErrors ()) {
      InitializeS0 ();
      return;
    }

    while ((FSTAT & 0x40) == 0) // wait for command to complete
      ;
  }// while (nbrOfBytes > 0)
}// WriteBytes


void
WriteRow (const unsigned int address, const unsigned char row [])
{
  unsigned char *dst;
  unsigned char idx;

  Red ();
  FlashLight (1);

  ClearAnyErrors ();

  for (idx = 0; idx < FLASH_ROW_SIZE; idx++) {

    while ((FSTAT & 0x80) == 0) // wait until a cmd may be written
      ;

    dst = (unsigned char *) (address + idx);
    *dst = row [idx];

    // 0x25 byte program - burst mode
    FCMD = 0x25;      // write cmd to FCMD

    // 0x80 FLASH Command Buffer Empty Flag. is cleared by writing a 1 to it
    FSTAT |= 0x80; // activate cmd, clear cmd buffer empty

    UDelay (16);

    // check for flash protection
    // check for flash access error
    if (ClearAnyErrors ()) {
      InitializeS0 ();
      return;
    }
  }
  while ((FSTAT & 0x40) == 0) // wait for command to complete
    ;
}// WriteRow


void
_flashDownloader()
{
  unsigned char checksum;
  unsigned char tmpUChar;
  unsigned char value;

  //InitIo (); // for debugging
  InitializeFlashClock ();
  InitializeS0 ();

  White (); // for debugging

f1.cycle1 = 25000;
f1.cycle2 = 50000;

  while (1) {
    FlashLight (0);

    ReadChar ();
    if (f1.readChar == 0) {
      continue;
    }

    if (f1.idx == 0) {
      if (f1.readChar != 'S') {
        if (f1.firstPass == 0) {
          InitializeS0 ();
        }
        continue;
      }
    }

    if (f1.idx == 1) {
      if (f1.readChar == '9') {
         f1.state = S9_STATE; 
      }
      switch (f1.state) {
        case S0_STATE: 
          if (f1.readChar != '0') {
            InitializeS0 ();
            continue;
          }
        break;
        case S1_STATE: 
          if (f1.readChar == '5') {
            InitializeS5 ();
          }
          else if (f1.readChar != '1') {
            InitializeS0 ();
            continue;
          }
        break;
        case S5_STATE: 
          if (f1.readChar != '5') {
            InitializeS0 ();
          }
        break;
      }
    }

    if (f1.idx >= (FLASH_ROW_SIZE * 2) + 2 + 2 + 4 + 2) {
      InitializeS0 ();
      continue;
    }

    if (f1.firstPass == 1) {
      f2.input [f1.idx] = f1.readChar;
    } else {
      if (f2.input [f1.idx] != f1.readChar) { // verify each character
        InitializeS0 ();
        continue;
      }
    }
    f1.idx++;

    if (f1.idx < 4) {
      continue;
    }

    if (f1.idx == 4) {
      f1.nbrOfInputChars = (unsigned char) HexToDecimal (&f2.input [2], 2);
      f1.nbrOfInputChars *= 2;
      continue;
    }

    tmpUChar = (unsigned char) (f1.nbrOfInputChars + 4);
    if (f1.idx != tmpUChar) { // 2 for S0 and 2 for length
      continue;
    }

    checksum = Checksum (&f2.input [2], f1.nbrOfInputChars);
    tmpUChar = (unsigned char) (f1.idx - 2); // last 2 characters
    value = (unsigned char) HexToDecimal (&f2.input [tmpUChar], 2);
    if (checksum != value) {
      InitializeS0 ();
      continue;
    }

    if (f1.firstPass == 1) {
      f1.firstPass = 0;
      f1.idx = 0;
      Green ();
      continue;
    }

    f1.inputAddress = HexToDecimal (&f2.input [4], 4);

    if (f1.state == S0_STATE) {
      HandleS0 (); 
    }
    else if (f1.state == S1_STATE) {
      if (PG3)     // toggle PWR/STAT LED on every S1 record
        PG3 = 0;
      else
        PG3 = 1;

      HandleS1 (); 
    }
    else if (f1.state == S5_STATE) {
      PG3 = 1;        // status light ON while programming      
      HandleS5 ();
      PG3 = 0;        // OFF
      InitializeS1 ();
    }
    else if (f1.state == S9_STATE) {
      PG3 = 0;            // turn off status light
      _asm("stop");      // reboot processor
    }
  }
} // _flashDownloader
