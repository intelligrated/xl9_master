#ifndef _FLASH_H
#define _FLASH_H


int _fctcpy(char name); // This function is implemented in the Cosmic C
                        // library, but there isn't a prototype for it in
                        // any of the headers.  So the prototype has been
                        // added here.

void _flashDownloader(void);


#endif
