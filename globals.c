#include <limits.h>
#include <string.h>

#include "structs.h"
#include "trakConstants.h"
#include "utils.h"
#include "diagState.h"


@tiny volatile unsigned char activeBufferIdx;
@tiny volatile unsigned char activeBufferIdx2;

@tiny unsigned char bootStatus;

//volatile struct ButtonsInfo buttons;
@tiny volatile unsigned int delay1Cycles;
@tiny volatile unsigned int delay2Cycles;
@tiny volatile unsigned int delay3Cycles;
@tiny volatile unsigned char myClass;
@tiny volatile unsigned int myZone;

volatile struct Packet bufPacket [3];
volatile struct Packet rxPacket;
volatile struct Packet txPacket;

volatile struct Packet bufPacket2[2];
volatile struct Packet rxPacket2;
volatile struct Packet txPacket2;


// page zero variables
@tiny volatile unsigned long myAddress;
@tiny volatile unsigned char currentState;
@tiny volatile unsigned int deviceStatus;
@tiny volatile unsigned int mySection;



//////// for XL Controller

volatile struct MiniPacket rxSPIpacket[MAX_SPI_PACKETS];
volatile struct MailBox ButtonMailBox;
volatile struct ConfigPortx XLConfigPort[MAX_XL_PORTS];
volatile struct BayDispDevice XLBayDispDevice[MAX_XL_BAY_DISP_DEVICES];
@tiny volatile struct PBSwitchX S2;
volatile struct XLPickData pick[MAX_XL_PORTS];
@tiny volatile unsigned timer1_ctr;
@tiny volatile unsigned slaveCommTimer;
@tiny unsigned char msgDataLength2;

@tiny unsigned char checkSumBootStrap;
@tiny unsigned char checkSumMainCode;
@tiny unsigned char nbrOfSends;
@tiny char nbrToStr [10];
@tiny unsigned char lastSendSeq;
@tiny unsigned char msgDataLength;
@tiny unsigned char sendSeq;


void
InitializeGlobals (void)
{
  unsigned int tmpUInt;

  timer1_ctr = 0;

  activeBufferIdx = 0;
  activeBufferIdx2 = 0;

  deviceStatus = DEVICE_STATUS_CLEAR;

  myClass = 0;
  mySection = 0;
  myZone = 0;
  nbrOfSends = 0;
  lastSendSeq = 0;
  sendSeq = 0;

  memset(&bufPacket, 0, sizeof(bufPacket));
  memset(&rxPacket, 0, sizeof(rxPacket));
  memset(&txPacket, 0, sizeof(txPacket));

  memset(&bufPacket2, 0, sizeof(bufPacket2));
  memset(&rxPacket2, 0, sizeof(rxPacket2));
  memset(&txPacket2, 0, sizeof(txPacket2));

  checkSumMainCode = 0;
  for (tmpUInt=0x1840; tmpUInt < 0xF000; tmpUInt++)
    checkSumMainCode ^= *((unsigned char*) (tmpUInt));
  checkSumBootStrap = 0;
  for (tmpUInt=0xF000; tmpUInt < 0xFFAC; tmpUInt++)
    checkSumBootStrap ^= *((unsigned char*) (tmpUInt));

} // End of InitializeGlobals (void)

void
SetState (enum TrakStates state) {
  currentState = state;
  switch (currentState) {
    case TRAK_DIAG_STATE: ActivateDiagState (); break;
  }
}

