#ifndef GLOBALS_H     
#define GLOBALS_H

//
// Author: Lamar
//
//


#include "structs.h"
#include "TrakConstants.h"


void InitializeGlobals (void);
void SetState (enum TrakStates state);


// page zero variables
extern volatile unsigned char activeBufferIdx;
extern volatile unsigned char activeBufferIdx2;


extern unsigned char bootStatus;
extern volatile unsigned int delay1Cycles;
extern volatile unsigned int delay2Cycles;
extern volatile unsigned int delay3Cycles;
extern volatile unsigned char myClass;
extern volatile unsigned int myZone;

extern volatile struct Packet bufPacket [];
extern volatile struct Packet rxPacket;
extern volatile struct Packet txPacket;
extern volatile struct Packet bufPacket2 [];
extern volatile struct Packet rxPacket2;
extern volatile struct Packet txPacket2;



// in page zero
extern volatile unsigned long myAddress;
extern volatile unsigned char currentState;
extern volatile unsigned int mySection;


 // XL Controller
//extern volatile struct Shelfx Shelf[];
//extern volatile struct BLPortx BLPort[];
//extern volatile struct ButtonPortX ButtonPort[];
extern volatile struct ConfigPortx XLConfigPort[];
extern volatile struct MailBox ButtonMailBox;
extern volatile struct BayDispDevice XLBayDispDevice[];
extern volatile struct PBSwitchX S2;
extern volatile struct XLPickData pick[];
extern volatile unsigned timer1_ctr;
extern volatile unsigned slaveCommTimer;
extern unsigned char msgDataLength2;

extern unsigned char checkSumBootStrap;
extern unsigned char checkSumMainCode;
extern unsigned char msgDataLength;
extern unsigned char nbrOfSends;
extern char nbrToStr [];
extern unsigned char lastSendSeq;
extern unsigned char sendSeq;

extern volatile unsigned int deviceStatus;


#define SET_DISPLAY_STATUS(value) \
		display.status = ( (unsigned) (value         | \
					DISPLAY_START_ACTIVATION  | \
					DISPLAY_NEEDS_ACTIVATION) );
#define  SET_BIT(var,mask)          (var |= mask)
#define  CLR_BIT(var,mask)          (var &= ~(mask))


#endif // ifndef GLOBALS_H
