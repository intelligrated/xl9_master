//
// Author: Lamar
//


#include "globals.h"
#include "flash.h"
#include "ignoreState.h"
#include "packet.h"
#include "TrakConstants.h"
#include "utils.h"
#include "XLController.h"
#include "SPICommunication.h"


void
IgnoreState ()
{
  if (deviceStatus & DEVICE_STATUS_HAS_MESSAGE)     
  {
    deviceStatus &= (unsigned int) ~DEVICE_STATUS_HAS_MESSAGE; 
    switch (rxPacket.data [PKT_DATA_INDEX])
    {
      case INSTR_DOWNLOAD:
      case INSTR_DOWNLOAD2:
      case INSTR_DOWNLOAD3:
        rxPacket.status = PKT_RX_EMPTY;
        SendAck ();
      break;

      case INSTR_XL_DOWNLOAD4:
        rxPacket.status = PKT_RX_EMPTY;
        SendAck ();
        
        SendCommand2Slave(CMD_INDICATE_DOWNLOAD,0,0,0,0,0);  // start "downloading" display

        _asm ("sei\n"); // DISABLE_INTERRUPTS();
        _asm ("jmp 0xf067\n");  // jump to LoadDownloader (bootstrap.c)
      break;

      default: // ignore all other messages
        rxPacket.status = PKT_RX_EMPTY;
        txPacket.status = PKT_TX_EMPTY;
    }
  } // End of if (deviceStatus & DEVICE_STATUS_HAS_MESSAGE)
} // End of IgnoreState ().
