/* INTERRUPT.C
*
*  All interrupt service routines are located here.
*
*/

#include "iosgb.h"
#include "portbits.h"
#include "globals.h"
#include "TrakConstants.h"



@interrupt void interruptNULL(void) {}
@interrupt void interruptICG(void)
{ // interrupt due to internal clock generator
  ICGS1 |= 1;                     // clear ICG intr flag
}
