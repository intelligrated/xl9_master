/* interruptRTI.c
*/

#include <limits.h>

#include "iosgb.h"
#include "globals.h"
#include "TrakConstants.h"



@interrupt void
interruptRTI (void)
{
/*
  buttons.counter++;
  if (buttons.counter == UINT_MAX)
    buttons.counter = BUTTON_IGNORE_CYCLES;

  display.counter++;

  ltUp.counter++;
  ltDown.counter++;
  lt1.counter++;
  lt2.counter++;
  lt3.counter++;
  lt4.counter++;

  if (currentState == TRAK_CONFIGURED_STATE) { // rotate dots across display
//    if ((display.counter % 63) == 0)  // twice a second
    if ((display.counter % 500) == 0) { // twice a second
      if (display.periodBits < display.size) {
        display.periodBits++;
  
        if (display.periodBits >= display.size)
          display.periodBits = 0;
      
        SET_DISPLAY_STATUS (DISPLAY_SOLID);
      }
    }
  }
*/

  SET_BIT (SRTISC, BIT6); // acknowledge interrupt
}
