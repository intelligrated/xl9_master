// interruptSCI1TX.c
//
// Author: Lamar
//

#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "iosgb.h"
#include "globals.h"
#include "utils.h"



@interrupt void
interruptSCI1_RX ()
{
  unsigned long *addressPtr;
  unsigned char readData;
  unsigned char readSCI1S1;
  unsigned char readSCI1S3;
  struct Packet *buf;
  struct BayDispDevice *bdd;
  unsigned char nextBufferIdx;
  unsigned char i;


  readSCI1S1 = SCI1S1; // read status register
  readData = SCI1D;    // read data
  readSCI1S3 = SCI1C3; // clear the intr flag by reading this reg (9 bit mode)

  buf = &bufPacket [activeBufferIdx];

  if ((txPacket.status & PKT_TX_TRANSMITTING) &&
     (buf->status == PKT_RX_EMPTY))
  {
    // we are receiveing a packet that we are sending
    // this should be the start byte or something went wrong

    if (readSCI1S1 & 0x0F) { // overrun,noise,frame,parity check
      txPacket.status = PKT_TX_COLLIDED;
      goto reset;
    }

    if (readData == PKT_START_BYTE) {
      buf->activeIndex = 0;  
      buf->status = PKT_RX_TX_RECEIVING;
      return;
    }

    txPacket.status = PKT_TX_COLLIDED;
    goto reset;
  } // End of if (txPacket.status == PKT_TX_TRANSMITTING)


  if (buf->status & (unsigned char) PKT_RX_TX_RECEIVING)
  {
    if (buf->activeIndex == txPacket.data [PKT_LENGTH_INDEX]) {
      goto LastCharCheck;
    }

    // we are receiveing a packet that we are sending.
    // this is not the first char and it's not the last char

    if (readSCI1S1 & 0x0F) { // overrun,noise,frame,parity check
      txPacket.status = PKT_TX_COLLIDED;
      goto reset;
    }

    if (buf->status & PKT_RX_ESCAPE)
      buf->status &= (unsigned char) ~PKT_RX_ESCAPE;
    else {
      if (txPacket.data [buf->activeIndex] >= PKT_ESCAPE_BYTE) {
        buf->status |= PKT_RX_ESCAPE;
        return;
      }
    }

    if (readData == txPacket.data [buf->activeIndex])
      buf->activeIndex++;
    else {
      txPacket.status = PKT_TX_COLLIDED;
      goto reset;
    }
    if (buf->activeIndex >= PKT_MAX_LENGTH) {
      txPacket.status = PKT_TX_COLLIDED;
      goto reset;
    }
    return;
  }


  if (buf->status & PKT_RX_TX_RECEIVING) {
LastCharCheck:
    // we are receiveing a packet that we are sending
    // this must be last sent byte, or something when wrong

    if (buf->status != PKT_RX_TX_RECEIVING) {
      txPacket.status = PKT_TX_COLLIDED;
      goto reset;
    }

    if (readSCI1S1 & 0x0F) { // overrun,noise,frame,parity check
      txPacket.status = PKT_TX_COLLIDED;
      goto reset;
    }
  
    if (buf->activeIndex != txPacket.data [PKT_LENGTH_INDEX]) {
      txPacket.status = PKT_TX_COLLIDED;
      goto reset;
    }

    if (readData == PKT_STOP_BYTE) {
      // we believe packet was sent correctly
      txPacket.status = PKT_TX_SENT;
      goto reset;
    }
    txPacket.status = PKT_TX_COLLIDED;
    goto reset;

  } // End of if (packetStatus & PKT_RX_TX_RECEIVING)


  //// If we have a packet that has not been processed
  //if (rxPacket.status == PKT_RX_READ)
  //{
  //  // put a status here and check if this ever occurs
  //  return;
  //}


  //
  // We must be receiving a packet that we are not sending
  //

  if (readSCI1S1 & 0x0F) // overrun,noise,frame,parity check
  {
    goto reset;
  }


  if (buf->status & PKT_RX_ESCAPE)
  {
    buf->status &= (unsigned char) ~PKT_RX_ESCAPE;
  }
  else
  {
    if (readData == PKT_ESCAPE_BYTE)
    {
      buf->status |= PKT_RX_ESCAPE;
      return;
    }
    if (readData == PKT_START_BYTE)
    {
      buf->activeIndex = 0;
      buf->status = PKT_RX_RX_RECEIVING;
      return;
    }
    if (readData == PKT_STOP_BYTE)
    {
      // verify packet.
      if (buf->activeIndex >= PKT_MAX_LENGTH) {
        goto reset;
      }
      buf->data [buf->activeIndex] = 0; // terminate strings at end

      // Did I receive the correct number of bytes?
      if (buf->activeIndex != buf->data [PKT_LENGTH_INDEX])
      {
        goto reset;
      }

      // Is packet long enough?
      if (buf->activeIndex < PKT_DATA_INDEX + 1) // header + cmd
      {
        goto reset;
      }


      // Is packet for me?
      addressPtr = (unsigned long *) &buf->data [PKT_ADDRESS_INDEX];

      if (!(*addressPtr & MASK_BROADCAST)) // if not a broadcast msg    
      {
        if ((*addressPtr != myAddress) && (!IsABayDispAddress(*addressPtr)))
          goto reset; // if not for me and not for the bay disp port, ignore it
      }

      // buffering all broadcast msgs and direct msgs for me or the bay display port

      if (activeBufferIdx == 0) {
        nextBufferIdx = 1;
      } else if (activeBufferIdx == 1) {
        nextBufferIdx = 2;
      } else {
        nextBufferIdx = 0;
      }
      if (bufPacket[nextBufferIdx].status != PKT_RX_EMPTY) {
        goto reset;
      }

      if (buf->data[PKT_DATA_INDEX] == INSTR_ABORT) {
        if (rxPacket.status != PKT_RX_EMPTY) {
          if (rxPacket.data[PKT_DATA_INDEX] == INSTR_ABORT) {
            goto reset; // throw away excessive aborts
          }
        }
      }

      buf->status = PKT_RX_READ;
      activeBufferIdx = nextBufferIdx;
      buf = &bufPacket[activeBufferIdx];
      goto reset;
    }
  } // end of else 
  if (buf->activeIndex >= PKT_MAX_LENGTH)
  {
    goto reset;
  }
  buf->data [buf->activeIndex] = readData;
  buf->activeIndex++;

  return;

reset:
    buf->activeIndex = 0;
    buf->status = PKT_RX_EMPTY;

}// interruptSCI1_RX
