// interruptSCI1TX.c
//
// Author: Lamar
//

#include <limits.h>
#include <string.h>

#include "globals.h"
#include "iosgb.h"
#include "portbits.h"



@interrupt void
interruptSCI1_TX ()
{
  unsigned char readSCI1S1 = SCI1S1;


  if (txPacket.status == PKT_TX_COLLIDED) {
    goto quit;
  }

  if (rxPacket.status == PKT_RX_READ) {
    txPacket.status = PKT_TX_COLLIDED;
    goto quit;
  }

  if (bufPacket [activeBufferIdx].status & PKT_RX_RX_RECEIVING) {
    txPacket.status = PKT_TX_COLLIDED;
    goto quit;
  }

  if (txPacket.status == PKT_TX_READY_TO_SEND) {
    if (SCI1S2 & 1) { // does receive line have traffic?
      txPacket.status = PKT_TX_COLLIDED;
      goto quit;
    }
    
    TXEN1 = 1; // Enable 485 transmitter

    txPacket.status = PKT_TX_TRANSMITTING;
    txPacket.activeIndex = 0;
    SCI1D = PKT_START_BYTE; // Write first byte to buffer
    return;
  }


  if (txPacket.activeIndex < txPacket.data [PKT_LENGTH_INDEX])
  {
    if (txPacket.status & PKT_TX_ESCAPE)
    {
      txPacket.status &= (unsigned char) ~PKT_TX_ESCAPE;
    }
    else
    if (txPacket.data [txPacket.activeIndex] >= PKT_ESCAPE_BYTE)
    {
      txPacket.status |= PKT_TX_ESCAPE;
      SCI1D = PKT_ESCAPE_BYTE;
      return;
    }
    SCI1D = txPacket.data [txPacket.activeIndex]; // Transmit next byte
    txPacket.activeIndex++;
    return;
  }


  if (txPacket.activeIndex == txPacket.data [PKT_LENGTH_INDEX])
  {
    SCI1C2 &= (unsigned char) ~0x80; // Disable transmit empty interrupt
    SCI1C2 |= 0x40; // Enable transmit complete interrupt

    SCI1D = PKT_STOP_BYTE; // Transmit last byte
    txPacket.activeIndex++;
    return;
  }
  

quit:
  txPacket.status &= (unsigned char) ~PKT_TX_TRANSMITTING;

  SCI1C2 &= (unsigned char) ~0x80; // Disable transmit empty interrupt
  SCI1C2 &= (unsigned char) ~0x40; // Disable transmit complete interrupt

  TXEN1 = 0;  // Disable 485 transmitter
  SCI1D = PKT_START_BYTE; // to clear interrupt

}// interruptSCI1_TX 
