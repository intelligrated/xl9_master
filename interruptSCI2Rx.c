// interruptSCI2Rx.c
//
// Author: Lamar, Tom
//

#include <limits.h>
#include <string.h>

#include "globals.h"
#include "interruptSCI2Rx.h"
#include "iosgb.h"


@interrupt void
interruptSCI2_RX ()
{
  unsigned char readData;
  unsigned char readSCI2S1;
  unsigned char readSCI2S3;
  struct Packet *buf;
  unsigned char nextBufferIdx;


  readSCI2S1 = SCI2S1; // read status register
  readData = SCI2D;    // read data
  readSCI2S3 = SCI2C3; // clear the intr flag by reading this reg (9 bit mode)
  
  buf = &bufPacket2 [activeBufferIdx2];

  if (txPacket2.status & PKT_TX_TRANSMITTING) {
    // we are receiveing a packet that we are sending ignore data
    goto reset;
  }

  //
  // We maybe receiving a packet that we are not sending
  //

  if (readSCI2S1 & 0x0F) { // overrun,noise,frame,parity check
    goto reset;
  }

  if (buf->status & PKT_RX_ESCAPE)
  {
    buf->status &= (unsigned char) ~PKT_RX_ESCAPE;
  }
  else
  {
    if (readData == PKT_ESCAPE_BYTE)
    {
      buf->status |= PKT_RX_ESCAPE;
      return;
    }
    if (readData == PKT_START_BYTE)
    {
      buf->activeIndex = 0;
      buf->status = PKT_RX_RX_RECEIVING;
      return;
    }
    if (readData == PKT_STOP_BYTE)
    {
      // verify packet.
      if (buf->activeIndex >= PKT_MAX_LENGTH) {
        goto reset;
      }
      buf->data [buf->activeIndex] = 0; // terminate strings at end

      // Did I receive the correct number of bytes?
      if (buf->activeIndex != buf->data [PKT_LENGTH_INDEX])
      {
        goto reset;
      }

      // Is packet long enough?
      if (buf->activeIndex < PKT_DATA_INDEX + 1) // header + cmd
      {
        goto reset;
      }

      // ALL VALID PACKETS ARE PASSED THRU TO THE LOCAL CONTROLLER

      if (activeBufferIdx2 == 0) {
        nextBufferIdx = 1;
      } else {
        nextBufferIdx = 0;
      }
      if (bufPacket2[nextBufferIdx].status != PKT_RX_EMPTY) {
        goto reset;
      }

      buf->status = PKT_RX_READ;
      activeBufferIdx2 = nextBufferIdx;
      buf = &bufPacket2[activeBufferIdx2];
      goto reset;
    }
  }
  if (buf->activeIndex >= PKT_MAX_LENGTH)
  {
    goto reset;
  }
  if (buf->activeIndex == 0 && buf->status == PKT_RX_EMPTY) {
    goto reset; // wait for start byte
  }
  buf->data [buf->activeIndex] = readData;
  buf->activeIndex++;

  return;

  reset:
    buf->activeIndex = 0;
    buf->status = PKT_RX_EMPTY;

} // end of interruptSCI2_RX


