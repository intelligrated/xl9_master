// interruptSCI2TX.c
//
// Author: Tom (based on Lamar's interruptSCI1TX.c
//

#include <limits.h>
#include <string.h>

#include "globals.h"
#include "iosgb.h"
#include "portbits.h"



@interrupt void
interruptSCI2_TX ()
{
  unsigned char readSCI2S1 = SCI2S1;

  if (txPacket2.status == PKT_TX_READY_TO_SEND) {
    TXEN2 = 1; // Enable 485 transmitter2

    txPacket2.status = PKT_TX_TRANSMITTING;
    txPacket2.activeIndex = 0;
    SCI2D = PKT_START_BYTE; // Write first byte to buffer
    return;
  }


  if (txPacket2.activeIndex < txPacket2.data [PKT_LENGTH_INDEX])
  {
    if (txPacket2.status & PKT_TX_ESCAPE)
    {
      txPacket2.status &= (unsigned char) ~PKT_TX_ESCAPE;
    }
    else
    if (txPacket2.data [txPacket2.activeIndex] >= PKT_ESCAPE_BYTE)
    {
      txPacket2.status |= PKT_TX_ESCAPE;
      SCI2D = PKT_ESCAPE_BYTE;
      return;
    }
    SCI2D = txPacket2.data [txPacket2.activeIndex]; // Transmit next byte
    txPacket2.activeIndex++;
    return;
  }


  if (txPacket2.activeIndex == txPacket2.data [PKT_LENGTH_INDEX])
  {
    SCI2C2 &= (unsigned char) ~0x80; // Disable transmit empty interrupt
    SCI2C2 |= 0x40; // Enable transmit complete interrupt

    SCI2D = PKT_STOP_BYTE; // Transmit last byte
    txPacket2.activeIndex++;
    return;
  }
  

quit:
  txPacket2.status = PKT_TX_EMPTY;

  SCI2C2 &= (unsigned char) ~0x80; // Disable transmit empty interrupt
  SCI2C2 &= (unsigned char) ~0x40; // Disable transmit complete interrupt

  TXEN2 = 0; // Disable 485 transmitter2
  SCI2D = PKT_START_BYTE; // to clear interrupt

}// interruptSCI2_TX 
