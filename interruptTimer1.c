// interruptTimer1.c
//
// Author: Lamar
//

#include <limits.h>

#include "globals.h"
#include "iosgb.h"



@interrupt void
interruptTPM1_OV (void)
{
  timer1_ctr++;
  //buttons.counter++;
  //if (buttons.counter == UCHAR_MAX)
    //buttons.counter = BUTTON_IGNORE_CYCLES;

/*  display.counter++;

  ltDown.counter++;
  ltUp.counter++;
  lt1.counter++;
  lt2.counter++;
  lt3.counter++;
  lt4.counter++;
  clScrollCounter++;

  if (currentState == TRAK_CONFIGURED_STATE) { // rotate dots across display
    if ((display.counter % 10) == 0) { // twice a second
      if (display.periodBits < display.size) {
        display.periodBits++;
 
        if (display.periodBits >= display.size)
          display.periodBits = 0;
 
        SET_DISPLAY_STATUS (DISPLAY_SOLID);
      }
    }
  }
  */
  // TPM1SC;                 // read timer status register, done below
  TPM1SC &= (unsigned char) ~(0x80);

}// interruptTPM1_OV
