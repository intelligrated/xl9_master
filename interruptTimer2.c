// interruptTimer2.c
//
// Author: Lamar
//

#include <limits.h>

#include "globals.h"
#include "iosgb.h"




@interrupt void
interruptTPM2_OV (void)
{
 
  if (delay1Cycles == 0) {
    CLR_BIT (deviceStatus, (unsigned int)DEVICE_STATUS_DELAYING);
  } else {
    delay1Cycles--;
  }

  if (delay2Cycles == 0) {
    CLR_BIT (deviceStatus, (unsigned int)DEVICE_STATUS_DELAYING2);
  } else {
    delay2Cycles--;
  }

  if (delay3Cycles == 0) {
  CLR_BIT (deviceStatus, (unsigned int)DEVICE_STATUS_DELAYING3);
  } else {
  delay3Cycles--;
  }

  if (slaveCommTimer > 0)
    slaveCommTimer--;
  
  
  //TPM2SC;                 // read timer status register (done in following ststr)
  TPM2SC &= (unsigned char) ~(0x80);

}// interruptTPM2_OV
