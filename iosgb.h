#ifndef IOSGB_H
#define IOSGB_H

/*	IO DEFINITIONS FOR MC9S08GB60
 *	Copyright (c) 2003 by COSMIC Software
 */
#define uint	unsigned int

/*	PORTS section
 */
volatile char PTAD     @0x00;	/* port A data */
volatile char PTAPE    @0x01;	/* port A pull-up enable */
volatile char PTASE    @0x02;	/* port A slow rate enable */
volatile char PTADD    @0x03;	/* port A direction */
volatile char PTBD     @0x04;	/* port B data */
volatile char PTBPE    @0x05;	/* port B pull-up enable */
volatile char PTBSE    @0x06;	/* port B slow rate enable */
volatile char PTBDD    @0x07;	/* port B direction */
volatile char PTCD     @0x08;	/* port C data */
volatile char PTCPE    @0x09;	/* port C pull-up enable */
volatile char PTCSE    @0x0a;	/* port C slow rate enable */
volatile char PTCDD    @0x0b;	/* port C direction */
volatile char PTDD     @0x0c;	/* port D data */
volatile char PTDPE    @0x0d;	/* port D pull-up enable */
volatile char PTDSE    @0x0e;	/* port D slow rate enable */
volatile char PTDDD    @0x0f;	/* port D direction */
volatile char PTED     @0x10;	/* port E data */
volatile char PTEPE    @0x11;	/* port E pull-up enable */
volatile char PTESE    @0x12;	/* port E slow rate enable */
volatile char PTEDD    @0x13;	/* port E direction */

/*	INTERRUPT/KEYBOARD section
 */
volatile char IRQSC    @0x14;	/* IRQ status/control */
volatile char KBISC    @0x16;	/* Keyboard status/control */
volatile char KBIPE    @0x17;	/* Keyboard pin enable */

/*	SCI section
 */
volatile uint SCI1BD   @0x18;	/* SCI 1 baud rate */
volatile char SCI1BDH  @0x18;	/* SCI 1 baud rate high */
volatile char SCI1BDL  @0x19;	/* SCI 1 baud rate low */
volatile char SCI1C1   @0x1a;	/* SCI 1 control 1 */
volatile char SCI1C2   @0x1b;	/* SCI 1 control 2 */
volatile char SCI1S1   @0x1c;	/* SCI 1 status 1 */
volatile char SCI1S2   @0x1d;	/* SCI 1 status 2 */
volatile char SCI1C3   @0x1e;	/* SCI 1 control 3 */
volatile char SCI1D    @0x1f;	/* SCI 1 data */

volatile uint SCI2BD   @0x20;	/* SCI 2 baud rate */
volatile char SCI2BDH  @0x20;	/* SCI 2 baud rate high */
volatile char SCI2BDL  @0x21;	/* SCI 2 baud rate low */
volatile char SCI2C1   @0x22;	/* SCI 2 control 1 */
volatile char SCI2C2   @0x23;	/* SCI 2 control 2 */
volatile char SCI2S1   @0x24;	/* SCI 2 status 1 */
volatile char SCI2S2   @0x25;	/* SCI 2 status 2 */
volatile char SCI2C3   @0x26;	/* SCI 2 control 3 */
volatile char SCI2D    @0x27;	/* SCI 2 data */

/*	SPI section
 */
volatile char SPIC1    @0x28;	/* SPI control 1 */
volatile char SPIC2    @0x29;	/* SPI control 2 */
volatile char SPIBR    @0x2a;	/* SPI baud rate */
volatile char SPIS     @0x2b;	/* SPI status */
volatile char SPID     @0x2d;	/* SPI data */

/*	TIMER 1 section
 */
volatile char TPM1SC   @0x30;	/* timer 1 status/control */
volatile uint TPM1CNT  @0x31;	/* timer 1 counter */
volatile char TPM1CNTH @0x31;	/* timer 1 counter high */
volatile char TPM1CNTL @0x32;	/* timer 1 counter low */
volatile uint TPM1MOD  @0x33;	/* timer 1 modulo */
volatile char TPM1MODH @0x33;	/* timer 1 modulo high */
volatile char TPM1MODL @0x34;	/* timer 1 modulo low */
volatile char TPM1C0SC @0x35;	/* timer 1 chan 0 status/control */
volatile uint TPM1C0V  @0x36;	/* timer 1 chan 0 */
volatile char TPM1C0VH @0x36;	/* timer 1 chan 0 high */
volatile char TPM1C0VL @0x37;	/* timer 1 chan 0 low */
volatile char TPM1C1SC @0x38;	/* timer 1 chan 1 status/control */
volatile uint TPM1C1V  @0x39;	/* timer 1 chan 1 */
volatile char TPM1C1VH @0x39;	/* timer 1 chan 1 high */
volatile char TPM1C1VL @0x3a;	/* timer 1 chan 1 low */
volatile char TPM1C2SC @0x3b;	/* timer 1 chan 2 status/control */
volatile uint TPM1C2V  @0x3c;	/* timer 1 chan 2 */
volatile char TPM1C2VH @0x3c;	/* timer 1 chan 2 high */
volatile char TPM1C2VL @0x3d;	/* timer 1 chan 2 low */

/*	PORTS section
 */
volatile char PTFD     @0x40;	/* port F data */
volatile char PTFPE    @0x41;	/* port F pull-up enable */
volatile char PTFSE    @0x42;	/* port F slow rate enable */
volatile char PTFDD    @0x43;	/* port F direction */
volatile char PTGD     @0x44;	/* port G data */
volatile char PTGPE    @0x45;	/* port G pull-up enable */
volatile char PTGSE    @0x46;	/* port G slow rate enable */
volatile char PTGDD    @0x47;	/* port G direction */

/*	CLOCK GENERATOR section
 */
volatile char ICGC1    @0x48;	/* clock gen control 1 */
volatile char ICGC2    @0x49;	/* clock gen control 2 */
volatile char ICGS1    @0x4a;	/* clock gen status 1 */
volatile char ICGS2    @0x4b;	/* clock gen status 2 */
volatile char ICGFLTU  @0x4c;	/* clock gen filter upper */
volatile char ICGFLTL  @0x4d;	/* clock gen filter lower */
volatile char ICGTRM   @0x4e;	/* clock gen trim */

/*	ANALOG/DIGTAL section
 */
volatile char ATDC     @0x50;	/* A/D control */
volatile char ATDSC    @0x51;	/* A/D status/control */
volatile uint ATDR     @0x52;	/* A/D data */
volatile char ATDRH    @0x52;	/* A/D data high */
volatile char ATDRL    @0x53;	/* A/D data low */
volatile char ATDPE    @0x54;	/* A/D pin enable */

/*	IIC section
 */
volatile char IICA     @0x58;	/* IIC address */
volatile char IICF     @0x59;	/* IIC frequency divider */
volatile char IICC     @0x5a;	/* IIC control */
volatile char IICS     @0x5b;	/* IIC status */
volatile char IICD     @0x5c;	/* IIC data */

/*	TIMER 2 section
 */
volatile char TPM2SC   @0x60;	/* timer 2 status/control */
volatile uint TPM2CNT  @0x61;	/* timer 2 counter */
volatile char TPM2CNTH @0x61;	/* timer 2 counter high */
volatile char TPM2CNTL @0x62;	/* timer 2 counter low */
volatile uint TPM2MOD  @0x63;	/* timer 2 modulo */
volatile char TPM2MODH @0x63;	/* timer 2 modulo high */
volatile char TPM2MODL @0x64;	/* timer 2 modulo low */
volatile char TPM2C0SC @0x65;	/* timer 2 chan 0 status/control */
volatile uint TPM2C0V  @0x66;	/* timer 2 chan 0 */
volatile char TPM2C0VH @0x66;	/* timer 2 chan 0 high */
volatile char TPM2C0VL @0x67;	/* timer 2 chan 0 low */
volatile char TPM2C1SC @0x68;	/* timer 2 chan 1 status/control */
volatile uint TPM2C1V  @0x69;	/* timer 2 chan 1 */
volatile char TPM2C1VH @0x69;	/* timer 2 chan 1 high */
volatile char TPM2C1VL @0x6a;	/* timer 2 chan 1 low */
volatile char TPM2C2SC @0x6b;	/* timer 2 chan 2 status/control */
volatile uint TPM2C2V  @0x6c;	/* timer 2 chan 2 */
volatile char TPM2C2VH @0x6c;	/* timer 2 chan 2 high */
volatile char TPM2C2VL @0x6d;	/* timer 2 chan 2 low */
volatile char TPM2C3SC @0x6e;	/* timer 2 chan 3 status/control */
volatile uint TPM2C3V  @0x6f;	/* timer 2 chan 3 */
volatile char TPM2C3VH @0x6f;	/* timer 2 chan 3 high */
volatile char TPM2C3VL @0x70;	/* timer 2 chan 3 low */
volatile char TPM2C4SC @0x71;	/* timer 2 chan 4 status/control */
volatile uint TPM2C4V  @0x72;	/* timer 2 chan 4 */
volatile char TPM2C4VH @0x72;	/* timer 2 chan 4 high */
volatile char TPM2C4VL @0x73;	/* timer 2 chan 4 low */

/*	SYSTEM section
 */
volatile char SRS    @0x1800;	/* system reset status */
volatile char SBDFR  @0x1801;	/* system background debug force reset */
volatile char SOPT   @0x1802;	/* system options */
volatile uint SDID   @0x1806;	/* system device identification */
volatile char SDIDH  @0x1806;	/* system device identification high */
volatile char SDIDL  @0x1807;	/* system device identification low */
volatile char SRTISC @0x1808;	/* system real-time status/control */
volatile char SPMSC1 @0x1809;	/* system power management status/control 1 */
volatile char SPMSC2 @0x180a;	/* system power management status/control 2 */

/*	DEBUG section
 */
volatile uint DBGCA  @0x1810;	/* DBG comparator A */
volatile uint DBGCB  @0x1812;	/* DBG comparator B */
volatile char DBGFH  @0x1814;	/* DBG fifo high */
volatile char DBGFL  @0x1815;	/* DBG fifo low */
volatile char DBGC   @0x1816;	/* DBG control */
volatile char DBGT   @0x1817;	/* DBG trigger */
volatile char DBGS   @0x1818;	/* DBG status */

/*	FLASH section
 */
volatile char FCDIV  @0x1820;	/* FLASH clock divider */
volatile char FOPT   @0x1821;	/* FLASH options */
volatile char FCNFG  @0x1823;	/* FLASH configuration */
volatile char FPROT  @0x1824;	/* FLASH protection */
volatile char FSTAT  @0x1825;	/* FLASH status */
volatile char FCMD   @0x1826;	/* FLASH command */

#undef uint


#endif // IOSGB_H
