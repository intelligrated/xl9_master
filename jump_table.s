;
; Jump table implementation for EP track interrupts
; Author: Steven Reed-Pittman
;


;-------------------------------------------------
; External references
;-------------------------------------------------
	xref _interruptNULL
	xref _interruptRTI
        xref _interruptIIC
        xref _interruptATD
        xref _interruptKeyboard
        xref _interruptSCI2_TX
        xref _interruptSCI2_RX
        xref _interruptSCI2_ERR
        xref _interruptSCI1_TX
        xref _interruptSCI1_RX
        xref _InterruptSCI1_ERR
        xref _interruptSPI
        xref _interruptTPM2_OV
        xref _interruptTPM2_CH4
        xref _InterruptTPM2_CH3
        xref _interruptTPM2_CH2
        xref _interruptTPM2_CH1
        xref _interruptTIM2_CH0
        xref _interruptTPM1_OV
        xref _interruptTPM1_CH2
        xref _interruptTPM1_CH1
        xref _interruptTIM1_CH0
        xref _interruptICG
        xref _interruptLVD
        xref _interruptMultidrop
        xref _interruptSoftware
;-------------------------------------------------

RTI:   section +abs
  org 0xEE00
  jmp _interruptRTI

IIC:      section +abs
  org 0xEE04
  jmp _interruptNULL

ADT:      section +abs
  org 0xEE08
  jmp _interruptNULL

KEYBOARD:      section +abs
  org 0xEE0C
  jmp _interruptNULL

SCI2_TX:      section +abs
  org 0xEE10
  jmp _interruptSCI2_TX

SCI2_RX:      section +abs
  org 0xEE14
  jmp _interruptSCI2_RX

SCI2_ERR:      section +abs
  org 0xEE18
  jmp _interruptNULL

SCI1_TX:      section +abs
  org 0xEE1C
  jmp _interruptSCI1_TX

SCI1_RX:      section +abs
  org 0xEE20
  jmp _interruptSCI1_RX

SCI1_ERR:      section +abs
  org 0xEE24
  jmp _interruptNULL

SPI:      section +abs
  org 0xEE28
  jmp _interruptSPI

TPM2_OV:      section +abs
  org 0xEE2C
  jmp _interruptTPM2_OV

TPM2_CH4:      section +abs
  org 0xEE30
  jmp _interruptNULL

TPM2_CH3:      section +abs
  org 0xEE34
  jmp _interruptNULL

TPM2_CH2:      section +abs
  org 0xEE38
  jmp _interruptNULL

TPM2_CH1:      section +abs
  org 0xEE3C
  jmp _interruptNULL

TPM2_CH0:      section +abs
  org 0xEE40
  jmp _interruptNULL

TPM1_OV:      section +abs
  org 0xEE44
  jmp _interruptTPM1_OV

TPM1_CH2:      section +abs
  org 0xEE48
  jmp _interruptNULL

TPM1_CH1:      section +abs
  org 0xEE4C
  jmp _interruptNULL

TPM1_CH0:      section +abs
  org 0xEE50
  jmp _interruptNULL

ICG:      section +abs
  org 0xEE54
  jmp _interruptICG

LVD:      section +abs
  org 0xEE58
  jmp _interruptNULL

EXT_IRQ:      section +abs
  org 0xEE5C
  jmp _interruptNULL

SOFTWARE:      section +abs
  org 0xEE60
  jmp _interruptNULL

	end
