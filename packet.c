//
// Author: Lamar
//
//

#include <stdlib.h>
#include <string.h>

#include "globals.h"
#include "iosgb.h"
#include "packet.h"
#include "portbits.h"
#include "structs.h"
#include "utils.h"


@tiny static unsigned char activeTxBufIdx;
@tiny static unsigned char processBufIdx;
@tiny static unsigned char processBufIdx2;      
@tiny static unsigned char txBufferIdx;
@tiny static unsigned char txBufferIdx2;
static struct Packet txBuffers[3];
static struct Packet txBuffers2[4];
      

void
InitPacketVars() {
  activeTxBufIdx = 0;
  processBufIdx = 0;
  processBufIdx2 = 0;
  txBufferIdx = 0;
  txBufferIdx2 = 0;
  memset(&txBuffers, 0, sizeof(txBuffers));
  memset(&txBuffers2, 0, sizeof(txBuffers2));
} 


void
IncrementProcessBufIdx() {
  if (processBufIdx == 0) {
    processBufIdx = 1;
  } else if (processBufIdx == 1) {
    processBufIdx = 2;
  } else {
    processBufIdx = 0;
  }
}

void
IncrementProcessBufIdx2(void) {
  if (processBufIdx2 == 0) {
    processBufIdx2 = 1;
  } else {
    processBufIdx2 = 0;
  }
}


void
IncrementTxBufferIdx(void) {
  if (txBufferIdx == 0) {
    txBufferIdx = 1;
  } if (txBufferIdx == 1) {
    txBufferIdx = 2;
  } else {
    txBufferIdx = 0;
  }
}


void
IncrementTxBufferIdx2(void) {
  if (txBufferIdx2 == 0) {
    txBufferIdx2 = 1;
  } else if (txBufferIdx2 == 1) {
    txBufferIdx2 = 2;
  } else if (txBufferIdx2 == 2) {
    txBufferIdx2 = 3;
  } else {
    txBufferIdx2 = 0;
  }
}


struct Packet *
GetQueuedTxBuffer() {
  unsigned char i;
  unsigned idx = txBufferIdx;

  for (i = 0; i < 3; ++i) {
    idx++;
    if (idx == 3) {
      idx = 0;
    }
    if (txBuffers[idx].status == PKT_TX_QUEUED) {
      return &txBuffers[idx];
    }
  }

  return NULL;

}// GetQueuedTxBuffer


struct Packet *
GetQueuedTxBuffer2() {
  unsigned char i;
  unsigned idx2 = txBufferIdx2;

  for (i = 0; i < 4; ++i) {
    idx2++;
    if (idx2 == 4) {
      idx2 = 0;
    }
    if (txBuffers2[idx2].status == PKT_TX_QUEUED) {
      return &txBuffers2[idx2];
    }
  }

  return NULL;

}// GetQueuedTxBuffer2


struct Packet *
GetTxBuffer() {
  if (txBuffers[txBufferIdx].status == PKT_TX_EMPTY) {
    return &txBuffers[txBufferIdx];
  }
  return NULL;
}


struct Packet *
GetTxBuffer2(void) {
  if (txBuffers2[txBufferIdx2].status == PKT_TX_EMPTY) {
    return &txBuffers2[txBufferIdx2];
  }
  return NULL;
}


unsigned char
QueueTxPacket(struct Packet *txBuffer) {
  if (&txBuffers[txBufferIdx] == txBuffer) {
    txBuffers[txBufferIdx].status = PKT_TX_QUEUED;
    IncrementTxBufferIdx();
    return TRUE;
  }
  return FALSE;
}


unsigned char
QueueTxPacket2(struct Packet *txBuffer2) {
  if (&txBuffers2[txBufferIdx2] == txBuffer2) {
    txBuffers2[txBufferIdx2].status = PKT_TX_QUEUED;
    IncrementTxBufferIdx2();
    return TRUE;
  }
  return FALSE;
}


void
BuildPacket (void)
{
  unsigned char i;

  *((unsigned long *)(&txPacket.data [PKT_ADDRESS_INDEX])) = myAddress; // restore local address
  // header + cmd
  txPacket.data [PKT_LENGTH_INDEX] = INSTR_INDEX + 1;

  txPacket.data [PKT_LENGTH_INDEX] += msgDataLength;

  txPacket.data [PKT_CHECKSUM_INDEX] = 0;
  for (i = 1; i < txPacket.data [PKT_LENGTH_INDEX]; i++) //skip checksum byte
    txPacket.data [PKT_CHECKSUM_INDEX] ^= txPacket.data [i];

}// BuildPacket


void
BuildPacket2 (void)
{
  unsigned char i;


  // header + cmd
  txPacket2.data [PKT_LENGTH_INDEX] = INSTR_INDEX + 1;

  txPacket2.data [PKT_LENGTH_INDEX] += msgDataLength2;

  txPacket2.data [PKT_CHECKSUM_INDEX] = 0;
  for (i = 1; i < txPacket2.data [PKT_LENGTH_INDEX]; i++) //skip checksum byte
    txPacket2.data [PKT_CHECKSUM_INDEX] ^= txPacket2.data [i];

}// BuildPacket2


// the bufPacket queue contains packets that are 
//  1. direct messages for either the XL controller or for devices on the bay display port
//  2. broadcast messages (that may or may not apply to any devices here)
//
struct Packet*
GetPacket1 (void) {
  unsigned char checksum;
  unsigned char i;
  unsigned long *addressPtr;

  if (!(bufPacket[processBufIdx].status & PKT_RX_READ)) {
    return (struct Packet*)NULL;  // buffer is empty
  }

  // Is the checksum correct?
  checksum = bufPacket[processBufIdx].data [PKT_CHECKSUM_INDEX];
  for (i=PKT_CHECKSUM_INDEX + 1; i < bufPacket[processBufIdx].activeIndex;++i) {
    checksum ^= bufPacket[processBufIdx].data[i];
  }
  if (checksum) {
    goto reset;    // bad checksum, discard msg
  }
  
  addressPtr =(unsigned long*)&bufPacket[processBufIdx].data[PKT_ADDRESS_INDEX];

  if (IsABayDispAddress(*addressPtr)) {
    struct Packet *txBuffer2 = GetTxBuffer2();
    if (txBuffer2 != NULL) {
      memcpy(txBuffer2, &bufPacket[processBufIdx],
             bufPacket[processBufIdx].data[PKT_LENGTH_INDEX] + 2);
      QueueTxPacket2(txBuffer2);

      if (txBuffer2->data[INSTR_INDEX] == INSTR_ACK) {
        txPacket.status = PKT_TX_EMPTY;   // cancel resend of pkt to controller
      }
    }
    goto reset;
  }

  deviceStatus &= (unsigned int) ~DEVICE_STATUS_RECV_BROADCAST; // assume it's not a broadcast msg
  if (*addressPtr & MASK_BROADCAST) {
    struct Packet *txBuffer2 = GetTxBuffer2();
    if (txBuffer2 != NULL) {
      memcpy(txBuffer2, &bufPacket[processBufIdx],
             bufPacket[processBufIdx].data[PKT_LENGTH_INDEX] + 2);
      QueueTxPacket2(txBuffer2);
    }

    // if it's a broadcast msg AND and an Abort msg AND we are in the Pick State, accept it so that
    // all pick zones & sections can be checked (see PickState() )

    if ((bufPacket[processBufIdx].data [PKT_DATA_INDEX] == INSTR_ABORT) &&
        (currentState == TRAK_PICK_STATE))
    {
      memcpy(&rxPacket, &bufPacket[processBufIdx],
                     bufPacket[processBufIdx].data[PKT_LENGTH_INDEX] + 2);
      bufPacket[processBufIdx].status = PKT_RX_EMPTY;
      IncrementProcessBufIdx();
      txPacket.data [PKT_SEQUENCE_INDEX] = rxPacket.data [PKT_SEQUENCE_INDEX];
      deviceStatus |= (unsigned int) DEVICE_STATUS_RECV_BROADCAST;  // broadcast msg is for me
      return &rxPacket;
    }

    // check if broadcast msg is for this device (XL)
    if ((*addressPtr & MASK_CLASS) != 0) // is it for my class?
    {
      if (((*addressPtr >> 16) & myClass) != myClass)
        goto reset;
    }

    if ((*addressPtr & MASK_TYPE) != 0) // is it for my type?
    {
      if ((*addressPtr & MASK_TYPE) != (myAddress & MASK_TYPE))
        goto reset;
    }

    if ((*addressPtr & MASK_SECTION) != 0) // is it for my section?
    {
      if ((unsigned int)(*addressPtr & MASK_SECTION) != mySection)
      {
        if ((unsigned int)(*addressPtr & MASK_SECTION) != // my zone?
           (myZone | MASK_ZONE_BIT))
          goto reset;
      }
    }
    deviceStatus |= (unsigned int) DEVICE_STATUS_RECV_BROADCAST;  // broadcast msg is for me
  }

  memcpy(&rxPacket, &bufPacket[processBufIdx],
                     bufPacket[processBufIdx].data[PKT_LENGTH_INDEX] + 2);
  bufPacket[processBufIdx].status = PKT_RX_EMPTY;
  IncrementProcessBufIdx();

  txPacket.data [PKT_SEQUENCE_INDEX] = rxPacket.data [PKT_SEQUENCE_INDEX];
  return &rxPacket;

reset:
  bufPacket[processBufIdx].status = PKT_RX_EMPTY;
  IncrementProcessBufIdx();
  return (struct Packet*)NULL; 

}// GetPacket1


struct Packet*
GetPacket2 (void) {     // get a packet received by SCI2 (bay display mode)
                        // return a NULL if no packet available or bad checksum
  unsigned char checksum;
  unsigned char i;
  unsigned long *addressPtr;

  if (!(bufPacket2[processBufIdx2].status & PKT_RX_READ)) {
    return (struct Packet*)NULL;
  }

  // Is the checksum correct?
  checksum = bufPacket2[processBufIdx2].data [PKT_CHECKSUM_INDEX];
  for (i=PKT_CHECKSUM_INDEX + 1; i < bufPacket2[processBufIdx2].activeIndex; ++i){
    checksum ^= bufPacket2[processBufIdx2].data[i];
  }
  if (checksum) {
    bufPacket2[processBufIdx2].status = PKT_RX_EMPTY;
    IncrementProcessBufIdx2();
    return (struct Packet*)NULL; // checksum failed, return a NULL
  }

  memcpy(&rxPacket2, &bufPacket2[processBufIdx2],
                     bufPacket2[processBufIdx2].data[PKT_LENGTH_INDEX] + 2);
  bufPacket2[processBufIdx2].status = PKT_RX_EMPTY;
  IncrementProcessBufIdx2();

  txPacket2.data [PKT_SEQUENCE_INDEX] = rxPacket2.data [PKT_SEQUENCE_INDEX];
  return &rxPacket2;

}// GetPacket2

void
SendAck (void)
{
  if ((deviceStatus & DEVICE_STATUS_RECV_BROADCAST)) {
    txPacket.status = PKT_TX_EMPTY;
    return;
  }
  txPacket.data [INSTR_INDEX] = INSTR_ACK;
  msgDataLength = 0;
  BuildPacket ();
  SendOnce ();
}

void
SendAck2 (unsigned long bay_disp_address)   // sends an ACK to the bay display port
{
  txPacket2.data [INSTR_INDEX] = INSTR_ACK;
  *((unsigned long *)(&txPacket2.data [PKT_ADDRESS_INDEX])) = bay_disp_address;
  msgDataLength2 = 0;
  BuildPacket2();   // determine length and calculate checksum
  SendOnce2();

}


void
SendDebug(unsigned char num, const char *str) {
  txPacket.data[INSTR_INDEX] = INSTR_DEBUG;
  txPacket.data[SEQUENCE_INDEX] = sendSeq++;
  txPacket.data[MSG_DEBUG_NUM] = num;
  StringCopy(&txPacket.data[MSG_DEBUG_STR], str);
  msgDataLength = (unsigned char) (1 + strlen(str) + 1);
  BuildPacket();
  SendOnce();
}


void
SendErrorState (void)
{
  txPacket.data [INSTR_INDEX] = INSTR_ERROR;
  txPacket.data [SEQUENCE_INDEX] = sendSeq++;
  txPacket.data [MSG_ERROR_NUM] = ERROR_SET_STATE;
  StringCopy (&txPacket.data [MSG_ERROR_STR], "bad state=");
  NbrToStr (rxPacket.data [MSG_SET_STATE_STATE], 10);
  strcat (&txPacket.data [MSG_ERROR_STR], nbrToStr);
  msgDataLength = (unsigned char) (1+strlen (&txPacket.data [MSG_ERROR_STR])+1);
  BuildPacket ();
  SendOnce ();
}


void
SendOnce ()   // 
{
  SendPacket ();
  if (txPacket.status != PKT_TX_SENT)
    SendPacket ();
  txPacket.status = PKT_TX_EMPTY; // no more tries
}


void                  
SendOnce2(void) {   // transmits packets addressed to the bay display via SCI2
  // txPacket2 statuses leaving this function are:
  // PKT_TX_EMPTY
  // PKT_TX_READY_TO_SEND;

  txPacket2.status = PKT_TX_READY_TO_SEND;

  // packets read are more important than packets to send
  if (rxPacket2.status == PKT_RX_READ) {
    return;
  }

  // send the packet
  SET_BIT (SCI2C2, 0x80); // Enable SCI2 transmit interrupt

  // Wait until we are no longer transmitting before continuing.
  while (txPacket2.status & PKT_TX_TRANSMITTING)
    ;

  TXEN2 = 0; // Disable 485 transmitter
  SCI2D = PKT_START_BYTE; // to clear Transmit Complete Interrupt

}// SendOnce2


void
SendPacket (void)
{
  // txPacket statuses leaving this function are:
  // PKT_TX_READY_TO_SEND
  // PKT_TX_SENT

  static unsigned char collisions; // do not initialized, helps ramdomize start


  //BuildPacket ();   // removed this so SendPacket can send Bay Display Port Packets

  if (sendSeq >= PKT_ESCAPE_BYTE)
    sendSeq = 1; // to stop escapes because of the sequence #

  // CD: Collision Detection
  // CSMA: Carrier Sense Multiple Access Protocol. We use nonpersistent CSMA,
  // where we do not send immediately after a busy line becomes free we wait
  // a random period of time.  This leads to better channel utilization.
  // CSMA/CD: Carrier Sense Multiple Access Protocol with Collision Detection.
  // This is what we use.  IEEE 802.3 is a version of CSMA/CD.

  // packets read are more important than packets to send
  if (rxPacket.status == PKT_RX_READ)
  {
    txPacket.status = PKT_TX_READY_TO_SEND;
    return;
  }

  if (bufPacket [activeBufferIdx].status & PKT_RX_RX_RECEIVING) {
    collisions++;
    if (collisions > 10) // to avoid lockup, when partial packet received.
    {
      collisions = 0;
      if (bufPacket [activeBufferIdx].status & PKT_RX_RX_RECEIVING) {
        bufPacket [activeBufferIdx].status = PKT_RX_EMPTY;
      }
    }
    RandomDelayNoWait ();
    txPacket.status = PKT_TX_READY_TO_SEND;
    return;
  }

  if (collisions) 
    RandomDelayWait ();
  

  if (SCI1S2 & 1) // does receive line have traffic?
  {
    collisions++;
    if (collisions > 10) {
      collisions = 0;
      srand (txPacket.data [PKT_CHECKSUM_INDEX]);
    }
    RandomDelayNoWait ();
    txPacket.status = PKT_TX_READY_TO_SEND;
    return;
  }

  // try to send the packet
  txPacket.status = PKT_TX_READY_TO_SEND;
  SET_BIT (SCI1C2, 0x80); // Enable transmit interrupt

  // Wait until we are no longer transmitting before continuing.
  while (txPacket.status & PKT_TX_TRANSMITTING)
    ;

  TXEN1 = 0; // Disable 485 transmitter
  SCI1D = PKT_START_BYTE; // to clear Transmit Complete Interrupt


  // packets read are more important than packets to send
  if (rxPacket.status == PKT_RX_READ)
  {
    txPacket.status = PKT_TX_READY_TO_SEND; // keep trying
    return;
  }

  // packet sent successfully
  if (txPacket.status == PKT_TX_SENT)
  {
    lastSendSeq = txPacket.data [PKT_SEQUENCE_INDEX];
    collisions = 0;
    MilliSecondDelayNoWait (60); // start timer for resend
    return;
  }

  collisions++;
  if (collisions > 10)
  {
    collisions = 0;
    srand (txPacket.data [PKT_CHECKSUM_INDEX]);
  }

  if (txPacket.status == 0) {
    if (nbrOfSends != 255) {
      nbrOfSends++;
    }
  }

  RandomDelayNoWait ();
  txPacket.status = PKT_TX_READY_TO_SEND; // keep trying

}// SendPacket



void
SendUnconfigured (void)
{
  txPacket.data [INSTR_INDEX] = INSTR_UNCONFIGURED;
  txPacket.data [SEQUENCE_INDEX] = sendSeq++;
  txPacket.data [MSG_UNCONFIGURED_BOOT_STATUS] = bootStatus;
  msgDataLength = 1;
  //txPacket.data [MSG_UNCONFIGURED_KEY_VALUE] = buttons.value;
  txPacket.data [MSG_UNCONFIGURED_KEY_VALUE] = 0;
  msgDataLength += 1;
  txPacket.data [MSG_UNCONFIGURED_CHECKSUM_M] = checkSumMainCode;
  msgDataLength += 1;
  txPacket.data [MSG_UNCONFIGURED_CHECKSUM_B] = checkSumBootStrap;
  msgDataLength += 1;
  StringCopy (&txPacket.data [MSG_UNCONFIGURED_VERSION], XLVersionAlpha);
  strcat (&txPacket.data [MSG_UNCONFIGURED_VERSION], XLVersionNumeric);
  strcat(&txPacket.data[MSG_UNCONFIGURED_VERSION], XLRelease);
  msgDataLength += (unsigned char)
                   (strlen (&txPacket.data [MSG_UNCONFIGURED_VERSION]) + 1);
  BuildPacket ();
  SendPacket ();
}


void
BroadcastReset2BayDisp() // broadcasts the RESET instruction via the bay display port
{                                
  txPacket2.data [INSTR_INDEX] = INSTR_RESET;
  txPacket2.data [SEQUENCE_INDEX] = 0;
  *((unsigned long *)(&txPacket2.data [PKT_ADDRESS_INDEX])) = MASK_BROADCAST;
  msgDataLength2 = 0;
  BuildPacket2();   // insert msg length & calculate checksum

  SendOnce2();      // no response will be sent
  SendOnce2();      // no response will be sent
}

void
BroadcastNotConfigured2BayDisp() // broadcasts the NOT_CONFIGURED instruction via the bay display port
{                                // note that there is a response to this cmd only if the device is unconfigured
  txPacket2.data [INSTR_INDEX] = INSTR_NOT_CONFIGURED;
  txPacket2.data [SEQUENCE_INDEX] = 1;
  *((unsigned long *)(&txPacket2.data [PKT_ADDRESS_INDEX])) = MASK_BROADCAST;
  msgDataLength2 = 0;
  BuildPacket2();   // insert msg length & calculate checksum
  SendOnce2();
  SendOnce2();
}

