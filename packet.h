#ifndef PACKET_H
#define PACKET_H
//
// Author: Lamar
//  
  
  

/////////////
// Includes
/////////////
#include "structs.h"

  
///////////////
// Prototypes
/////////////// 
void IncrementProcessBufIdx(void);
struct Packet *GetPacket1 (void);
struct Packet *GetPacket2 (void);
struct Packet *GetQueuedTxBuffer(void);
struct Packet *GetQueuedTxBuffer2(void);
struct Packet *GetTxBuffer(void);
void InitPacketVars(void);
unsigned char QueueTxPacket(struct Packet *);
void SendAck (void);
void SendAck2 (unsigned long bay_disp_address);
void SendOnce (void);
void SendOnce2(void);
void SendPacket (void);
void SendUnconfigured (void);
void BroadcastReset2BayDisp(void);
void BroadcastNotConfigured2BayDisp (void);
void BuildPacket (void);
void BuildPacket2 (void);

#endif
