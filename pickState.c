//
// Author: Tom
//

#include <limits.h>
#include <string.h>

#include "pickState.h"
#include "globals.h"
#include "packet.h"
#include "SPICommunication.h"
#include "XLConstants.h"
#include "utils.h"

// button values
//   1 -> S1 button closest to downstream (controller side) of stick
//   2 -> S2 button next to S1
//   4 -> S3 button next to S2
//   8 -> S4 button closest to upstream (controller side) of stick


static unsigned char gIport = 0;


void
PickState(void)   {
  unsigned char deviceIdx, iport, stick_count, SWvalue, switchIndex;
  unsigned long exceptAddr, msgAddr;

  if (deviceStatus & DEVICE_STATUS_HAS_MESSAGE) {
    deviceStatus &= (unsigned int) ~DEVICE_STATUS_HAS_MESSAGE;
    switch (rxPacket.data [PKT_DATA_INDEX]) {
      case INSTR_ABORT:
        rxPacket.status = PKT_RX_EMPTY;
        msgAddr = *((unsigned long *)(&rxPacket.data [PKT_ADDRESS_INDEX]));
        exceptAddr = *((unsigned long *)(&rxPacket.data [MSG_ABORT_EXCEPT_ADDRESS]));
        if (exceptAddr == myAddress) {
          break;
        }
        
        if (deviceStatus & DEVICE_STATUS_RECV_BROADCAST) {
          if (((msgAddr & MASK_CLASS) != 0) &&
              (((msgAddr & MASK_CLASS) >> 16) == myClass))
            goto XLabort;

          if (((msgAddr & MASK_TYPE) != 0) &&
              ((msgAddr & MASK_TYPE) == (myAddress & MASK_TYPE)))
            goto XLabort;
          
          if ((msgAddr & MASK_SECTION) != 0)
          { 
            if ((unsigned int)(msgAddr & MASK_SECTION) == (myZone | MASK_ZONE_BIT))
              goto XLabort;
            
            for (iport = 0; iport < MAX_XL_PORTS; iport++)
            {
              if (((msgAddr & MASK_SECTION) == pick[iport].section) ||
                  (msgAddr & MASK_SECTION) == (pick[iport].zone | MASK_ZONE_BIT))
              {     // abort pick on this port
                SendCommand2Slave(CMD_PORT_CLEAR, iport, 0, 0, 0, 0);   // turn off all LEDs on this port
                memset(&pick[iport], 0, sizeof(struct XLPickData)); // clear out pick data 
              }
            }
          }
          break;
        } else { // end of if (broadcast msg)
          SendAck();
          goto XLabort;
        }
      break;  // end of case ABORT

      case INSTR_XLABORT_PORT:
        iport = (unsigned char) (rxPacket.data[MSG_XLABORT_PORT_PORT] - 1);
        rxPacket.status = PKT_RX_EMPTY;
        if (iport < MAX_XL_PORTS) {
          SendAck();
          SendCommand2Slave(CMD_PORT_CLEAR, iport, 0, 0, 0, 0);   // turn off all LEDs on this port
          memset(&pick[iport], 0, sizeof(struct XLPickData)); // clear out pick data 
        } else {
          SendError("bad xl port=", rxPacket.data[MSG_XLABORT_PORT_PORT]);
        }
      break;

      case INSTR_ACK:
        if (txPacket.data[INSTR_INDEX] != INSTR_XLCOMPLETED_PICK) {
          rxPacket.status = PKT_RX_EMPTY;
          txPacket.status = PKT_TX_EMPTY;
          break;
        }

        if (rxPacket.data[PKT_SEQUENCE_INDEX] == lastSendSeq) {
          txPacket.status = PKT_TX_EMPTY;
          SendCommand2Slave(CMD_PORT_CLEAR, gIport, 0, 0, 0, 0);   // turn off all LEDs on this port
          memset(&pick[gIport], 0, sizeof(struct XLPickData)); // clear out pick data so it doesn't happen again
        }
        rxPacket.status = PKT_RX_EMPTY;
      break; // case INSTR_ACK:

      default:
        deviceStatus |= DEVICE_STATUS_HAS_MESSAGE;

    } // end of switch
  } // end of if (DEVICE_STATUS_HAS_MESSAGE)

  if (txPacket.status == PKT_TX_EMPTY)
  {
    if (ButtonMailBox.status == MB_FULL)
    {
      SWvalue = ButtonMailBox.value;
      gIport = ButtonMailBox.portIdx;
      deviceIdx = ButtonMailBox.deviceIdx;
      ButtonMailBox.status = MB_EMPTY;
    
      switchIndex = GetPBSindex(XLConfigPort[gIport].deviceType[0], deviceIdx, SWvalue);
      
      if ((switchIndex >= pick[gIport].begIdx) && (switchIndex <= pick[gIport].endIdx))
      {
        txPacket.data [PKT_SEQUENCE_INDEX] = sendSeq++;
        txPacket.data [PKT_DATA_INDEX] = INSTR_XLCOMPLETED_PICK;
        txPacket.data[MSG_XLCOMPLETED_PICK_PORT] = (unsigned char) (gIport + 1);
        *((unsigned long *)(&(txPacket.data [MSG_XLCOMPLETED_PICK_SEQ]))) = pick[gIport].seqNum;
        msgDataLength = 5;
        BuildPacket ();
        SendPacket();
      }
    } // end of if (ButtonMailBox.status == MB_FULL
  }  // end of if (txPacket.status == PKT_TX_EMPTY

  for (iport = 0; iport < MAX_XL_PORTS; iport++) // check if any active picks
    if (pick[iport].color != 0)
      break;
  
  if (iport == MAX_XL_PORTS)
  {                  // no active picks remain -> exit PICK State
    if (mySection > 0)
      SetState(TRAK_CONFIGURED_STATE);
    else
      SetState(TRAK_UNCONFIGURED_STATE);
    
  }
  // else remain in TRAK_PICK_STATE  -> active picks present
  return;

XLabort: // abort all picks and leave the pick state
  SendCommand2Slave(CMD_DISPLAY_CLEAR, 0, 0, 0, 0, 0);   // turn off all LEDs
  for (iport = 0; iport < MAX_XL_PORTS; iport++)
    memset(&pick[iport], 0, sizeof(struct XLPickData)); // clear out pick data
    
  if (mySection > 0)
    SetState(TRAK_CONFIGURED_STATE);
  else 
    SetState(TRAK_UNCONFIGURED_STATE);

}// PickState
