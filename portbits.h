#ifndef PORTBITS_H
#define PORTBITS_H

// make 68hcs08 PORTs bit addressable
#include "iosgb.h"


_Bool PA0    @PTAD:0;
_Bool PA1    @PTAD:1;
_Bool PA2    @PTAD:2;
_Bool PA3    @PTAD:3;
_Bool PA4    @PTAD:4;
_Bool PA5    @PTAD:5;
_Bool PA6    @PTAD:6;
_Bool PA7    @PTAD:7;

_Bool PB0    @PTBD:0;
_Bool PB1    @PTBD:1;
_Bool PB2    @PTBD:2;
_Bool PB3    @PTBD:3;
_Bool PB4    @PTBD:4;
_Bool PB5    @PTBD:5;
_Bool PB6    @PTBD:6;
_Bool PB7    @PTBD:7;

_Bool PC0    @PTCD:0;
_Bool PC1    @PTCD:1;
_Bool PC2    @PTCD:2;
_Bool PC3    @PTCD:3;
_Bool PC4    @PTCD:4;
_Bool PC5    @PTCD:5;
_Bool PC6    @PTCD:6;
_Bool PC7    @PTCD:7;

_Bool PD0    @PTDD:0;
_Bool PD1    @PTDD:1;
_Bool PD2    @PTDD:2;
_Bool PD3    @PTDD:3;
_Bool PD4    @PTDD:4;
_Bool PD5    @PTDD:5;
_Bool PD6    @PTDD:6;
_Bool PD7    @PTDD:7;

_Bool PE0    @PTED:0;
_Bool PE1    @PTED:1;
_Bool PE2    @PTED:2;
_Bool PE3    @PTED:3;
_Bool PE4    @PTED:4;
_Bool PE5    @PTED:5;
_Bool PE6    @PTED:6;
_Bool PE7    @PTED:7;

_Bool PF0    @PTFD:0;
_Bool PF1    @PTFD:1;
_Bool PF2    @PTFD:2;
_Bool PF3    @PTFD:3;
_Bool PF4    @PTFD:4;
_Bool PF5    @PTFD:5;
_Bool PF6    @PTFD:6;
_Bool PF7    @PTFD:7;

_Bool PG0    @PTGD:0;
_Bool PG1    @PTGD:1;
_Bool PG2    @PTGD:2;
_Bool PG3    @PTGD:3;
_Bool PG4    @PTGD:4;
_Bool PG5    @PTGD:5;
_Bool PG6    @PTGD:6;
_Bool PG7    @PTGD:7;



#endif // PORTBITS_H
