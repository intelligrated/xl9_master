//
// Author: Lamar
//


////////////
// Headers
////////////
#include <string.h>

#include "globals.h"
#include "packet.h"
#include "processMsgs.h"
#include "utils.h"
#include "XLConstants.h"
#include "SPICommunication.h"


void
AbortMsg() {
  unsigned port;
  unsigned long exceptAddr;

  exceptAddr = *((unsigned long*)(&rxPacket.data [MSG_ABORT_EXCEPT_ADDRESS]));

  rxPacket.status = PKT_RX_EMPTY;
  txPacket.status = PKT_TX_EMPTY;

  if (deviceStatus & DEVICE_STATUS_RECV_BROADCAST) {
    if ((exceptAddr & MASK_BROADCAST)) { // broadcast exceptAddr?
      if (((exceptAddr & MASK_CLASS) != 0) &&
          (((exceptAddr & MASK_CLASS) >> 16) != myClass))
        goto Abort;

      if (((exceptAddr & MASK_TYPE) != 0) &&
          ((exceptAddr & MASK_TYPE) != (myAddress & MASK_TYPE)))
        goto Abort;

      if (((exceptAddr & MASK_SECTION) != 0) &&
          ((unsigned)(exceptAddr & MASK_SECTION) != mySection))
        goto Abort;

      return;
    } else {
      if (myAddress == exceptAddr) {
        return;
      }
    }
  }
  else {
    SendAck();
  }

Abort: // perform the abort
  SendCommand2Slave(CMD_DISPLAY_CLEAR, 0, 0, 0, 0, 0);
  for (port = 0; port < MAX_XL_PORTS; port++) {
    memset(&pick[port], 0, sizeof(struct XLPickData));
  }

  if (mySection > 0) {
    SetState(TRAK_CONFIGURED_STATE);
  }
  else {
    SetState(TRAK_UNCONFIGURED_STATE);
  }
}// AbortMsg


void
ConfigureMsg(void) {
  myZone = *((unsigned int *) &rxPacket.data[MSG_CONFIGURE_ZONE]);
  mySection = *((unsigned int *) &rxPacket.data[MSG_CONFIGURE_SECTION]);

  rxPacket.status = PKT_RX_EMPTY;
  SendAck ();
  currentState = TRAK_CONFIGURED_STATE;
}


void
XLConfigMsg(void) {
    unsigned char portIdx, i, deviceType, nbrOfDevices;

    myZone = *((unsigned int *) &rxPacket.data[MSG_XLCONFIG_ZONE]);
    mySection = *((unsigned int *) &rxPacket.data[MSG_XLCONFIG_SECTION]);

    for (portIdx = 0; portIdx < MAX_XL_PORTS; portIdx++) {
        deviceType = rxPacket.data[MSG_XLCONFIG_TYPE1 + portIdx];
        nbrOfDevices = rxPacket.data[MSG_XLCONFIG_NBR_PORT1 + portIdx];
        
        if (nbrOfDevices > MAX_XL_STICKS_PER_PORT) {
            XLConfigPort[portIdx].deviceCount = 0;
            rxPacket.status = PKT_RX_EMPTY;
            SendError("illegal nbr of devices", nbrOfDevices);
            return;
        }
        else {
            XLConfigPort[portIdx].deviceCount = nbrOfDevices;
        }

        if (nbrOfDevices > 0) {
            if ((deviceType == XL_BL_1_BUTTON)     ||
                (deviceType == XL_BL_2_BUTTON)     ||
                (deviceType == XL_9STICK_W_BUTTONS) ||
                (deviceType == XL_9STICK_WO_BUTTONS)||
                (deviceType == XL_12STICK_W_BUTTONS)||
                (deviceType == XL_12STICK_WO_BUTTONS)) {
                for (i = 0; i < nbrOfDevices; i++) {
                    XLConfigPort[portIdx].deviceType[i] = deviceType;
                }
            }
            else {
                XLConfigPort[portIdx].deviceCount = 0;
                rxPacket.status = PKT_RX_EMPTY;
                SendError("illegal device type", deviceType);
                return;
            }
        }
        SendPortDevices2Slave(CMD_SET_PORT_DEVICES, portIdx, nbrOfDevices,
                              deviceType);
    }
    
    rxPacket.status = PKT_RX_EMPTY;
    SendAck();

    currentState = TRAK_CONFIGURED_STATE;

}// XLConfigMsg 


void
GetStatusMsg (void)
{
  txPacket.data [PKT_DATA_INDEX] = INSTR_STATUS;
  txPacket.data[PKT_SEQUENCE_INDEX] = rxPacket.data[PKT_SEQUENCE_INDEX];
  rxPacket.status = PKT_RX_EMPTY;
  *((unsigned int*)(&(txPacket.data [MSG_STATUS_ZONE]))) = myZone;
  *((unsigned int*)(&(txPacket.data [MSG_STATUS_SECTION]))) = mySection;
  txPacket.data [MSG_STATUS_BOOT] = bootStatus;
  txPacket.data [MSG_STATUS_STATE] = currentState;
  msgDataLength = 6;
  txPacket.data [MSG_STATUS_CHECKSUM_M] = checkSumMainCode;
  msgDataLength += 1;
  txPacket.data [MSG_STATUS_CHECKSUM_B] = checkSumBootStrap;
  msgDataLength += 1;
  StringCopy (&txPacket.data [MSG_STATUS_VERSION], XLVersionAlpha);
  strcat (&txPacket.data [MSG_STATUS_VERSION], XLVersionNumeric);
  strcat(&txPacket.data[MSG_STATUS_VERSION], XLRelease);
  msgDataLength += (unsigned char)
                   (strlen (&txPacket.data [MSG_STATUS_VERSION]) + 1);
  BuildPacket ();

  SendOnce ();

}// GetStatusMsg


void
PingMsg (void)
{
  rxPacket.status = PKT_RX_EMPTY;
  if (mySection == 0 || myZone == 0) {
    GetStatusMsg ();
    return;
  }

  txPacket.data [INSTR_INDEX] = INSTR_PING;
  msgDataLength = 0;
  BuildPacket ();
  SendOnce ();
}


void
SelfTestMsg (void) {
  rxPacket.status = PKT_RX_EMPTY;
  txPacket.status = PKT_TX_EMPTY;
  SendAck ();
  SetState(TRAK_DIAG_STATE); // SetState calls ActivateDiagState
}                           // which sends command to slave CPU


void
SetStateMsg (void)
{
  if (rxPacket.data [MSG_SET_STATE_STATE] >= TRAK_NUMBER_OF_STATES) {
    rxPacket.status = PKT_RX_EMPTY;
    SendError ("bad state=", rxPacket.data [MSG_SET_STATE_STATE]);
    return;
  }

  SetState(rxPacket.data [MSG_SET_STATE_STATE]);
  rxPacket.status = PKT_RX_EMPTY;
  SendAck ();

}// SetStateMsg


// function: GenericMsg
// desc:     for the XL/SL controller, this function is only useful during installation
//           
void
GenericMsg (void)
{
  //unsigned char lightsByte;   // tells you which lights to update
  //unsigned char port, device, idx;
  //unsigned char deviceCount, color;
  //unsigned char beaconClearFlag;
  //unsigned char ltDownStatus, ltUpStatus, lt1Status;
  //unsigned char lt2Status, lt3Status, lt4Status;

  // Lamar 20161010 currently I'm supporting 9 inch only
  unsigned char color, port, device, deviceCount;

  color = rxPacket.data[MSG_GENERIC_LTDOWN_COLOR];
  for (port = 0; port < MAX_XL_PORTS; port++) {
    deviceCount = XLConfigPort[port].deviceCount;
    if (deviceCount > 0) {
      //SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, 0, color, rxPacket.data[MSG_GENERIC_LTDOWN_STATUS], (deviceCount*9)-1);
      SendCommand2Slave(CMD_COLOR_FILL_PORT, port, 0, color, rxPacket.data[MSG_GENERIC_LTDOWN_STATUS], 0);
    }
  }

/* Lamar 20161010
  lightsByte = rxPacket.data [MSG_GENERIC_LIGHTS_BYTE];
  if (lightsByte == 0)   // if none specified, update all 6 lights
    lightsByte = MSG_GENERIC_LIGHTS_BYTE_LTDN |
                 MSG_GENERIC_LIGHTS_BYTE_LTUP |
                 MSG_GENERIC_LIGHTS_BYTE_LT1  |
                 MSG_GENERIC_LIGHTS_BYTE_LT2  |
                 MSG_GENERIC_LIGHTS_BYTE_LT3  |
                 MSG_GENERIC_LIGHTS_BYTE_LT4;

 
  // button light down - direct to 1st half of stick lights
  
  if (lightsByte & MSG_GENERIC_LIGHTS_BYTE_LTDN)
  {
    color = rxPacket.data[MSG_GENERIC_LTDOWN_COLOR];
    for (port = 0; port < MAX_XL_PORTS; port++) 
    {
      deviceCount = XLConfigPort[port].deviceCount;
      if ((XLConfigPort[port].deviceType[0] == XL_BL_1_BUTTON) ||
          (XLConfigPort[port].deviceType[0] == XL_BL_2_BUTTON))
      {
        for (idx = 0; idx < MAX_XL_BUTTONLIGHTS_PER_PORT; idx++) // need to update all devices on this port: all even # indexes
        {
          if ((idx % 2) == 0)   // if even
          {
            //UpdateBLmemory(port, idx, rxPacket.data [MSG_GENERIC_LTDOWN_COLOR], rxPacket.data [MSG_GENERIC_LTDOWN_STATUS]);
            SendCommand2Slave(CMD_LED_RAM_UPDATE1, port, idx, color, rxPacket.data [MSG_GENERIC_LTDOWN_STATUS], 0);
          }
        }
      }
      else if ((XLConfigPort[port].deviceType[0] == XL_12STICK_W_BUTTONS) ||
          (XLConfigPort[port].deviceType[0] == XL_12STICK_WO_BUTTONS))
      {
        for (device = 0; device < deviceCount; device++)
        {
          SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12), color, rxPacket.data[MSG_GENERIC_LTDOWN_STATUS], (device*12)+5);
        }
      }
      else    // all that's left is 9 inch sticks
      {
        for (device = 0; device < deviceCount; device++)
        {
          SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12), color, rxPacket.data[MSG_GENERIC_LTDOWN_STATUS], (device*12)+4);
        }
      }
    } // end of for (port=...
  } // end of light down

  // button light up - direct to last half of stick lights
  
  if (lightsByte & MSG_GENERIC_LIGHTS_BYTE_LTUP)
  {
    color = rxPacket.data[MSG_GENERIC_LTUP_COLOR];
    for (port = 0; port < MAX_XL_PORTS; port++)   // index 1 (and all odd indices) is up
    {
      deviceCount = XLConfigPort[port].deviceCount;
      if ((XLConfigPort[port].deviceType[0] == XL_BL_1_BUTTON) ||
          (XLConfigPort[port].deviceType[0] == XL_BL_2_BUTTON))
      {
        for (idx = 1; idx < MAX_XL_BUTTONLIGHTS_PER_PORT; idx++)
        {
          if ((idx % 2) == 1) // if odd
          {
            //UpdateBLmemory(port, idx, rxPacket.data [MSG_GENERIC_LTUP_COLOR], rxPacket.data [MSG_GENERIC_LTUP_STATUS]);
            SendCommand2Slave(CMD_LED_RAM_UPDATE1, port, idx, color, rxPacket.data [MSG_GENERIC_LTUP_STATUS], 0);
          }
        }
      }
      else if ((XLConfigPort[port].deviceType[0] == XL_12STICK_W_BUTTONS) ||
          (XLConfigPort[port].deviceType[0] == XL_12STICK_WO_BUTTONS))
      {
        for (device = 0; device < deviceCount; device++)
        {
          SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12)+6, color, rxPacket.data[MSG_GENERIC_LTUP_STATUS], (device*12)+11);
        }
      }
      else    // all that's left is 9 inch sticks
      {
        for (device = 0; device < deviceCount; device++)
        {
          SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12)+6, color, rxPacket.data[MSG_GENERIC_LTUP_STATUS], (device*12)+8);
        }
      }
    } // end of for (port=
  } // end of
Lamar 20161010 */
  
/*  // remaining light fields are not used in the beacon test

  // 1st group of lights from the left (led 0, 1, 2) surrounding PB SW 1

  beaconClearFlag = FALSE;    // on beacon test, all lights are either on or off
  color = rxPacket.data[MSG_GENERIC_LT1_COLOR];
  if (lightsByte & MSG_GENERIC_LIGHTS_BYTE_LT1)  
    for (port = 0; port < MAX_XL_PORTS; port++)
    {
      if ((XLConfigPort[port].deviceType[0] != XL_BL_1_BUTTON) &&
          (XLConfigPort[port].deviceType[0] != XL_BL_2_BUTTON))
      {
        deviceCount = XLConfigPort[port].deviceCount;
        for (device = 0; device < deviceCount; device++)
        {
          if (color == 0)
          {
            beaconClearFlag = TRUE;     // indicate this is a clear operation (if running beacon test)
            //SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device * 12), color, rxPacket.data [MSG_GENERIC_LT1_STATUS], (device*12)+2);
            SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12), rxPacket.data[MSG_GENERIC_LTDOWN_COLOR], rxPacket.data[MSG_GENERIC_LTDOWN_STATUS], (device*12)+2);
          }
          else if (i == 0)
          {
            //SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device * 12), color, rxPacket.data [MSG_GENERIC_LT1_STATUS], (device*12)+2);
            SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12), rxPacket.data[MSG_GENERIC_LTDOWN_COLOR], rxPacket.data[MSG_GENERIC_LTDOWN_STATUS], (device*12)+2);
          }
        }
      }
    }
    
  // 2nd group of lights from the left (led 3, 4, 5) surrounding PB SW 2

  color = rxPacket.data [MSG_GENERIC_LT2_COLOR];
  if (lightsByte & MSG_GENERIC_LIGHTS_BYTE_LT2)  
    for (port = 0; port < MAX_XL_PORTS; port++)
    {
      if ((XLConfigPort[port].deviceType[0] != XL_BL_1_BUTTON) &&
          (XLConfigPort[port].deviceType[0] != XL_BL_2_BUTTON))
      {
        deviceCount = XLConfigPort[port].deviceCount;
        for (device = 0; device < deviceCount; device++)
        {
          if ((color == 0) || (i == 1))
            //SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device * 12)+3, color, rxPacket.data [MSG_GENERIC_LT2_STATUS], (device*12)+5);
            SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12)+3, rxPacket.data[MSG_GENERIC_LTUP_COLOR], rxPacket.data[MSG_GENERIC_LTUP_STATUS], (device*12)+5);
        }
      }
    }
  // 3rd group of lights from the left (led 6, 7, 8) surrounding PB SW 3
  color = rxPacket.data [MSG_GENERIC_LT3_COLOR];  
  if (lightsByte & MSG_GENERIC_LIGHTS_BYTE_LT3)  
    for (port = 0; port < MAX_XL_PORTS; port++)
    {
      if ((XLConfigPort[port].deviceType[0] != XL_BL_1_BUTTON) &&
          (XLConfigPort[port].deviceType[0] != XL_BL_2_BUTTON))
      {
        deviceCount = XLConfigPort[port].deviceCount;
        for (device = 0; device < deviceCount; device++)
        {
          if ((color == 0) || (i == 2))
            //SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device * 12)+6, color, rxPacket.data [MSG_GENERIC_LT3_STATUS], (device*12)+8);
            SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12)+6, rxPacket.data[MSG_GENERIC_LTDOWN_COLOR], rxPacket.data[MSG_GENERIC_LTDOWN_STATUS], (device*12)+8);
        }
      }
    }

  // 4th group of lights from the left (led 9, 10, 11) surrounding PB SW 4  (only on 12" sticks)
  
  color = rxPacket.data [MSG_GENERIC_LT4_COLOR];
  if (lightsByte & MSG_GENERIC_LIGHTS_BYTE_LT4)  
    for (port = 0; port < MAX_XL_PORTS; port++)
    {
      if ((XLConfigPort[port].deviceType[0] == XL_12STICK_W_BUTTONS) ||
          (XLConfigPort[port].deviceType[0] == XL_12STICK_WO_BUTTONS))
      {
        deviceCount = XLConfigPort[port].deviceCount;
        for (device = 0; device < deviceCount; device++)
        {
          if ((color == 0) || (i == 3))
            //SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device * 12)+9, color, rxPacket.data [MSG_GENERIC_LT4_STATUS], (device*12)+11);
            SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, (device*12)+9, rxPacket.data[MSG_GENERIC_LTUP_COLOR], rxPacket.data[MSG_GENERIC_LTUP_STATUS], (device*12)+11);
        }
      }
    }
*/
  SendCommand2Slave(CMD_UPDATE_ALL_PORTS, 0, 0, 0, 0, 0);
  
/*
  if (beaconClearFlag == FALSE)   // don't modify if this was a clear operation
    if (++i > 3)
      i = 0;

  currentState = rxPacket.data [MSG_GENERIC_STATE];
  if ((currentState == 0) || (currentState >= TRAK_NUMBER_OF_STATES)) 
    currentState = TRAK_GENERIC_STATE;
*/

  currentState = TRAK_CONFIGURED_STATE;

  rxPacket.status = PKT_RX_EMPTY;
  SendAck ();

}// GenericMsg


void
XLPickMsg (void)
{
  unsigned char led_idx, deviceCount, pickPort, rem;
  struct Packet tmpPacket;
 
  memcpy(&tmpPacket, &rxPacket, sizeof(rxPacket));
  rxPacket.status = PKT_RX_EMPTY;

  pickPort = tmpPacket.data[MSG_XLPICK_PORT];
  
  if ((pickPort == 0) || (pickPort > MAX_XL_PORTS))
  {
    SendError("Port = 0 or > Max", pickPort);
    return;
  }
  pickPort--;     // convert port nbr to indx

  if (pick[pickPort].color != 0)
  {
    SendError("Port already has a pick", pickPort+1);
    return;
  }
  pick[pickPort].seqNum = (*((unsigned long*)(&(tmpPacket.data[MSG_XLPICK_SEQ]))));
  pick[pickPort].begIdx = tmpPacket.data[MSG_XLPICK_BEG_LED_NUM]; 
  pick[pickPort].endIdx = tmpPacket.data[MSG_XLPICK_END_LED_NUM];
  pick[pickPort].color = tmpPacket.data[MSG_XLPICK_LED_COLOR];
  pick[pickPort].status = tmpPacket.data[MSG_XLPICK_LED_STATUS];
  pick[pickPort].zone = (*((unsigned int*)(&(tmpPacket.data[MSG_XLPICK_ZONE]))));
  pick[pickPort].section = (*((unsigned int*)
                             (&(tmpPacket.data[MSG_XLPICK_SECTION]))));

  if (pick[pickPort].endIdx < pick[pickPort].begIdx)
  {
    SendError("end nbr < beg nbr", pick[pickPort].endIdx);
    return;
  }
  pick[pickPort].begIdx--;    // convert # to index
  pick[pickPort].endIdx--;
  deviceCount = XLConfigPort[pickPort].deviceCount;

  if ((XLConfigPort[pickPort].deviceType[0] == XL_BL_1_BUTTON) ||   // an XL port will contain either all button lights OR all stick lights
      (XLConfigPort[pickPort].deviceType[0] == XL_BL_2_BUTTON))
  {
    if (pick[pickPort].endIdx >= (deviceCount * MAX_XL_RGBLEDS_PER_BUTTONLIGHT))
    {
      SendError("Light number > Max for Button Light Count", pick[pickPort].endIdx+1);
      return;
    }
  }
  else if ((XLConfigPort[pickPort].deviceType[0] == XL_12STICK_W_BUTTONS) ||   // an XL port will contain either all button lights OR all stick lights
      (XLConfigPort[pickPort].deviceType[0] == XL_12STICK_WO_BUTTONS))    // stick lights
  {
    if (pick[pickPort].endIdx >= (deviceCount * MAX_XL_RGBLEDS_PER_12STICK))
    {
      SendError("Light number > Max for Stick Count", pick[pickPort].endIdx+1);
      return;
    }
  }
  else    // only device type left is 9 inch sticks, with or without buttons
  {
    if (pick[pickPort].endIdx >= (deviceCount * MAX_XL_RGBLEDS_PER_9STICK))
    {
      SendError("Light number > Max for Stick Count", pick[pickPort].endIdx+1);
      return;
    }
  }

  SendAck();

  SendCommand2Slave(CMD_LED_RAM_UPDATEx, pickPort, pick[pickPort].begIdx, pick[pickPort].color, pick[pickPort].status, pick[pickPort].endIdx);    
      
  SendCommand2Slave(CMD_PORT_UPDATE, pickPort, 0, 0, 0, 0);  // update physical LEDs  
    
  SetState(TRAK_PICK_STATE);

}// XLPickMsg


void
XLGetPortDevicesMsg (void)
{
  unsigned char port, i, j;


  port = rxPacket.data[MSG_XLGET_PORT_NUM];
  rxPacket.status = PKT_RX_EMPTY;

  if ((port == 0) || (port > MAX_XL_PORTS))
  {
    SendError("Port = 0 or > Max", port);
    return;
  }
  // assume Master CPUs XLConfigPort[] array has been updated

  txPacket.data[INSTR_INDEX] = INSTR_XLPORT_DEVICES;
  txPacket.data[MSG_XLPORT_PORT_NUM] = port;
  port--;     // convert port number to port index
  txPacket.data[MSG_XLPORT_NUM_DEVICES] = XLConfigPort[port].deviceCount;
  j = MSG_XLPORT_DEVICE1;

  for (i = 0; i < XLConfigPort[port].deviceCount; i++)
  {
    txPacket.data[j++] = XLConfigPort[port].deviceType[i];
  }

  msgDataLength = XLConfigPort[port].deviceCount;
  msgDataLength += 2;   // for port num and num of devices
  BuildPacket ();
  SendPacket ();

}

void
XLSetPortDevicesMsg (void)     // allows the local controller to define the port config, 
{                               // over-writing auto-config (device count & device types)
  unsigned char port, num_of_devices, i;
  unsigned char deviceType = 0;

  rxPacket.status = PKT_RX_EMPTY;
/*  if (runAutoConfig)
  {                                                     
    SendError ("Auto Config Active", 0);
    return;
  }
*/
  port = rxPacket.data[MSG_XLSET_PORT_NUM];
  num_of_devices = rxPacket.data[MSG_XLSET_NUM_DEVICES];

  if ((port == 0) || (port > MAX_XL_PORTS))
  {
    SendError("Port = 0 or > Max", port);
    return;
  }

  if ((num_of_devices == 0) || (num_of_devices > MAX_XL_BUTTONLIGHTS_PER_PORT))
  {
    SendError("# of devices = 0 or > Max", num_of_devices);
    return;
  }
  port--;   // convert port # to port index

  for (i = 0; i < num_of_devices; i++)
  {
    deviceType = rxPacket.data[MSG_XLSET_DEVICE1 + i];
    if ((deviceType == XL_BL_1_BUTTON)     ||
        (deviceType == XL_BL_2_BUTTON)     ||
        (deviceType == XL_9STICK_W_BUTTONS) ||
        (deviceType == XL_9STICK_WO_BUTTONS)||
        (deviceType == XL_12STICK_W_BUTTONS)||
        (deviceType == XL_12STICK_WO_BUTTONS)) 
    {
      XLConfigPort[port].deviceType[i] = deviceType;
    }
    else
    {
      XLConfigPort[port].deviceCount = 0;
      SendError("illegal device type", deviceType);
      return;
    }
  }
  XLConfigPort[port].deviceCount = num_of_devices;
  SendPortDevices2Slave(CMD_SET_PORT_DEVICES, port, num_of_devices, deviceType);
  SendAck ();

}


void
XLSingleLightMsg(void)
{
  unsigned char port, led_num, led_color, led_status;
  unsigned char deviceCount;


  // sets the color of the LED on the specified port

  port = rxPacket.data[MSG_XLSINGLE_LIGHT_PORT];
  led_num = rxPacket.data[MSG_XLSINGLE_LIGHT_NUMBER];
  led_color = rxPacket.data[MSG_XLSINGLE_LIGHT_COLOR];
  led_status = rxPacket.data[MSG_XLSINGLE_LIGHT_STATUS];

  rxPacket.status = PKT_RX_EMPTY;

  if ((port == 0) || (port > MAX_XL_PORTS))
  {
    SendError("Port = 0 or > Max", port);
    return;
  }
  if (led_num == 0)
  {
    SendError("Led Num = 0", led_num);
    return;
  }
  port--;         // convert port from number to index
  led_num--;      // convert led num from number to index
  deviceCount = XLConfigPort[port].deviceCount;

  
  if ((XLConfigPort[port].deviceType[0] == XL_BL_1_BUTTON) ||   // an XL port will contain either all button lights OR all stick lights
      (XLConfigPort[port].deviceType[0] == XL_BL_2_BUTTON))
  {
    if (led_num >= (deviceCount * MAX_XL_RGBLEDS_PER_BUTTONLIGHT))
    {
      SendError("LED num > Max for Button Light Count", led_num+1);
      return;
    }
  }
  else if ((XLConfigPort[port].deviceType[0] == XL_12STICK_W_BUTTONS) ||
           (XLConfigPort[port].deviceType[0] == XL_12STICK_WO_BUTTONS))    // we have stick lights
  {
    if (led_num >= (deviceCount * MAX_XL_RGBLEDS_PER_12STICK))
    {
      SendError("LED num > Max for Stick Count", led_num+1);
      return;
    }
  }
  else  // only device type left is 9 inch stick
  {
    if (led_num >= (deviceCount * MAX_XL_RGBLEDS_PER_9STICK))
    {
      SendError("LED num > Max for Stick Count", led_num+1);
      return;
    }
  }

  SendAck();

  SendCommand2Slave(CMD_LED_RAM_UPDATE1, port, led_num, led_color, led_status, 0);    
  
  SendCommand2Slave(CMD_PORT_UPDATE, port, 0, 0, 0, 0);  // update physical LEDs
  
}


void
XLRangeColorMsg(void) {
  unsigned char portIdx, lightStatus, beginIdx, endIdx;
  unsigned char deviceCount;
  unsigned char buttonColor, lightColor;

  // sets the color of a range of LEDs on the specified port
  portIdx = rxPacket.data[MSG_XLRANGE_COLOR_PORT];
  beginIdx = rxPacket.data[MSG_XLRANGE_COLOR_BEG_NUMBER];
  endIdx = rxPacket.data[MSG_XLRANGE_COLOR_END_NUMBER];
  buttonColor = rxPacket.data[MSG_XLRANGE_COLOR_BUTTON];
  lightColor = rxPacket.data[MSG_XLRANGE_COLOR_LIGHT];
  lightStatus = rxPacket.data[MSG_XLRANGE_COLOR_STATUS];

  rxPacket.status = PKT_RX_EMPTY;

  if ((portIdx == 0) || (portIdx > MAX_XL_PORTS)) {
    SendError("Port = 0 or > Max", portIdx);
    return;
  }

  portIdx--;   // convert port from number to index
  deviceCount = XLConfigPort[portIdx].deviceCount;
  
  if (beginIdx == 0) {
    SendError("Beg Number = 0", beginIdx);
    return;
  }
  if (endIdx == 0) {
    SendError("End Number = 0", endIdx);
    return;
  }
  beginIdx--;    // convert from number to index
  endIdx--;

  if ((XLConfigPort[portIdx].deviceType[0] == XL_BL_1_BUTTON) ||   // an XL port will contain either all button lights OR all stick lights
      (XLConfigPort[portIdx].deviceType[0] == XL_BL_2_BUTTON)) {
    if (endIdx >= (deviceCount * MAX_XL_RGBLEDS_PER_BUTTONLIGHT)) {
      SendError("LED num > Max for Button Light Count", endIdx+1);
      return;
    }
  }
  else if ((XLConfigPort[portIdx].deviceType[0] == XL_12STICK_W_BUTTONS) ||
           (XLConfigPort[portIdx].deviceType[0] == XL_12STICK_WO_BUTTONS)) {  // we have stick lights
    if (endIdx >= (deviceCount * MAX_XL_RGBLEDS_PER_12STICK)) {
      SendError("LED num > Max for Stick Count", endIdx+1);
      return;
    }
  }
  else { // only remaining device type is 9 inch sticks
    if (endIdx >= (deviceCount * MAX_XL_RGBLEDS_PER_9STICK)) {
      SendError("LED num > Max for Stick Count", endIdx+1);
      return;
    }
  }

  SendAck();

  for (deviceCount = beginIdx; deviceCount <= endIdx;) {
    unsigned char startIdx = deviceCount;
    unsigned char endIdx = deviceCount;
    if (deviceCount % 3 == 1) {
      SendCommand2Slave(CMD_LED_RAM_UPDATE1, portIdx, startIdx, buttonColor,
                        lightStatus, 0);    
      deviceCount++;
      continue;
    }
    while (deviceCount % 3 != 1 && deviceCount <= endIdx) {
      endIdx = deviceCount;
      deviceCount++;
    }
    SendCommand2Slave(CMD_LED_RAM_UPDATEx, portIdx, startIdx, lightColor,
                      lightStatus, endIdx);
  }

  SendCommand2Slave(CMD_PORT_UPDATE, portIdx, 0, 0, 0, 0);// update LEDs

}// XLRangeColorMsg


void
XLRangeLightMsg(void)
{
  
  unsigned char port, led_color, led_status, beg_led_num, end_led_num;
  unsigned char deviceCount;

 
  // sets the color of a range of LEDs on the specified port
  port = rxPacket.data[MSG_XLRANGE_LIGHT_PORT];
  beg_led_num = rxPacket.data[MSG_XLRANGE_LIGHT_BEG_NUMBER];
  end_led_num = rxPacket.data[MSG_XLRANGE_LIGHT_END_NUMBER];
  led_color = rxPacket.data[MSG_XLRANGE_LIGHT_COLOR];
  led_status = rxPacket.data[MSG_XLRANGE_LIGHT_STATUS];

  rxPacket.status = PKT_RX_EMPTY;

  if ((port == 0) || (port > MAX_XL_PORTS))
  {
    SendError("Port = 0 or > Max", port);
    return;
  }

  port--;   // convert port from number to index
  deviceCount = XLConfigPort[port].deviceCount;
  
  if (beg_led_num == 0)
  {
    SendError("Beg Number = 0", beg_led_num);
    return;
  }

  if (end_led_num == 0)
  {
    SendError("End Number = 0", end_led_num);
    return;
  }
  beg_led_num--;    // convert from num to index
  end_led_num--;

  if ((XLConfigPort[port].deviceType[0] == XL_BL_1_BUTTON) ||   // an XL port will contain either all button lights OR all stick lights
    (XLConfigPort[port].deviceType[0] == XL_BL_2_BUTTON))
  {
    if (end_led_num >= (deviceCount * MAX_XL_RGBLEDS_PER_BUTTONLIGHT))
    {
      SendError("LED num > Max for Button Light Count", end_led_num+1);
      return;
    }
  }
  else if ((XLConfigPort[port].deviceType[0] == XL_12STICK_W_BUTTONS) ||
           (XLConfigPort[port].deviceType[0] == XL_12STICK_WO_BUTTONS))    // we have stick lights
  {
    if (end_led_num >= (deviceCount * MAX_XL_RGBLEDS_PER_12STICK))
    {
      SendError("LED num > Max for Stick Count", end_led_num+1);
      return;
    }
  }
  else // only remaining device type is 9 inch sticks
  {
    if (end_led_num >= (deviceCount * MAX_XL_RGBLEDS_PER_9STICK))
    {
      SendError("LED num > Max for Stick Count", end_led_num+1);
      return;
    }
  }

  SendAck();

  SendCommand2Slave(CMD_LED_RAM_UPDATEx, port, beg_led_num, led_color, led_status, end_led_num);
  
  SendCommand2Slave(CMD_PORT_UPDATE, port, 0, 0, 0, 0);  // update physical LEDs
  
}


void
XLSetExternalAddrMsg(void) // sets bay display device addresses
{                         // data format is [num of devices, (num of devices) 4 byte addresses]
  unsigned char i;
  unsigned char numOfDevices;

  numOfDevices = rxPacket.data[MSG_XLSET_EXTERNAL_NUM_DEVICES];
  if (numOfDevices <= MAX_XL_BAY_DISP_DEVICES)
  {
    ClearExternalAddresses();    // clear existing device addresses
    for (i = 0; i < numOfDevices; i++)
    {
      AddBayDispAddress(*((unsigned long*)(&(rxPacket.data[MSG_XLSET_EXTERNAL_ADDR1+(4 * i)]))));
    }
    rxPacket.status = PKT_RX_EMPTY;
  }
  else {
    rxPacket.status = PKT_RX_EMPTY;
    SendError("Num of Devices > Max", numOfDevices);
    return;
  }
  SendAck();

}


void
XLGetExternalAddrMsg (void)
{
  unsigned char i, j;
  unsigned char numOfBayDispDevices;
  
  rxPacket.status = PKT_RX_EMPTY;
  
  numOfBayDispDevices = GetNumOfBayDispDevices();
  txPacket.data [PKT_DATA_INDEX] = INSTR_XL_EXTERNAL_ADDR;
  txPacket.data [MSG_XLSET_EXTERNAL_NUM_DEVICES] = numOfBayDispDevices; 
  j = 0;
  for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++)
  {
    if (XLBayDispDevice[i].address != 0)
    {
      *((unsigned long*)(&(txPacket.data [MSG_XL_EXTERNAL_ADDR1+(4 * j)]))) = XLBayDispDevice[i].address;
      j++;
    }
  }
  msgDataLength = (unsigned char) (1 + (4 * numOfBayDispDevices));
  BuildPacket ();
  SendPacket ();

}// XLGetBayDispAddrMsg


void
XLRunAutoConfigMsg (void)
{
  rxPacket.status = PKT_RX_EMPTY;
  SendAck ();
  SendCommand2Slave(CMD_RUN_AUTOCONFIG, 0, 0, 0, 0, 0);
}
