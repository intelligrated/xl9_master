//
// Author: Lamar
//

#ifndef PROCESS_MSGS_H
#define PROCESS_MSGS_H


///////////////
// Prototypes
///////////////
void AbortMsg (void);
void ConfigureMsg (void);
void XLConfigMsg(void);
void GetStatusMsg (void);
void PingMsg (void);
void SelfTestMsg (void);
void SetStateMsg (void);
void GenericMsg (void);
void XLPickMsg (void);
void XLGetPortDevicesMsg (void);
void XLSetPortDevicesMsg(void);
void XLSingleLightMsg (void);
void XLRangeColorMsg(void);
void XLRangeLightMsg (void);
void XLGetExternalAddrMsg (void);
void XLSetExternalAddrMsg (void);
void XLRunAutoConfigMsg (void);

#endif

