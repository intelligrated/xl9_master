//
// Author: Lamar
//

#ifndef STRUCTS_H
#define STRUCTS_H


#include "TrakConstants.h"



struct Packet
{
  unsigned char status;
  unsigned char activeIndex;
  unsigned char data [PKT_MAX_LENGTH];
};


// XL
struct MiniPacket
{
  unsigned char status;
  unsigned char activeIndex;
  unsigned char data[MINI_PKT_MAX_LENGTH];
};

struct PBSwitchX
{
  unsigned char Value;    // good for 4 buttons 
  unsigned char Status;
};

struct BLPortx
{
  unsigned char RGBdata[MAX_XL_BUTTONLIGHTS_PER_PORT];    // 2 RGB leds per button, use msnibble, lsnibble
  
};

struct MailBox
{
  unsigned char status;
  unsigned char portIdx;
  unsigned char deviceIdx;
  unsigned char value;
};

struct ConfigPortx
{
  unsigned char deviceType[MAX_XL_BUTTONLIGHTS_PER_PORT];
  unsigned char deviceCount;
};

struct XLPickData
{
  unsigned long seqNum;
  unsigned char color;
  unsigned char status;
  unsigned char begIdx;
  unsigned char endIdx;
  unsigned int  zone;
  unsigned int  section;
};

struct BayDispDevice
{
  unsigned long address;
  unsigned int timer;
};

// input portion
// BIT  Meaning
//  0   logic x     3 bit XL device ID pattern 
//  1   logic x
//  2   logic x
//  3   button8     located next to upstream connector
//  4   button4
//  5   button2
//  6   button1     located next to downstream connector (controller side)
//  7   logic 1     1 bit stick present pattern

// structure defining how buttons are read
struct inputButtons {
	unsigned int XLdeviceID     : 3;        // 3 bit XL device ID
    unsigned int button8    	: 1;        // button S4 (closest to controller) downstream end
	unsigned int button4		: 1;        // button S3
	unsigned int button2		: 1;        // button S2
	unsigned int button1		: 1;        // button S1 (upstream end of stick)		
	unsigned int stickPresent1  : 1;        // 1 bit stick present pattern. logic 1
};
struct twoNibbles {
	unsigned char n1			: 4;        // these are nibbles so don't need to be swapped
	unsigned char n2			: 4;
};
union xlButtons {
	struct inputButtons b;
	struct twoNibbles	n;
	unsigned char		c;
};

										
#endif
