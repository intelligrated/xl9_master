//
// Author: Lamar, Tom
//


////////////
// Headers
////////////
#include <limits.h>

#include "globals.h"
#include "packet.h"
#include "unconfiguredState.h"
#include "utils.h"


void
UnconfiguredState (void)
{
  
  static unsigned char msg = 0;

  if (deviceStatus & DEVICE_STATUS_HAS_MESSAGE)     
  {
    nbrOfSends = 255;

    deviceStatus &= (unsigned int) ~DEVICE_STATUS_HAS_MESSAGE; 
    switch (rxPacket.data [PKT_DATA_INDEX])
    {
      case INSTR_ACK:
        rxPacket.status = PKT_RX_EMPTY;
        if (rxPacket.data [PKT_SEQUENCE_INDEX] == lastSendSeq) {
          txPacket.status = PKT_TX_EMPTY;
          MilliSecondDelayNoWait3 (60000); // 60 secs
        }
      break;

      case INSTR_NOT_CONFIGURED:
        rxPacket.status = PKT_RX_EMPTY;
        SendUnconfigured ();
      break;

      default:
        deviceStatus |= DEVICE_STATUS_HAS_MESSAGE;
    }
  }
  else if (txPacket.status == PKT_TX_EMPTY)
  {
    // this sends a message 60 seconds after receiving an ACK 
    if (!(deviceStatus & DEVICE_STATUS_DELAYING3))
    {
      SendUnconfigured ();
      MilliSecondDelayNoWait3 (10000); // 1 minute
/*
      switch (msg)
      {
      case 0:
        SendMsgToBayDisplay("WELCOME TO XL DEMO");
        break;
      case 1:
        SendMsgToBayDisplay("GENERATING PICK 1");
        break;
      case 2:
        SendMsgToBayDisplay("PUSH LIGHTED BUTTON");
        break;

      }
      if (++msg > 2)
        msg = 0;
*/      
    }
  }

  //// GetPacket may have changed this, so correct it
  //txPacket.data [PKT_SEQUENCE_INDEX] = (unsigned char) (sendSeq - 1);
  
  // button values
  //   1 -> S1 button closest to upstream end of stick
  //   2 -> S2 next to button S1
  //   4 -> S3 next to button S2
  //   8 -> S4 button closest to downstream (controller side) of stick
  // 
  
  if (txPacket.status == PKT_TX_EMPTY)
  {
    if (ButtonMailBox.status == MB_FULL)
    {
      txPacket.data [PKT_SEQUENCE_INDEX] = sendSeq++;
      txPacket.data [PKT_DATA_INDEX] = INSTR_XLBUTTON;
      txPacket.data [MSG_XLBUTTON_PORT] = (unsigned char) (ButtonMailBox.portIdx+1);    // send port # instead of index
      txPacket.data [MSG_XLBUTTON_DEVICE_NUMBER] = (unsigned char) (ButtonMailBox.deviceIdx+1);  // send device # instead of index
      txPacket.data [MSG_XLBUTTON_VALUE] = (unsigned char)(ButtonMailBox.value);
      msgDataLength = 3;
      BuildPacket ();
      SendPacket (); 
      ButtonMailBox.status = MB_EMPTY;
    }
  }

  if ((nbrOfSends >= 10) && (nbrOfSends != 255)) {
    _asm("stop");      // reboot processor
  }

}// UnconfiguredState
