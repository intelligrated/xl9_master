//
// Author: Lamar
//


////////////
// Headers
////////////
#include <stdlib.h>
#include <string.h>

#include "iosgb.h"
#include "portbits.h"
#include "globals.h"
#include "packet.h"
#include "utils.h"
#include "packet.h"
#include "XLController.h"




void uDelay(unsigned char u)
{                               // NOTE: lower numbers are less accurate
  while(u--);                   // 2:2us,  100:100us,  (these require int argument:: 1000: 1.2ms,  10000: 10ms)
} // end uDelay()


void
SendError (const char *str, const unsigned nbr)
{
  rxPacket.status = PKT_RX_EMPTY;
  
  if ((deviceStatus & DEVICE_STATUS_RECV_BROADCAST)) {  // no responding to broadcast msg
    txPacket.status = PKT_TX_EMPTY;
    return;
  }

  txPacket.data [INSTR_INDEX] = INSTR_ERROR;
  txPacket.data [SEQUENCE_INDEX] = sendSeq++;
  *((unsigned*)(&(txPacket.data [MSG_ERROR_NUM]))) = nbr;
  strcpy(&txPacket.data [MSG_ERROR_STR], str);
  msgDataLength = (unsigned char)(2+strlen(&txPacket.data [MSG_ERROR_STR])+1);
  BuildPacket ();
  SendPacket ();
}


void
SendRefreshDevice() {
  txPacket.data [PKT_SEQUENCE_INDEX] = sendSeq++;
  txPacket.data [PKT_DATA_INDEX] = INSTR_REFRESH_DEVICE;
  msgDataLength = 0;
  BuildPacket ();
  SendPacket ();
}


void
SetAddress(void) {
  // code      @ 0x1840
  // bootstrap @ 0xF067
  unsigned long unitSerialNumber @0xFFAC; 
  // unsigned char preAssignedUnitType @0x1200;

  myAddress = unitSerialNumber;
}

 

void
InitTimers (void)
{
  // init timer1 for free running w/interrupt on overflow
  // 7373/147456=5.000136e-2=.05000136seconds=50ms
  // 737/147456=0.00499810 = 5 mS
  TPM1MOD = 7373;  // 50 mS

  // Enable timer1's overflow interrupt
  // Operate in edge-aligned PWM mode
  // Bus rate clock 18.874368MHz
  // Divisor 128
  // this gives 6.78microseconds per cycle
  // 18.874368MHz/128=147456 cycles/sec
  // 128/(18.874368MHz)=6.78E-6 seconds
  TPM1SC = 0x4F;


  // init timer2 for free running w/interrupt on overflow
  // this gives 1ms per interrupt
  // 1ms=.001*18.874368MHz/128=147.456cycles
  // 148/147456=1.003689E-3seconds= 1ms
  TPM2MOD = 148;

  // Enable timer2's overflow interrupt
  // Operate in edge-aligned PWM mode
  // Bus rate clock 18.874368MHz
  // Divisor 128
  // this gives 6.78microseconds per cycle
  // 18.874368MHz/128=147456 cycles/sec
  // 128/(18.874368MHz)=6.78E-6 seconds/cycle
  TPM2SC = 0x4F;

}// InitTimers


void
MilliSecondDelayNoWait (unsigned int timeToDelay)
{
  // delay between 0 and 65535 milliseconds - this timer used by SC1 communications

  // BUS_CLOCK = 18.874368MHz = 18874368 cycles per second
  // divisor for timer2 = 128
  // modulus for timer2 = 147
  // t is in seconds
  // t = t * 18.874368MHz/128/147 = # cycles
  // 1ms = .001 * 18.874368MHz/128/147 = 1 cycle
  
  delay1Cycles = timeToDelay;
  deviceStatus |= DEVICE_STATUS_DELAYING;

}// MilliSecondDelayNoWait


void
MilliSecondDelayNoWait2 (unsigned int timeToDelay)
{
  // delay between 0 and 65535 milliseconds - this timer used by SC2

  // BUS_CLOCK = 18.874368MHz = 18874368 cycles per second
  // divisor for timer2 = 128
  // modulus for timer2 = 147
  // t is in seconds
  // t = t * 18.874368MHz/128/147 = # cycles
  // 1ms = .001 * 18.874368MHz/128/147 = 1 cycle
  
  delay2Cycles = timeToDelay;
  deviceStatus |= DEVICE_STATUS_DELAYING2;

}// MilliSecondDelayNoWait2


void
MilliSecondDelayNoWait3 (unsigned int timeToDelay)
{
  // delay between 0 and 65535 milliseconds - this timer is general purpose

  // BUS_CLOCK = 18.874368MHz = 18874368 cycles per second
  // divisor for timer2 = 128
  // modulus for timer2 = 147
  // t is in seconds
  // t = t * 18.874368MHz/128/147 = # cycles
  // 1ms = .001 * 18.874368MHz/128/147 = 1 cycle
  
  delay3Cycles = timeToDelay;
  deviceStatus |= DEVICE_STATUS_DELAYING3;

}// MilliSecondDelayNoWait3

void
MilliSecondDelayWait (unsigned int timeToDelay)
{
  MilliSecondDelayNoWait (timeToDelay);

  while (deviceStatus & DEVICE_STATUS_DELAYING) {
    if (rxPacket.status & PKT_RX_READ)
      return;
  }
}// MilliSecondDelayNoWait

/*
void
MilliSecondDelayWait2 (unsigned int timeToDelay)
{
  MilliSecondDelayNoWait2 (timeToDelay);

  while (deviceStatus & DEVICE_STATUS_DELAYING2) {
    if (rxPacket.status & PKT_RX_READ)
      return;
  }
}// MilliSecondDelayNoWait
*/

void
RandomDelayNoWait (void)
{
  // delay random time between 1 and 255.0 milliseconds

  unsigned delayMilliSecs = 1 + rand () % 255;
  MilliSecondDelayNoWait (delayMilliSecs);
}


void
RandomDelayWait (void)
{
  // delay random time between 1 and 255.0 milliseconds

  unsigned delayMilliSecs = 1 + rand () % 255;
  MilliSecondDelayWait (delayMilliSecs);
}

/*
void
RandomDelayNoWait2 (void)
{
  // delay random time between 1 and 255.0 milliseconds

  unsigned delayMilliSecs = 1 + rand () % 255;
  MilliSecondDelayNoWait2 (delayMilliSecs);
}


void
RandomDelayWait2 (void)
{
  // delay random time between 1 and 255.0 milliseconds

  unsigned delayMilliSecs = 1 + rand () % 255;
  MilliSecondDelayWait2 (delayMilliSecs);
}
*/

unsigned char *
NbrToStr (unsigned long number, unsigned char base)
{
  unsigned char digit;
  unsigned char index = 0;

  if (number == 0)
  {
    nbrToStr [0] = 0;
    return nbrToStr;
  }

  while (1)
  {
    digit = (unsigned char) (number % base);
    if (base == 10)
    {
      nbrToStr [index] = (unsigned char) ('0' + digit);
    }
    else
    {
      if (digit < 10)
        nbrToStr [index] = (unsigned char) ('0' + digit);
      else
        nbrToStr [index] = (unsigned char) ('7' + digit);
    }
    index++;
    if (number < base)
     break;
    number /= base;
  }
  nbrToStr [index] = 0;

  // reuse number
  for (number=0; number < index / 2; number++)
  {
    digit = nbrToStr [number];
    nbrToStr [number] = nbrToStr [index - number - 1];
    nbrToStr [index - number - 1] = digit;
  }
  nbrToStr [index] = 0;

  return nbrToStr;

}// NbrToStr


void  // warning: limited to strings <= 12 chars 
StringCopy (char *st1, const char* st2)
{
  int i;
  for (i = 0; i < MAX_T3C_DISPLAY_SIZE; i++) {
    st1 [i] = st2 [i];
    if (st1 [i] == 0)
      return;
  }
  st1 [MAX_T3C_DISPLAY_SIZE] = 0;
  return;
}


// **************************************************************************
// * FUNCTION  GetBit                                                       *
// **************************************************************************
// * ELB                                                                    *
// * Version 1.0                                                            *
// * Date 02/20/2003                                                        *
// **************************************************************************
// * PARAMETERS USED                                                        *
// * uInt  - iGetBit (bit to read 0-15)                                     *
// * uInt  - iFlag (unsigned interger used as a bit flag)                   *
// **************************************************************************
// * RETURN VALUE                                                           *
// * unsigned int (bit value 0-1)                                           *
// **************************************************************************
unsigned int GetBit(unsigned int iGetBit, unsigned int iValue)
{
    int iMaskBit = 1;
    // Bounds check SetBit
    if (iGetBit > 15)
        return (iValue);

    // Shift to set bit
    iMaskBit = iMaskBit << iGetBit;
    // Set processor flag
    iValue = iValue & iMaskBit;
    iValue = iValue >> iGetBit;
    return (iValue);
}

 
// --------------------------------------------------------------------------
// - FUNCTION  GetBit1  uses more code space, but executes much faster
//                      limited to byte operands (unsigned char)
// --------------------------------------------------------------------------
// - INPUTS                                                                 
// - uChar  - iBitPos (bit position to read 0-7)                               
// - uChar  - iValue (variable to extract bit from)                          
// --------------------------------------------------------------------------
// - RETURN VALUE                                                           
// - uChar (bit value 0-1)                                           
// --------------------------------------------------------------------------
unsigned char GetBit1(unsigned char iBitPos, unsigned char iValue)
{
  unsigned char iMask;

  switch (iBitPos)
  {
    case 0:
      iMask = 0x01;
    break;
    case 1:
      iMask = 0x02;
    break;
    case 2:
      iMask = 0x04;
    break;
    case 3:
      iMask = 0x08;
    break;
    case 4:
      iMask = 0x10;
    break;
    case 5:
      iMask = 0x20;
    break;
    case 6:
      iMask = 0x40;
    break;
    case 7:
      iMask = 0x80;
    break;
    default:
      return(0);    // illegal iBitPos value
  }
  if (iMask & iValue)
    return(1);
  else
    return(0);
}  // GetBit1

// **************************************************************************
// * FUNCTION  SetBit                                                       *
// **************************************************************************
// * Date 04042011                                                          *
// **************************************************************************
// * PARAMETERS USED                                                        *
// * uInt  - iSetBit (bit filed to affect 0-15)                             *
// * uChar - OnOff (Turn Bit on or off 0-1)                                 *
// * uInt  - iFlagToSet (unsigned interger used as a bit flag)              *
// **************************************************************************
// * RETURN VALUE                                                           *
// * unsigned char                                                          *
// **************************************************************************
unsigned int SetBit(unsigned int iSetBit, unsigned char OnOff,unsigned int iValue)
{
unsigned int iMaskBit;

	 // Bounds check SetBit
	 if (iSetBit > 15)
		  return(iValue);

	 // Set mask bit
	 if (OnOff == 1)
	{
		  iValue |= 1 << iSetBit;
    }
	else
	{
		  iMaskBit = 1;
	 // Shift to set bit
	 iMaskBit = iMaskBit << iSetBit;
		  // Set processor flag
		  iValue = iValue & (~iMaskBit);
	}
    return(iValue);
}

 
// --------------------------------------------------------------------------
// - FUNCTION  SetBit1       uses more code space, but executes much faster
//                           limited to byte operands (unsigned char)
// --------------------------------------------------------------------------
// - INPUTS                                                                 
// - uChar  - iBitPos (bit position to modify 0-7)
// - uChar - OnOff (desired logic state of bit, 1 or 0)
// - uChar  - iValue (variable to be modified)                         
// --------------------------------------------------------------------------
// - RETURN VALUE                                                           
// - uChar (input variable with modified bit)                                           
// --------------------------------------------------------------------------
unsigned char SetBit1(unsigned char iBitPos, unsigned char OnOff, unsigned char iValue)
{
  unsigned char iMask;

       
  switch (iBitPos)
  {
    case 0:
      iMask = 0x01;
    break;
    case 1:
      iMask = 0x02;
    break;
    case 2:
      iMask = 0x04;
    break;
    case 3:
      iMask = 0x08;
    break;
    case 4:
      iMask = 0x10;
    break;
    case 5:
      iMask = 0x20;
    break;
    case 6:
      iMask = 0x40;
    break;
    case 7:
      iMask = 0x80;
    break;
    default:
      return(0);    // illegal iBitPos value
  }
  if (OnOff)
    iValue |= iMask;
  else
    iValue &= (unsigned char)(~iMask);
  return(iValue);

} // SetBit1



// --------------------------------------------------------------------------
// - FUNCTION  IsABayDisplayAddress()                                                             
// --------------------------------------------------------------------------
// - INPUTS                                                                 
// - unsigned long - address                         
// --------------------------------------------------------------------------
// - RETURN VALUE                                                           
// - unsigned char - TRUE if address is stored in the XLBayDispDevice structure
// -                 FALSE otherwise
// --------------------------------------------------------------------------
unsigned char 
IsABayDispAddress(unsigned long addr) {
  unsigned char i;

  for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++) {
    if (addr == XLBayDispDevice[i].address) {
      XLBayDispDevice[i].timer = BAY_DISP_DEV_ACTIVITY_TC;//reset activity timer
      return(TRUE);
    }
  }
  return(FALSE);
}
  

// -----------------------------------------------------------------------------
// - FUNCTION  AddBayDisplayAddress(unsigned long addr)
// -           if the device is already in the array, preset its' activity timer
// -           if not, and there is room in the array, add the address and 
// -              preset its' activity timer
// -           if not, and there is no room in the array, find a device that has
// -              its' activity timer equal to 0.  Remove that address and
// -              insert the new address & preset activity timer
// -           if not in array, array is full and no timers = 0, return FALSE
// -----------------------------------------------------------------------------
// - INPUTS                                                                 
// - unsigned long - address                         
// --------------------------------------------------------------------------
// - RETURN VALUE                                                           
// - unsigned char - TRUE if address was added to the array
// -                      or if the address was already stored
// -                 FALSE otherwise
// --------------------------------------------------------------------------
unsigned char 
AddBayDispAddress(unsigned long addr) {
  unsigned char i;

  for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++) {
    if (XLBayDispDevice[i].address == addr) {
      XLBayDispDevice[i].timer = BAY_DISP_DEV_ACTIVITY_TC; // reset activity
      return(TRUE);       // address is already in the array
    }
  }

  for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++) {
    if (XLBayDispDevice[i].address == 0) {
      XLBayDispDevice[i].address = addr; // store new address in unused location
      XLBayDispDevice[i].timer = BAY_DISP_DEV_ACTIVITY_TC;
      return(TRUE);
    }
  }
  
  return(FALSE);  // address could not be added

}// AddBayDispAddress


unsigned char
GetNumOfBayDispDevices( void )
{
  unsigned char i;
  unsigned char numDevices = 0;

  for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++)
    if (XLBayDispDevice[i].address != 0)
      numDevices++;

  return(numDevices);  
}

void
ClearExternalAddresses( void )    // clears the external address array
{
  unsigned char i;

  for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++)
    memset(&XLBayDispDevice[i], 0, sizeof(struct BayDispDevice));
}


unsigned char 
SendMsgToBayDisplay (const char *str)
{
  unsigned char i;

  rxPacket2.status = PKT_RX_EMPTY;
  
  for (i = 0; i < MAX_XL_BAY_DISP_DEVICES; i++)
  {
    if (XLBayDispDevice[i].address != 0)
    {
     *((unsigned long*)(&(txPacket2.data [PKT_ADDRESS_INDEX]))) = XLBayDispDevice[i].address;
     break;
    }
  }
  if (i == MAX_XL_BAY_DISP_DEVICES)
    return (FALSE);     // no bay display device attached
  
  txPacket2.data [INSTR_INDEX] = INSTR_GENERIC;
  txPacket2.data [SEQUENCE_INDEX] = sendSeq++;
  for (i = 0; i < 19; i++)
  {
    txPacket2.data[MSG_GENERIC_LTDOWN_STATUS+i] = 0;  // 0 is default value for this MSG
  }
  txPacket2.data[MSG_GENERIC_DISPLAY_STATUS] = DISPLAY_SOLID;
  msgDataLength2 = (unsigned char)(strlen(str));
  txPacket2.data[MSG_GENERIC_STR1_LEN] = msgDataLength2;
  strcpy(&txPacket2.data [MSG_GENERIC_STR1], str);
  msgDataLength2 += 20;   // +20 for INSTR and everything prior to STR1
  BuildPacket2(); // calc msg length & checksum
  SendOnce2();
  return (TRUE);    // show success
}


// GetPBSindex calculates the equivalent (LED) index for a given push button switch
// deviceIndex is the relative position on a port
unsigned char
GetPBSindex (unsigned char deviceType, unsigned char deviceIndex, unsigned char SWvalue)
{
  unsigned char slIndex = 0;    // (the *#!% compiler made me do the auto initialize)
  unsigned char blIndex = 0;

  switch (SWvalue)  // SWvalue is returned from the switch scanning code; 1 bit set per switch
  {
    case 0x01:
      slIndex = 1;    // index is set to the index of the LED associated with the switch
      blIndex = 0;
      break;        

    case 0x02:
      slIndex = 4;
      blIndex = 1;
      break;

    case 0x04:
      slIndex = 7;
      break;

    case 0x08:
      slIndex = 10;
      break;
  }

  switch (deviceType)
  {
    case XL_9STICK_W_BUTTONS:
      slIndex = (unsigned char) (slIndex + (deviceIndex * MAX_XL_RGBLEDS_PER_9STICK));
      return (slIndex);
      break;

    case XL_12STICK_W_BUTTONS:
      slIndex = (unsigned char)(slIndex + (deviceIndex * MAX_XL_RGBLEDS_PER_12STICK));
      return (slIndex);
      break;

    case XL_BL_1_BUTTON:
    case XL_BL_2_BUTTON: 
      blIndex = (unsigned char)(blIndex + (deviceIndex * MAX_XL_RGBLEDS_PER_BUTTONLIGHT));
      break;
  }
  return (blIndex);
}

/*
void
StickDispVar(unsigned char uc)  // use the stick lights to display a variable
{                               // for debugging only
                                // for 10 stick port, max value is 119
  unsigned char port, ledColor;
  unsigned char stick_count, i;

  port = 0; 
  ledColor = LT_BLUE;

  stick_count = XLConfigPort[port].deviceCount;

  for (i = 0; i <= uc; i++)
  {
    UpdateXL12LEDMemory(port, i, ledColor, LT_SOLID);  // update memory
  }
  SendShelfStickData(port, stick_count, FALSE);  // update physical LEDs 

}
*/


// constants
#define MAX_S9_LENGTH 10


// Globals
struct PassThruVars {
  unsigned char input [MAX_S9_LENGTH];
  unsigned char idx;
  unsigned char nbrOfInputChars;
  unsigned char readChar;
  unsigned char readSCI1S1;
  unsigned char nbrOfS9Records;

} ptv;


unsigned int
HexToDecimal (const unsigned char *data, const unsigned char length)
{
  unsigned char idx;
  unsigned int value;

  value = 0;
  for (idx = 0; idx < length; idx++) {
    if (data [idx] <= '9')
      value += (data [idx] - '0') * (1 << (4 * (length - 1 - idx)));
    else
      value += (data [idx] - '7') * (1 << (4 * (length - 1 - idx)));
  }
  return value;
}


unsigned char
Checksum (const char *input, const unsigned char nbrOfChars)
{
  unsigned char checkSum;
  unsigned char idx;


  checkSum = 0xFF;
  for (idx = 0; idx < nbrOfChars; idx += 2) {
    checkSum -= (unsigned char) HexToDecimal (&input [idx], 2);
  }
  return checkSum;
}



void
ReadCharSCI1 (void)
{
  // returns the character read from SCI1
  // returns 0 if no data
  // returns -1 for data error

  ptv.readSCI1S1 = SCI1S1; // read status register
  ptv.readChar = SCI1D;    // read data
  SCI1C3; // 9 bit mode

  if ((ptv.readSCI1S1 & 0x20) == 0) { // no data
    ptv.readChar = 0;
  }
//  if ((f1.readSCI1S1 & 0x0F) != 0) { // data error
//    f1.readChar = -1;
//  }
  if ((ptv.readSCI1S1 & 0x10) != 0) { // idle event
    ptv.readChar = 0;
  }
}// ReadChar

void
WriteCharSCI2 (void)  // writes ptv.readChar to SCI2
{
    
  while(!(SCI2S1 & 0x80))  // loop til transmitter data reg empty (TDRE)
    ;
  SCI2D = ptv.readChar;

}

// ------------------------------------------------------------------
// PassThru2BayDisplayPort() copies all chars received on SCI1
//    to the Bay Display Port (SCI2).  At the same time, it
//    monitors the data stream for 4 consecutive S9 records,
//    which indicates the end of programming.
//    NOTE: interrupts must be disabled prior to calling this routine
// ------------------------------------------------------------------
void
PassThru2BayDisplayPort (void)
{
  unsigned char forever;
  unsigned char checksum;
  unsigned char tmpUChar;
  unsigned char value;

  forever = TRUE;
  ptv.idx = 0;
  ptv.nbrOfS9Records = 0;
  TXEN2 = 1; // Enable 485 transmitter2

  while (forever)
  {
    ReadCharSCI1();
    if (ptv.readChar == 0)
      continue;
    
    WriteCharSCI2();  // send char to bay display port

    ptv.input[ptv.idx++] = ptv.readChar;

    switch (ptv.idx) {
      
      case 1:
        if (ptv.readChar != 'S')
          ptv.idx = 0;
        break;

      case 2:     // record type
        if (ptv.readChar != '9')
        {
          ptv.idx = 0;
          ptv.nbrOfS9Records = 0;
        }
        break;

      case 3:     // 1st nibble of record length
        break;

      case 4:     // 2nd nibble - calculate record length
         ptv.nbrOfInputChars = (unsigned char) HexToDecimal(&ptv.input[2], 2);
         ptv.nbrOfInputChars *= 2; // convert bytes to nibbles
         break;

      case 5:     // collecting starting address & 1st hex digit of checksum
      case 6:
      case 7:
      case 8:
      case 9:
        break;

      case 10:  // 2nd digit of checksum
        checksum = Checksum (&ptv.input [2], ptv.nbrOfInputChars);
        tmpUChar = (unsigned char) (ptv.idx - 2); // last 2 chars
        value = (unsigned char) HexToDecimal (&ptv.input [tmpUChar], 2);
        if (checksum != value)
        {                    // S9 record failed checksum
          ptv.idx = 0;
          ptv.nbrOfS9Records = 0;
        }
        else
        {
          if (++ptv.nbrOfS9Records == 4)
          {                   // 4 consecutive S9 records
            forever = FALSE;  // end of pass thru mode
          }
          else
          {
            ptv.idx = 0;
          }
        }
        break;
    
      default:
        ptv.idx = 0;
        ptv.nbrOfS9Records = 0;
        break;

    } // end of switch
  } // end of while (forever)
  
  while (!(SCI2S1 & 0x40))    // wait for last char to be sent
    ;

  TXEN2 = 0; // Disable 485 transmitter2
}

