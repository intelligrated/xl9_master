//
// Author: Lamar
//

#ifndef UTILS_H
#define UTILS_H


///////////////
// Prototypes
///////////////
void AddCharsInFront (char ch);
void InitTimers (void);
void MilliSecondDelayNoWait (unsigned int timeToDelay);
void MilliSecondDelayNoWait2 (unsigned int timeToDelay);
void MilliSecondDelayNoWait3 (unsigned int timeToDelay);
void MilliSecondDelayWait (unsigned int timeToDelay);
void RandomDelayNoWait (void);
void RandomDelayWait (void);
void RandomDelayNoWait2 (void);
void RandomDelayWait2 (void);
unsigned char *NbrToStr (unsigned long number, unsigned char base);
void ResetScrolling (void);
void SendError (const char *str, const unsigned nbr);
void SendMenuChoice (unsigned char lightChoice);
void SendMenuInstr (unsigned char menuIndex, unsigned char direction);
void SendRefreshDevice(void);
void ScrollMenu (void);
unsigned char Scrolling (void);
void SetAddress (void);
void StringCopy (char *st1, const char *st2);
void uDelay (unsigned char u);
unsigned int GetBit(unsigned int iGetBit, unsigned int iValue);
unsigned char GetBit1(unsigned char iBitPos, unsigned char iValue);
unsigned int SetBit(unsigned int iSetBit, unsigned char OnOff,unsigned int iValue);
unsigned char SetBit1(unsigned char iBitPos, unsigned char OnOff, unsigned char iValue);
unsigned char BayDispArrayMember (unsigned long addr);
unsigned char IsABayDispAddress (unsigned long addr);
unsigned char AddBayDispAddress (unsigned long addr);
unsigned char GetNumOfBayDispDevices( void );
void ClearExternalAddresses(void);
unsigned char SendMsgToBayDisplay (const char *str);
unsigned char GetPBSindex (unsigned char deviceType, unsigned char deviceIndex, unsigned char SWvalue);
unsigned char GetBL_SWvalue(unsigned char bedIdx);
void StickDispVar(unsigned char uc);
void PassThru2BayDisplayPort (void);

#endif

