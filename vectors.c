/*	INTERRUPT VECTORS TABLE FOR MC68HC08XL36
 *	Copyright (c) 1995 by COSMIC Software
 */

void _stext();			/* startup routine */
void InterruptRTI ()		@0xEE00;
void InterruptIIC ()		@0xEE04;
void InterruptATD ()		@0xEE08;
void InterruptKeyboard ()	@0xEE0C;
void InterruptSCI2_TX ()	@0xEE10;
void InterruptSCI2_RX ()	@0xEE14;
void InterruptSCI2_ERR ()	@0xEE18;
void InterruptSCI1_TX ()	@0xEE1C;
void InterruptSCI1_RX ()	@0xEE20;
void InterruptSCI1_ERR ()	@0xEE24;
void InterruptSPI ()		@0xEE28;
void InterruptTPM2_OV ()	@0xEE2C;
void InterruptTPM2_CH4 ()	@0xEE30;
void InterruptTPM2_CH3 ()	@0xEE34;
void InterruptTPM2_CH2 ()	@0xEE38;
void InterruptTPM2_CH1 ()	@0xEE3C;
void InterruptTPM2_CH0 ()	@0xEE40;
void InterruptTPM1_OV ()	@0xEE44;
void InterruptTPM1_CH2 ()	@0xEE48;
void InterruptTPM1_CH1 ()	@0xEE4C;
void InterruptTPM1_CH0 ()	@0xEE50;
void InterruptICG ()		@0xEE54;
void InterruptLVD ()		@0xEE58;
void InterruptMultidrop ()	@0xEE5C;
void InterruptSoftware ()	@0xEE60;


void (* const _vectab[])() =
{
  InterruptRTI,		/* Real-time interrupt */
  InterruptIIC,		/* IIC control */
  InterruptATD,		/* AD conversion complete */
  InterruptKeyboard,	/* Keyboard pins */
  InterruptSCI2_TX,	/* SCI2 tx empty, complete     */
  InterruptSCI2_RX,	/* SCI2 rx full, input idle       */
  InterruptSCI2_ERR,	/* SCI2 rx overrun,noise,framing & parity err */
  InterruptSCI1_TX,	/* SCI1 tx empty, complete     */
  InterruptSCI1_RX,	/* SCI1 rx full, input idle       */
  InterruptSCI1_ERR,	/* SCI1 rx overrun,noise,framing & parity err */
  InterruptSPI,		/* SPI */
  InterruptTPM2_OV,	/* TPM2 overflow   */
  InterruptTPM2_CH4,	/* TPM2 channel 4  */
  InterruptTPM2_CH3,	/* TPM2 channel 3  */
  InterruptTPM2_CH2,	/* TPM2 channel 2  */
  InterruptTPM2_CH1,	/* TPM2 channel 1  */
  InterruptTPM2_CH0,	/* TPM2 channel 0  */
  InterruptTPM1_OV,	/* TPM1 overflow   */
  InterruptTPM1_CH2,	/* TPM1 channel 2  */
  InterruptTPM1_CH1,	/* TPM1 channel 1  */
  InterruptTPM1_CH0,	/* TPM1 channel 0  */
  InterruptICG,		/* ICG             */
  InterruptMultidrop,	/* EXT IRQ1        */
  InterruptLVD,		/* Lo-voltage detect */
  InterruptSoftware,	/* SWI             */
  _stext,		/* RESET           */
};
